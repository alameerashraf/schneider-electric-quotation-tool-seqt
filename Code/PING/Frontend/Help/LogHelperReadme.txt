********************************************************************************************************************
* TITLE :
* Log Helper
*
* DESCRIPTION : 
* This component is used to log errors and information in an application. 
* It provides a standard set of methods that is used for application specific logs 
* and information in a predefined format. Log4Net is used for application logging. With log4net it is possible 
* to enable logging at runtime without modifying the application binary.
* The log file will be created under the bin folder of the project directory in case of a console application/ 
* windows service eg. ~/Bin/Logs/log.txt and in the project directory(~/Logs/log.txt) for a web application.
*
* PREREQUISITES :
* The folder path(eg. ~/Bin/Logs/log.txt for a console application and in the project directory(~/Logs/log.txt) 
* for a web application) where the logs are being written to should have appropriate permissions.
* The target framework of the application should be 4.0 and above.
*
* DEPENDENCIES :
* This component internally uses log4net dll for the logging functionality.
*
*******************************************************************************************************************
* CHANGE LOG:
* ModifiedDate		ModifiedBy		Comments
*
*
*
*******************************************************************************************************************
///METHODS:
 
1.	Error:
	This method is used to log application specific errors and exceptions.
	
	Input Parameters:
	Exception

	Output Parameters:
	String, exception is logged in a log.txt file created under the Logs folder.

2.	Information:
	This method is used to log application specific information in a text file. 
	This information will be helpful for debugging purposes.
	
	Input Parameters:
	String 

	Output Parameters:
	String, the information string is logged in a log.txt file created under the Logs folder. 	

3.	Warn:
	This method is used to log application specific warning messages in a text file.

	Input Parameters:
	String

	Output:
	String, the warning is logged in a log.txt file created under the Logs folder 	
**********************************************************************************************************************
///HOW TO IMPLEMENT THIS DLL IN YOUR PROJECT?

/*Note : Schnieder Nuget Package should be configured in Your Visual Studio, 
		Please refer SchneiderNuget Package server document*/

1. Install the LogHelper dll from the private Schneider Nuget server and add as reference to your project.

2. Add the namespace to your project.
	eg. using SchneiderElectric.Common;

3. Create an object of the LogHelper class present in the LogHelper Namespace.
	eg LogHelper obj = new LogHelper();

4. Setup the logger configuration by calling the Setup() method of the LogHelper class.
    eg obj.Setup();

5. Pass the required parameters to the desired methods of the LogHelper class.
	eg. try 
		{
	      int a, b;
		  quotient = a/b;
		  Console.Log(a);
		  obj.Information(a.ToString());
		  obj.Warn("Warning");
		}
		catch (Exception ex)
		{
			obj.Error(ex);	
		}

*********************************************************************************************************************