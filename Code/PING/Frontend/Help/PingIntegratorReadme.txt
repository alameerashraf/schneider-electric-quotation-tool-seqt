﻿
The component is to help integrate with PING Federate for authentication (using PING Federeate’ s own OPENID connect framework) & authorization (using PING Federate’s Oauth2 implementation) for MVC application.

Prerequisites
-----------------
please raise a remedy ticket using the link 
http://2929it.schneider-electric.com/kinetic/DisplayPage?srv=KSb7d2e9425f86bc322d7b91e92e8ced5d6 and fill the PingSSORequest excel.

Provide the below config details in the web.config.
    
    ClientId
    CodeChallenge
    ClientSecret
    redirect_uri
    JwtFormatIssuer
    as:AudienceId
    as:AudienceSecret

VS 2012 and above.

Target web server must be allowed over firewall to access the relevant PING urls like - https://ping-sso-uat.schneider-electric.com

Make sure the application is available in Schneider Network to download the component.

Dependencies
------------
Nuget.Org
	JWT 1.3.4
	Newtonsoft.JSON 9.0.1
Schneider Nuget
	LogHelper 1.0.0


Please follow the installtion instructions in the below box path.
https://schneider-electric.box.com/s/rqtczihhjjza2cfl2s5xlsmq4v46r8k5 
