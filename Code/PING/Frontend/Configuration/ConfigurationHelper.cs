﻿using Microsoft.Extensions.Configuration;

namespace Frontend.Configuration
{
    public class ConfigurationHelper
    {
        public static T GetConfiguration<T>(string filename, string basePath)
        {
            return new ConfigurationBuilder().SetBasePath(basePath).AddJsonFile(filename).Build().Get<T>();
        }
    }
}
