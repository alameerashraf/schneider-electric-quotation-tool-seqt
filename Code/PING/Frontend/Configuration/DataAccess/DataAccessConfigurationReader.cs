﻿namespace Frontend.Configuration.DataAccess
{
    public static class DataAccessConfigurationReader
    {
        public static DataAccessConfiguration ReadConfig(string filename, string basePath)
        {
            return ConfigurationHelper.GetConfiguration<DataAccessConfiguration>(filename, basePath);
        }
    }
}
