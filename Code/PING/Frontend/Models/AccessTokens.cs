﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;

namespace Frontend.Models
{
    public class AccessTokens
    {
        public ObjectId Id { get; set; }

        public AccessTokens()
        {
            Id = ObjectId.GenerateNewId();
        }

        public string GetIdString()
        {
            return Id.ToString();
        }
        public string Token { get; set; }
        public string SESA { get; set; }
        public DateTime IssuedAt { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}