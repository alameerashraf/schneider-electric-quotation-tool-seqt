﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Frontend.Configuration.DataAccess;
using Frontend.Models;
using MongoDB.Driver;
using PingOAuthFilterNS;
using SchneiderElectric.Common;
using static System.String;

namespace Frontend.Controllers
{
    [PingOAuthFilter]
    public class HomeController : Controller
    {
        public void PingCallback()
        {
            var logger = new LogHelper();
            var data = Request.QueryString.AllKeys.ToDictionary(index => index, index => Request.QueryString[index]);

            var info = new StringBuilder();
            foreach (var element in data)
            {
                info.Append(element.Key + ": " + element.Value + ",");
            }

            logger.Information("Current Info at: [" + DateTime.Now.ToLongDateString() + "] is :" + info);
        }

        public ActionResult auth(string returnUrl)
        {
            var token = StoreTokenFromSession();

            //returnUrl = returnUrl.Replace(@"/",@"$");
            var mainAppUrl = ConfigurationManager.AppSettings["mainAppUrl"];

            return Redirect($"{mainAppUrl}?sesa={token.sesa}&name={token.givenName}&secret={token.tkn}&returnUrl={returnUrl}");
        }

        private JWTModel StoreTokenFromSession()
        {
            var logger = new LogHelper();
            if (Request.Cookies["JWTCookie"] != null)
            {
                var tkn = Request.Cookies["JWTCookie"];
                var token = tkn["JWTCookie"];
                if (!IsNullOrEmpty(token))
                {
                    logger.Information("Current JWTCookie at: [" + DateTime.Now.ToLongDateString() + "] is :" + token);
                    var jwtModeldata = PingMethods.GetUserDetails(token);
                    return CheckAndStoreToken(jwtModeldata);
                }
            }

            if (HttpContext.Session["JWTToken"] != null)
            {
                var tkn = HttpContext.Session["JWTToken"].ToString();
                if (!IsNullOrEmpty(tkn))
                {
                    logger.Information("Current JWTToken at: [" + DateTime.Now.ToLongDateString() + "] is :" + tkn);
                    var userDetails = PingMethods.GetUserDetails(tkn);
                    return CheckAndStoreToken(userDetails);
                }
            }

            return null;
        }

        private static JWTModel CheckAndStoreToken(JWTModel jwtModeldata)
        {
            try
            {
                var dataAccessConfig =
                    DataAccessConfigurationReader.ReadConfig("DataAccessConfig.json", HostingEnvironment.ApplicationPhysicalPath);
                var client = new MongoClient(dataAccessConfig.Mongo.ConnectionString);
                var db = client.GetDatabase(dataAccessConfig.Mongo.Database);
                var collection = db.GetCollection<AccessTokens>(typeof(AccessTokens).Name);


                var filterBuilder = Builders<AccessTokens>.Filter;
                var filter = filterBuilder.Where(t => t.Token == jwtModeldata.tkn);
                var found = collection.Find(filter).CountDocuments() > 0;
                if (found) return jwtModeldata;
                var newToken = new AccessTokens()
                {
                    SESA = jwtModeldata.sesa,
                    Token = jwtModeldata.tkn,
                    IssuedAt = new DateTime(1970, 1, 1).AddSeconds(jwtModeldata.iat),
                    ExpiresAt = new DateTime(1970, 1, 1).AddSeconds(jwtModeldata.exp)
                };

                collection.InsertOne(newToken);
                return jwtModeldata;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}