import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { localStorageService, httpService, urls, authorizationService, responseModel, constants } from "./@core";
import { userRolesModel } from './@core/services/auth/userRolesModel';
import { headersModel } from './@core/services/http/headersModel';

@Component({
  selector: "seqt-app-landing",
  template: "<p></p>"
})
export class AppLandingComponent implements OnInit {
  loggedInUser = {
    userName: "",
    userAvatar: "",
    userSESA: "",
    token: "",
    isManager: false,
    userRoles: []
  };
  returnUrl: any = "";

  constructor(
    private route: ActivatedRoute,
    private storageService: localStorageService,
    private httpService: httpService,
    private authorizationService: authorizationService
  ) {
    this.route.queryParams.subscribe(params => {
      this.loggedInUser.userSESA = params["sesa"];
      this.loggedInUser.userName = params["name"];
      this.loggedInUser.token = params["secret"];
      this.returnUrl = params["returnUrl"];
      
      this.storageService.setLocalStorage("loggedInUser" , this.loggedInUser);
      
      let userRolesURL = `${urls.GET_USERS_ROLES}/${this.loggedInUser.userSESA}`
      this.httpService.Get(userRolesURL , {}).subscribe((roles: responseModel) => {
        this.storageService.setLocalStorage(constants.LOGGED_IN_USER_ROLES , roles.data);
        window.location.href = this.returnUrl;
      });


    });
  }

  ngOnInit() {}
}
