export class logger {
    action: string;
    entity: string;
    affectedUser: string;
    takenBy: string;
    createdAt: Date;
    actionText: string;
    actionColor?: string;
};

