import { StrictWS } from 'xlsx/types';

export class family {
    family: String;
    products : product[]
}

export class product {
    _id?: String;
    name: String = "";
    description: String = "";
    cco_gm: String = "";
    upstream: String = "";
}