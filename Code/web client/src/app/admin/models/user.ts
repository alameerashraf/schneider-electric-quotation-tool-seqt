export class user {
    isActive?: boolean;
    name?: string;
    sesa?: string;
    email?: string;
    department?: string;
    managerName?: string;
    managerSESA?: string;
    delegatedFrom?: string;
    roles?: string;
    offers?: string;
    isSelected?: boolean = false;
};
