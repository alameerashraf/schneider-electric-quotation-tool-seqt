export class employee {
    name: string = "";
    sesa: string = "SESA";
    email: string = "@se.com";
    department: string = "";
    manager: string = "";
    subOrdinates: any = [];
    roles: any = [];
    isActive: boolean = false;
};
