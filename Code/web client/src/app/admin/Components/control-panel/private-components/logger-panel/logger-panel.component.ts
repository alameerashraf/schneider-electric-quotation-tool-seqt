import { Component, OnInit } from '@angular/core';
import {  logger } from '../../../../models/logger';
import { httpService, urls, responseModel, constants } from '../../../../../@core';


@Component({
  selector: 'logger-panel',
  templateUrl: './logger-panel.component.html',
  styleUrls: ['./logger-panel.component.scss']
})

export class LoggerPanelComponent implements OnInit {
  userActions: logger[] = [];
  usersSESA: string[] = [];
  selectedUserSESA = "";

  selectedPage;
  pageSize = 5;
  limit: number;
  skip: number;


  constructor(
    private httpService: httpService
  ) { }

  ngOnInit() {
    this.initUsersSESAs();
    this.pageChange(1)
  }


  initUsersSESAs(){
    let loadUsersURL = `${urls.LOAD_ALL_USERS}`;
    this.httpService.Get(loadUsersURL, {} ).subscribe((response: responseModel) =>{
      if(!response.error){
        let users = response.data;
        users.forEach(user => {
          this.usersSESA.push(user.sesa);
        });
      }
    })
  };

  usersSESASelected(usersesa){
    let logsURL = `${urls.LOAD_LOGS_FOR_ACTIONS}/${usersesa}`;
    this.httpService.Get(logsURL , {}).subscribe((response: responseModel) => {
      if(!response.error){
        this.userActions = response.data;
        this.userActions.forEach(log => {
          log.createdAt = new Date(log.createdAt);
          log.actionColor = constants.LOG_ACTIONS_COLORS[log.action];
        });
      }
    })
  }

  pageChange(pageNumber){
    this.limit = this.pageSize * pageNumber;
    this.skip = Math.abs(this.pageSize - this.limit);
    this.selectedPage = pageNumber;
  }

}
