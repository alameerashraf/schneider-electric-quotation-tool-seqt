import { Component, OnInit, Inject } from '@angular/core';
import { employee } from '../../../../models/employee';
import { urls, httpService, responseModel, localStorageService } from '../../../../../@core';
import { NbToastrService } from '@nebular/theme';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, NgModel } from '@angular/forms';

@Component({
  selector: 'create-new-user',
  templateUrl: './create-new-user.component.html',
  styleUrls: ['./create-new-user.component.scss']
})
export class CreateNewUserComponent implements OnInit {
  newUser: employee = new employee();
  errors: any = {};

  managers = [];
  departments = [
    "Hub Tendering",
    "Hub Execution",
    "RAC Tendering",
    "RAC Execution",
    "Services Tendering",
    "Services Execution",
    "Sales Oil & Gas",
    "Sales Utility",
    "Sales WWW",
    "Sales MMM",
    "Sales GI",
    "Sales Building",
    "Sales Infrastructure",
    "Sales Export"
  ];

  subOrdinatesSettings = {
    idField: '_id',
    textField: 'sesa',
    selectAllText: 'Select All',
    unSelectAllText: 'Remove All',
    itemsShowLimit: 2,
    allowSearchFilter: true,
    enableCheckAll: false,
    disabled: false
  };
  selectedSubOrdinates = [];
  allPossibleSubordinates = [];

  rolesSettings = {
    idField: '_id',
    textField: 'displayName',
    selectAllText: 'Select All',
    unSelectAllText: 'Remove All',
    itemsShowLimit: 2,
    allowSearchFilter: true,
    enableCheckAll: false
  };
  multipleSelectValue = [];
  rolesPerUser = [];
  allPossibleRoles = [];
  loggedInUserSESA: any;
  creationMode: string;
  currentUserSESA: any;


  constructor(
    private httpService: httpService,
    private localStorageService: localStorageService,
    private toaster: NbToastrService,
    private dialogRef: MatDialogRef<CreateNewUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    if (this.data != null) {
      this.creationMode = "UPDATE";
      this.newUser = this.data;
      this.currentUserSESA = this.data.sesa;
    }


    this.loadManagers();
    this.loadEngineers();
    this.loadRoles();

    this.loggedInUserSESA = this.localStorageService.getLocalStorage("loggedInUser")["userSESA"];
  }

  //#region Loaders
  loadRoles() {
    let rolesURL = `${urls.GET_ROLES}`;
    this.httpService.Get(rolesURL, {}).subscribe((response: responseModel) => {
      if (!response.error) {
        this.allPossibleRoles = response.data;
        if (this.data != null) {
          let selectedRoles = [];
          this.data.roles.forEach(element => {
            selectedRoles.push(element.name);
          });

          this.multipleSelectValue = this.rolesPerUser = selectedRoles;
        }
      }
    })
  };

  loadManagers() {
    let managersURL = `${urls.GET_ALL_MANAGERS}`;
    this.httpService.Get(managersURL, {}).subscribe((response: responseModel) => {
      if (!response.error) {
        this.managers = response.data;
        if (this.data != null) {
          if (this.data["manager"] != undefined)
            this.newUser.manager = this.data["manager"]._id;
        }
      }
    })
  };

  loadEngineers() {
    let engineersURL = `${urls.GET_ALL_EMPLOYEES}`;
    this.httpService.Get(engineersURL, {}).subscribe((response: responseModel) => {
      if (!response.error) {
        this.allPossibleSubordinates = response.data;
        if (this.data != null) {
        }
      }
    })
  };
  //#endregion

  //#region  Selectors for multi-select 
  onSubOrdinatesSelect(selected) {
    let selectedEmployee = selected._id;
    this.newUser.subOrdinates.push(selectedEmployee);
  };

  onSubOrdinatesSelectAll(selected) {
    this.allPossibleSubordinates.forEach((subOrdinate) => {
      let selectedRole = subOrdinate._id;
      this.newUser.roles.push(selectedRole);
    })
  };

  onRolesSelect(selected) {
    let selectedRole = selected._id;
    this.newUser.roles.push(selectedRole);
  };

  onRolesSelectAll(selected) {
    this.allPossibleRoles.forEach((role) => {
      let selectedRole = role._id;
      this.newUser.roles.push(selectedRole);
    })
  };
  //#endregion

  constructRoles() {
    let userRoles = [];
    this.multipleSelectValue.forEach((roleName) => {
      let role = this.allPossibleRoles.find(x => x.name == roleName);
      userRoles.push(role);
    })

    return userRoles;
  }


  save() {

    let createNewUserURL = `${urls.CREATE_NEW_USER}`;

    this.newUser.roles = this.constructRoles();

    let isUserValid = this.validate();

    if (isUserValid) {
      if (this.newUser.manager == "") {
        delete this.newUser.manager;
      }
      this.httpService.Post(createNewUserURL, {}, this.newUser).subscribe((response: responseModel) => {
        if (!response.error) {
          this.logAdminAction("CREATE");
          this.dialogRef.close(true);
          this.toaster.success("New user has been saved successfully", "Data Saved successfully", {
            destroyByClick: true,
            hasIcon: true
          });
        }
      })
    }
  };

  update() {
    let updateUserURL = `${urls.UPDATE_USER}/${this.currentUserSESA}`;

    this.newUser.roles = this.constructRoles();
    if (this.newUser.manager == "") {
      delete this.newUser.manager;
    }

    let isUserValid = this.validate();

    if (isUserValid) {
      this.httpService.Post(updateUserURL, {}, this.newUser).subscribe((response: responseModel) => {
        if (!response.error) {
          this.logAdminAction("UPDATE");
          this.dialogRef.close(true);
          this.toaster.success("New user has been updated successfully", "Data Saved successfully", {
            destroyByClick: true,
            hasIcon: true
          });
        }
      })
    }
  };

  logRoleChange() {
    this.errors["roles"] = false;

    let grantedRoles = this.multipleSelectValue.filter(x => !this.rolesPerUser.includes(x));
    let revokedRoles = this.rolesPerUser.filter(x => !this.multipleSelectValue.includes(x));

    let listOfActions = [];


    if (grantedRoles.length > 0) {
      grantedRoles.forEach((role) => {
        let newAction = {};
        newAction["takenBy"] = this.loggedInUserSESA;
        newAction["affectedUser"] = this.newUser.sesa;
        newAction["action"] = "GRANT";
        newAction["entity"] = "ROLE";
        newAction["additionalInfo"] = role;

        listOfActions.push(newAction)
      });
    }

    if (revokedRoles.length > 0) {
      revokedRoles.forEach((role) => {
        let newAction = {};
        newAction["takenBy"] = this.loggedInUserSESA;
        newAction["affectedUser"] = this.newUser.sesa;
        newAction["action"] = "REVOKE";
        newAction["entity"] = "ROLE";
        newAction["additionalInfo"] = role;

        listOfActions.push(newAction)
      });
    }

    return listOfActions;
  }

  onEmployeeFullNameKeyPress(event) {
    this.errors["name"] = false;
  }

  onEmployeeSesaKeyPress(event) {
    this.errors["sesa"] = false;
  }

  onEmployeeEmailKeyPress(event) {
    this.errors["email"] = false;
  }

  onEmployeeDepartmentSelected(event) {
    this.errors["department"] = false;
  }


  validate() {
    let errorCheck = false;
    if (this.newUser.name == "") {
      this.errors["name"] = true;
      errorCheck = true;
    }

    if (this.newUser.sesa == "") {
      this.errors["sesa"] = true;
      errorCheck = true;
    }

    if (/^(SESA)+([0-9]+)$/.test(this.newUser.sesa) == false) {
      this.errors["sesa"] = true;
      errorCheck = true;
    }

    if (this.newUser.email == "") {
      this.errors["email"] = true;
      errorCheck = true;
    }

    if (/^[a-zA-Z._-]+@?(se.com|non.se.com)$/
      .test(this.newUser.email.toLowerCase()) == false) {
      this.errors["email"] = true;
      errorCheck = true;
    }

    if (this.newUser.department == "") {
      this.errors["department"] = true;
      errorCheck = true;
    }

    if (this.newUser.roles.length == 0) {
      this.errors["roles"] = true;
      errorCheck = true;
    }

    if (errorCheck) {
      return false;
    }

    return true;

  };

  logAdminAction(action) {
    let actions = [];
    let move = {}
    if (action == "CREATE") {
      move = {
        "action": "CREATE",
        "entity": "USER",
        "takenBy": this.loggedInUserSESA,
        "affectedUser": this.newUser.sesa
      }

      actions.push(move);
    }
    if (action == "UPDATE") {
      move = {
        "action": "UPDATE",
        "entity": "USER",
        "takenBy": this.loggedInUserSESA,
        "affectedUser": this.newUser.sesa
      }

      actions.push(move);
    }

    actions = actions.concat(this.logRoleChange());


    let logURL = `${urls.LOG_ADMIN_ACTION}`;
    this.httpService.Post(logURL, {}, actions).toPromise();
  }

};
