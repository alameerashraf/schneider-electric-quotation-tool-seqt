import { Component, OnInit, Inject } from '@angular/core';
import { urls, httpService, constants, responseModel } from '../../../../../@core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss']
})
export class UploadFilesComponent implements OnInit {
  numberOfFiles: any;
  isUploading: boolean;

  uploadingHistory = [
    {
      uploadedBy: 'SESA489756' ,
      createdAt: new Date().toDateString(),
    },
  ];

  selectedPage: any;
  pageSize: any = 3;
  limit: number;
  skip: number;
  
  uploadingType;
  url;
  sheetName: any;

  constructor(private httpService: httpService,
    private dialogRef: MatDialogRef<UploadFilesComponent>,
    private toaster: NbToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.pageChange(1)
  }

  ngOnInit() {
    this.getUploadingType();
  }

  getUploadingType(){
    this.uploadingType = this.data.uploadingName;
    this.url = this.data.url;
    this.sheetName = this.data.sheetName;
  }

  onFileChanged(){

  }

  upload(files) {
    this.numberOfFiles = files.length;
    if (files.length === 0) {
      return;
    };

    this.isUploading = true;

    const formData = new FormData();
    for (let file of files) {
      let fileName = `${this.sheetName}`;
      formData.append(fileName, file);
    };

    let uploadingUrl = this.url;

    this.httpService.Post(uploadingUrl, {} , formData).subscribe((response: responseModel) => {
      if(!response.error){
        this.toaster.success(`${this.uploadingType} has been uploaded successfully` , "File uploaded successfully" , {
          destroyByClick: true,
          hasIcon: true
        });

        this.dialogRef.close(true);
      } else {
        this.toaster.danger("Upload failes unexpectedly" , "Something went wrong on the server" , {
          destroyByClick: true,
          hasIcon: true
        });
      }
    });
  };

  dismiss(){
    this.dialogRef.close();
  };

  pageChange(pageNumber){
    this.limit = this.pageSize * pageNumber;
    this.skip = Math.abs(this.pageSize - this.limit);
    this.selectedPage = pageNumber;
  }

}
