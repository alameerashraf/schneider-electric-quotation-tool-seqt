import { Component, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { MatDialog } from '@angular/material';
import { ReportsComponent } from './../reports/reports.component';
import { UploadFilesComponent } from '../upload-files/upload-files.component';
import { constants } from '../../../../../@core';
import { FamiliesComponent } from '../families/families.component';

@Component({
  selector: 'manage-panel',
  templateUrl: './manage-panel.component.html',
  styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit {
  adminActions = [
    {
      text: 'Upload Expenses File',
      icon: 'layers-outline',
      color: 'info',
      handler: (): any => {
        this.openUploadExpensesCategoriesModal()
      }
    }, 
    {
      text: 'Upload Families Master File',
      icon: 'list-outline',
      color: 'info',
      handler: (): any => {
        this.openUploadFamilyModal()
      }
    },
    {
      text: 'Upload Exchange Rate',
      icon: 'credit-card-outline',
      color: 'info',
      handler: ():any => {
        this.openUploadExchangeRateModal()
      }
    },
    {
      text: 'Modify Families & Products',
      icon: 'layout-outline',
      color: 'info',
      handler: (): any => {
        this.modifyFamilies()
      }
    },
    {
      text: 'Extract Reports',
      icon: 'bar-chart-outline',
      color: 'info',
      handler: ():any => {
        this.openExportReportsModal()
      }
    }
  ]

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openUploadExpensesCategoriesModal(){
    const dialogRef =  this.dialog.open(UploadFilesComponent , {data: constants.ADMIN_UPLOADS_TYPES["expenses"]});
    dialogRef.afterClosed().subscribe((isExpensesUploaded) => {
    })
  };

  openExportReportsModal(){
    const dialogRef = this.dialog.open(ReportsComponent);
    dialogRef.afterClosed().subscribe((isDone) => {
    })
  };

  openUploadFamilyModal(){
    const dialogRef =  this.dialog.open(UploadFilesComponent , { data: constants.ADMIN_UPLOADS_TYPES["families"] });
    dialogRef.afterClosed().subscribe((isExpensesUploaded) => {
    })
  };

  openUploadExchangeRateModal(){
    const dialogRef =  this.dialog.open(UploadFilesComponent , { data: constants.ADMIN_UPLOADS_TYPES["rates"] });
    dialogRef.afterClosed().subscribe((isExpensesUploaded) => {
    })
  };

  modifyFamilies(){
    const dialogRef =  this.dialog.open(FamiliesComponent);
    dialogRef.afterClosed().subscribe((isUpdated) => {
    })
  };
}
