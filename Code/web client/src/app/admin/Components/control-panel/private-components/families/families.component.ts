import { Component, OnInit } from '@angular/core';
import { family, product } from '../../../../models/family';
import { httpService, urls, responseModel } from '../../../../../@core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { ModalBodyComponent } from '../../../../../@theme/components';

@Component({
  selector: 'families',
  templateUrl: './families.component.html',
  styleUrls: ['./families.component.scss']
})
export class FamiliesComponent implements OnInit {
  isDataSaved = true;
  familyNames = [];
  selectedFamily = "";
  changedFamilyName = "";
  families: family[] = [];
  products: product[] = [
  ];

  selectedPage;
  pageSize = 4;
  limit: number;
  skip: number;


  constructor(private httpService: httpService,
    private dialogeService: NbDialogService,
    private toaster: NbToastrService) { }

  ngOnInit() {
    this.loadFamilyNames();
  };

  loadFamilyNames(){
    let loadFamiliesURL = `${urls.LOAD_ALL_FAMILIES}`;
    this.httpService.Get(loadFamiliesURL , {}).subscribe((response: responseModel) => {
      if(!response.error){
        let familiesData = this.families =  response.data as family[];
        familiesData.forEach((family) => {
          this.familyNames.push(family.family);
        })
      }
    })
  };

  pageChange(pageNumber){
    this.limit = this.pageSize * pageNumber;
    this.skip = Math.abs(this.pageSize - this.limit);
    this.selectedPage = pageNumber;
  };

  removeProduct(id){
    let selectedFamily = this.families.find(x => x.family == this.selectedFamily);
    let elementToBeRemoved = selectedFamily.products.find(x => x._id == id);

    selectedFamily.products.splice(selectedFamily.products.indexOf(elementToBeRemoved) , 1);
  };

  createProduct(){
    if(this.selectedFamily == ""){
      this.toaster.warning("Select any family to add new product" , "Missing family selection" , {
        destroyByClick: true,
        hasIcon: true
      })
    } else {
      let selectedFamily = this.families.find(x => x.family == this.selectedFamily);
      selectedFamily.products.push(new product());
    }
  }

  changeFamily(familyName){
    
    if(!this.isDataSaved){
      this.dialogeService.open(ModalBodyComponent, {
        context: {
          title: "<i class='fa fa-check-square' aria-hidden='true'></i> &nbsp; Discard changes",
          message: `Any changes to the current family will not be applied, click save to update.`
        }
      }).onClose.subscribe((confirmation) => {
        if(confirmation){
          this.isDataSaved = false;
          this.selectFamily(familyName);
        }
      })
    } else {
      this.isDataSaved = false;
      this.selectFamily(familyName);
    }
  };

  selectFamily(familyName){
    let selectedFamily = this.families.find(x => x.family == familyName);
    this.products = selectedFamily.products;
    this.selectedFamily = familyName;
    this.changedFamilyName = familyName;

    this.pageChange(1);
  };

  save(){
    let updateFamilyurl =`${urls.UPDATE_FAMILY}/${this.selectedFamily}`;
    let familyToBeUpdated = this.families.find(x => x.family == this.selectedFamily);
    familyToBeUpdated.family = this.changedFamilyName;


    this.httpService.Post(updateFamilyurl , {} , familyToBeUpdated).subscribe((response: responseModel) => {
      if(!response.error){
        this.isDataSaved = true;
        this.toaster.success("Family has been updated successfully" , "Data has been saved" , {
          destroyByClick: true,
          hasIcon: true
        });
      }
    })
  };
}
