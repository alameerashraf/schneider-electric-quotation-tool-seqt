import { Component, OnInit } from '@angular/core';
import { user } from '../../../../models/user';
import { httpService, urls, responseModel } from '../../../../../@core';
import { MatDialog } from '@angular/material';
import { CreateNewUserComponent } from '../create-new-user/create-new-user.component';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { ModalBodyComponent } from '../../../../../@theme/components';
import { DelegateRoleComponent } from '../delegate-role/delegate-role.component';

@Component({
  selector: 'users-panel',
  templateUrl: './users-panel.component.html',
  styleUrls: ['./users-panel.component.scss']
})
export class UsersPanelComponent implements OnInit {
  adminActions = [
    {
      toolTipText: 'Create new user',
      icon: 'person-add',
      color: 'primary',
      handler: (): any => {
        this.openCreateNewUserModal();
      }
    }, 
    {
      toolTipText: 'Update Selected User',
      icon: 'edit-2-outline',
      color: 'primary',
      handler: (): any => {
        if(this.selectedUsers.length == 1){
          this.openEditSelectedUser();
        } else {
          this.toaster.warning('Select only one user from the list', 'Select single user to update' , {
            destroyByClick: true,
            hasIcon: true
          })
        }
      }
    },
    {
      toolTipText: 'Delegate Roles Between Selected Users',
      icon: 'people',
      color: 'info',
      handler: (): any => {
        if(this.selectedUsers.length == 2){
          this.openDelegateRoleModal();
        } else {
          this.toaster.warning('Two (2) users should be selected to delegate a role', 'Select two users to delegaet a role' , {
            destroyByClick: true,
            hasIcon: true
          })
        }
      }
    },
    {
      toolTipText: 'Delete Selected Users',
      icon: 'trash-2-outline',
      color: 'danger',
      handler: (): any => {
        if(this.selectedUsers.length > 0){
          this.openDeleteConfirmationModal()
        } else {
          this.toaster.warning('At least one user should be selected', 'Select any user to delete' , {
            destroyByClick: true,
            hasIcon: true
          })
        }
      }
    },
  ];
  users:user[] = [];
  selectedUsers = [];


  selectedPage;
  pageSize=6;
  limit: number;
  skip: number;

  constructor(
    private httpService: httpService,
    private dialog: MatDialog,
    private dialogeService: NbDialogService,
    private toaster: NbToastrService
  ) { }

  ngOnInit() {
    this.loadSystemUsers();
    this.pageChange(1);
  }

  loadSystemUsers(){
    let loadAllUsersURL = `${urls.LOAD_ALL_USERS}`;
    this.httpService.Get(loadAllUsersURL , {}).subscribe((response: responseModel) => {
      if(!response.error){
        let users = response.data;
        users.forEach(singleUser => {
          let newUser = new user();
          if(singleUser["manager"] != undefined){
            newUser.managerName = singleUser["manager"].name;
            newUser.managerSESA = singleUser["manager"].sesa;
          }

          newUser = singleUser;
          this.users.push(newUser);
        });
      }
    })
  }

  pageChange(pageNumber){
    this.limit = this.pageSize * pageNumber;
    this.skip = Math.abs(this.pageSize - this.limit);
    this.selectedPage = pageNumber;
  }

  userSelected(userSESA){
    let selectedUser = this.users.find(x => x.sesa == userSESA);

    selectedUser.isSelected = !selectedUser.isSelected;

    if(selectedUser.isSelected){
      this.selectedUsers.push(selectedUser);
    } else {
      this.selectedUsers.splice(this.selectedUsers.indexOf(selectedUser) , 1);
    }
  }


  //#region Actions Modals 
  openCreateNewUserModal(){
    const dialogRef = this.dialog.open(CreateNewUserComponent);
    dialogRef.afterClosed().subscribe((isCreated) => {
      if(isCreated){
        this.users = [] ;
        this.selectedUsers = [];
        this.loadSystemUsers();
        this.pageChange(1);
      }
    })
  };

  openEditSelectedUser(){
    const dialogRef = this.dialog.open(CreateNewUserComponent , { data: this.selectedUsers[0] });
    dialogRef.afterClosed().subscribe((isUpdated) => {
      if(isUpdated){
        this.users = [] ;
        this.selectedUsers = [];
        this.loadSystemUsers();
        this.pageChange(1);
      }
    })

  }

  //TODO: Receive an array of SESAs and delete it once from server.
  deleteSelectedUser(userSESA){
    let deleteUserURL = `${urls.DELETE_USER}/${userSESA}`;
    this.httpService.Post(deleteUserURL , {} , {}).subscribe((response: responseModel) => {
        this.users = [];
        this.selectedUsers = [];
        this.loadSystemUsers();
        this.pageChange(1);
    })
  };

  openDeleteConfirmationModal(){
    this.dialogeService.open(ModalBodyComponent, {
      context: {
        title: "<i class='fa fa-question-circle' aria-hidden='true'></i> &nbsp; Delete User",
        message: `Are you sure you want to delete the selected user? <br> users will be deleted permenantly.`
      }
    }).onClose.subscribe((isConfirmed) => {
      if(isConfirmed){
        this.selectedUsers.forEach((user) => {
          this.deleteSelectedUser(user.sesa);

        });
      }
    })
  };
  
  openDelegateRoleModal(){
    const dialogRef =  this.dialog.open(DelegateRoleComponent , { data: this.selectedUsers });
    dialogRef.afterClosed().subscribe((isDone) => {
      if(isDone){
        this.users = [] ;
        this.selectedUsers = [];
        this.loadSystemUsers();
        this.pageChange(1);
      }
    })
  }
}
