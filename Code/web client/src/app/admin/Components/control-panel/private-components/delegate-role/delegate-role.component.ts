import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { httpService, urls, responseModel, localStorageService } from '../../../../../@core';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-delegate-role',
  templateUrl: './delegate-role.component.html',
  styleUrls: ['./delegate-role.component.scss']
})
export class DelegateRoleComponent implements OnInit {
  users = [];
  fromUser = "";
  toUser = "";
  loggedInUserSESA: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef:MatDialogRef<DelegateRoleComponent>,
    private httpService: httpService,
    private toaster: NbToastrService,
    private localStorageService: localStorageService
  ) { }

  ngOnInit() {
    this.data.forEach(element => {
      this.users.push(element.sesa);
    });

    this.loggedInUserSESA = this.localStorageService.getLocalStorage("loggedInUser")["userSESA"];

  }

  delegate() {
    let isSelectionValid = this.validate();
    if (isSelectionValid) {
      let delegateRoleURL = `${urls.DELEGATE_ROLE}`;
      this.httpService.Post(delegateRoleURL, {}, {
        fromUserSesa: this.fromUser,
        toUserSesa: this.toUser
      }).subscribe((response: responseModel) => {
        if (!response.error) {
          this.dialogRef.close(true);
          this.logActions();
        }
      })
    }
  };

  validate(){
    if(this.fromUser == "" || this.toUser == ""){
      this.toaster.danger("Source and destination users should be specified" , "Select from and to user" , {
        destroyByClick: true,
        hasIcon: true
      })

      return false;
    }else if(this.fromUser == this.toUser){
      this.toaster.danger("Source and destination users shouldn't be identical" , "Select differnet users" , {
        destroyByClick: true,
        hasIcon: true
      })

      return false;
    } else {
      return true;
    }

  };

  logActions(){
    let actions = [{
      "action": "DELEGATE",
      "entity": "ROLE",
      "takenBy": this.loggedInUserSESA,
      "affectedUser": this.fromUser,
      "additionalText" : `to user ${this.toUser}`
    }, {
      "action": "DELEGATE",
      "entity": "ROLE",
      "takenBy": this.loggedInUserSESA,
      "affectedUser": this.toUser,
      "additionalText" : `from user ${this.fromUser}`
    }];

    let logURL = `${urls.LOG_ADMIN_ACTION}`;
    this.httpService.Post(logURL , {} ,  actions).toPromise();
  }
}
