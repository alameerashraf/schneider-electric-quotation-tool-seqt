import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import * as adminComponents from './Components';

const routes: Routes = [
  {
    path: 'control-panel',
    component: adminComponents.ControlPanelComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
