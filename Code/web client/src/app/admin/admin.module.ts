import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { NBULAR_THEME , NgxMask , MAT_MODAL , NgbModules  } from './admin.imports';
import { ThemeModule } from './../@theme/theme.module';

import * as adminComponents from './Components';

import {
  objectTransfer,
  httpService,
  localStorageService,
  lookupsService,
  excelService,
  headerInterceptor
} from '../@core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [
    adminComponents.ControlPanelComponent,
    adminComponents.ManagePanelComponent,
    adminComponents.UsersPanelComponent,
    adminComponents.ReportsComponent,
    adminComponents.LoggerPanelComponent,
    adminComponents.CreateNewUserComponent,
    adminComponents.UploadFilesComponent,
    adminComponents.DelegateRoleComponent,
    adminComponents.FamiliesComponent
  ],
  entryComponents: [
    adminComponents.ControlPanelComponent,
    adminComponents.LoggerPanelComponent,
    adminComponents.ReportsComponent,
    adminComponents.UploadFilesComponent,
    adminComponents.CreateNewUserComponent,
    adminComponents.DelegateRoleComponent,
    adminComponents.FamiliesComponent
  ],
  imports: [
    MAT_MODAL,
    ThemeModule,
    NgxMask.forRoot(),
    ...NBULAR_THEME,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgbModules,
    CommonModule,
    AdminRoutingModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [
    httpService,
    localStorageService,
    lookupsService,
    excelService,
    { provide: HTTP_INTERCEPTORS , useClass : headerInterceptor , multi : true  },
  ]
})
export class AdminModule { }
