import { Component, OnInit } from '@angular/core';
import { objectTransfer } from './../../../@core';

@Component({
  selector: 'vertical-stepper',
  templateUrl: './vertical-stepper.component.html',
  styleUrls: ['./vertical-stepper.component.scss']
})
export class VerticalStepperComponent implements OnInit {

  steps = [
    {
      isActive: false,
      name: "metadata",
      title: 'Offer metadata'
    },
    {
      isActive: false,
      name: "configure",
      title: 'Offer configuration'
    },
    {
      isActive: false,
      name: "submit",
      title: 'Submit offer'
    }
  ]

  constructor(
    private objectTransfer: objectTransfer
  ) { }

  ngOnInit() {
    this.objectTransfer.objectSubscriber.subscribe((object) => {
      this.steps.find(x => x.name == object.name).isActive = true;
    })
  }

}
