import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'modal-body',
  templateUrl: './modal-body.component.html',
  styleUrls: ['./modal-body.component.scss']
})
export class ModalBodyComponent implements OnInit {
  @Input() title: string;
  @Input() message: string;


  constructor(protected ref: NbDialogRef<ModalBodyComponent>) { }

  ngOnInit() {
  }

  
  cancel() {
    this.ref.close();
  }

  submit(confirmation) {
    this.ref.close(confirmation);
  };

}
