
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http/http';
import { Observable } from 'rxjs';
import { localStorageService } from './../../services/local-storage/local-storage';
import { constants } from './../../helpers/constants/constants';


@Injectable()
export class headerInterceptor implements HttpInterceptor {
    loggedInUser: any;

    constructor(private localStorageService: localStorageService){
        
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.loggedInUser = this.localStorageService.getLocalStorage("loggedInUser");

        let access_token = "";
        if(typeof this.loggedInUser == "undefined")
            access_token = "";
        else 
            access_token = this.loggedInUser.token || "";

        req = req.clone({
            headers : req.headers.set('access_token' , access_token)
        });

        return next.handle(req);
    }
}