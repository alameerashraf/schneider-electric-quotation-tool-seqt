import { Inject, Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { log , urls , constants } from '../helpers';


@Injectable()
export class AppConfig {
  private config: Object = null;
  private const: Object = null;
  private env: Object = null;

  constructor(private http: HttpClient) {
    urls._config = this;
    constants._config = this;
  }

  public getConfig(key: any) {
    return this.config[key];
  }

  public getConst(key: any) {
    return this.const[key];
  }

  public getEnv(key: any) {
    return this.env[key];
  }

  public load(){
    return new Promise((resolve , reject) => {
      this.http.get('env.json')
      .subscribe((envResponse: any) => {
          this.env = envResponse;
          let configRequest:Observable<any> = null;
          let constRequest:Observable<any> = null;

          configRequest = this.http.get(`config.${envResponse.env}.json`);
          constRequest = this.http.get(`const.${envResponse.env}.json`);

          if(configRequest){
              configRequest.subscribe((resposeData) => {
                  this.config = resposeData;
                  resolve(true);
              }, (error: any) => {
                  log.log(error , 'error' , 'load.configRequest' , 'both');
              });
          }

          if(constRequest){
            constRequest.subscribe((resposeData) => {
                this.const = resposeData;
                resolve(true);
            }, (error: any) => {
              log.log(error , 'error' , 'load.constRequest' , 'both');
            });
          }
      })
    })
  }
}
