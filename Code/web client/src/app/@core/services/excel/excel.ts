import { Injectable } from '@angular/core';
import * as xlsx from 'xlsx';
import { constants } from '../../helpers';


type AOA = any[][];
@Injectable()
export class excelService {
    data: AOA = [];
    fileName: string = "";
    workSheet: xlsx.WorkSheet = null;


    /**
     * Get the target work sheet from the uploaded file.
     * @param changeEvent 
     * @param sheetName 
     */
    initReader(changeEvent, sheetName,  requestedDataType , params?) {
        const target: DataTransfer = <DataTransfer>(changeEvent.target);
        if (target.files.length !== 1)
            throw new Error("Cann't use multiple files.");

        const reader: FileReader = new FileReader();
        reader.readAsBinaryString(target.files[0]);

        return new Promise<any>((resolve , reject) => {
            reader.onload = ((e: any) => {
                const bstr = e.target.result;
                const workBook = xlsx.read(bstr, { type: 'binary' });
    
                // Search by sheet name:
                const sheets = workBook.SheetNames;
                const ws: xlsx.WorkSheet = workBook.Sheets[sheetName];
    
                // init work sheet
                this.workSheet = ws;
                
                if(requestedDataType == constants.EXCEL_REQUESTD_DATA_TYPES.CELL_VALUE){
                    let result = this.getMultipleCells(params);
                    resolve(result);
                }

                if(requestedDataType == constants.EXCEL_REQUESTD_DATA_TYPES.ROWS_AND_COLS){
                    let data = this.getRowsAndColumns();
                    resolve(data);
                }

                else 
                    reject(0);
            })
        })
    };

    /**
     * return all the data.
     */
    getRowsAndColumns(): any {
        return <AOA>(xlsx.utils.sheet_to_json(this.workSheet, { header: 1 }));
    };

    /**
     * return the cell value.
     * @param cell 
     */
    getCellValue(cell): string{
        if(cell != "")
            return this.workSheet[cell];
        else 
            return "";
    };

    getMultipleCells(arrayOfCells: string[]): any {
        const cellValues = {};
        arrayOfCells.forEach(element => {
            cellValues[element] = this.getCellValue(element);
        });

        return cellValues;
    }
};

