import { AppConfig } from '../../config';



export class urls{
    public static _config: AppConfig;
    public static get BASE_URL(): string { return  urls._config.getConfig("BASE_URL")};
    public static get SERVER_URL(): string { return  urls._config.getConfig("SERVER_URL")};
    public static get PING(): string { return urls._config.getConfig("SSO_PING") };

    //SSO
    public static get SSO(): string { return this.PING + '/Home/auth?returnUrl=' };

    // utilities 
    public static get UTILITY() :string { return '/env' };


    // internal routes 
    public static get USER_DASHBOARD_INTERNAL() :string { return 'users/dashboard' };


    // API Controllers..
    public static get LOAD_DATA_FROM_BFO(): string { return this.BASE_URL + '/bFO/load-opportunity-data' };

    // offers URLs 
    public static get POST_OFFER_TO_SERVER(): string { return this.BASE_URL + '/offer/create' };
    public static get ASSIGN_OFFER(): string { return this.BASE_URL + '/offer/assign' };
    public static get GET_OFFER(): string { return this.BASE_URL + '/offer/load' };
    public static get GET_LATEST_OFFER_NUMBER(): string { return this.BASE_URL + '/offer/latest-offer-number' };
    public static get SUBMIT_OFFER(): string {return this.BASE_URL + '/offer/submit-offer'};
    public static get STARTING_APPROVAL_CYCLE(): string {return this.BASE_URL + '/offer/starting-approval-cycle'};
    public static get APPROVE_OFFER(): string {return this.BASE_URL + '/offer/approve'};
    public static get REJECT_OFFER(): string {return this.BASE_URL + '/offer/reject'};
    


    // users URLs
    public static get CREATE_NEW_USER(): string { return this.BASE_URL + '/user/create' };
    public static get GET_USERS_SUBORDINATES(): string { return this.BASE_URL + '/user/subordinates' };
    public static get GET_USERS_ROLES(): string { return this.BASE_URL + '/user/roles' };
    public static get UPDATE_USER_KPIS(): string { return this.BASE_URL + '/user/update/kpis' };
    public static get GET_ALL_TENDERING_ENGINEERS(): string {return this.BASE_URL + '/user/get-all-tendering-engineers'}
    public static get GET_ALL_MANAGERS(): string {return this.BASE_URL + '/user/load-managers'}
    public static get GET_ALL_EMPLOYEES(): string {return this.BASE_URL + '/user/load-employees'}

    // dashboards URLs
    public static get GET_USERS_OFFERS(): string { return this.BASE_URL + '/dashboard/offers' };
    public static get GET_USERS_KPIS(): string { return this.BASE_URL + '/dashboard/kpis' };
    public static get GET_FILTERS_COUNT(): string { return this.BASE_URL + '/dashboard/filters-count' };
    public static get DASHBOARD_SEARCH(): string { return this.BASE_URL + '/dashboard/search' };


    // Lookups URLs 
    public static get GET_COUNTRIES():string { return this.BASE_URL + '/lookup/countries' };
    public static get GET_EXPENSES():string { return this.BASE_URL + '/lookup/expenses' };
    public static get GET_CURRENCIES():string { return this.BASE_URL + '/lookup/currencies' };
    public static get GET_ROLES():string { return this.BASE_URL + '/lookup/roles' };

    // Offer Items URLs 
    public static get BULK_INSERT_ITEMS_TO_OFFER(): string { return this.BASE_URL + '/item/create-items' };
    public static get LOAD_OFFER_ITEMS(): string { return this.BASE_URL + '/item/load-items' };
    public static get DELETE_ITEMS_FROM_OFFER(): string { return this.BASE_URL + '/item/delete-items' };
    public static get CLONE_ITEMS(): string { return this.BASE_URL + '/item/clone-items' };
    // public static get ADD_EXPENSES_TO_ITEMS(): string { return this.BASE_URL + '/item/add-expenses-to-items' };
    // public static get ADD_EXPENSES_TO_OFFER(): string { return this.BASE_URL + '/item/add-expenses-to-offer' };
    public static get ADD_EXPENSES(): string { return this.BASE_URL + '/item/add-expenses' };
    public static get LOAD_EXPENSES_FOR_ITEMS(): string { return this.BASE_URL + '/item/load-expenses-for-items' };

    // export reports
    public static get EXPORT_REPORTS(): string {return this.BASE_URL + '/exports/export'}
    
    // Admin URLs
    public static get LOAD_ALL_USERS(): string { return this.BASE_URL + '/admin/load-users' };
    public static get LOAD_LOGS_FOR_ACTIONS(): string { return this.BASE_URL + '/admin/logs' };
    public static get LOG_ADMIN_ACTION(): string { return this.BASE_URL + '/admin/log' };
    public static get DELETE_USER(): string { return this.BASE_URL + '/admin/delete-user' };
    public static get UPDATE_USER(): string { return this.BASE_URL + '/admin/update-user' };
    public static get DELEGATE_ROLE(): string { return this.BASE_URL + '/admin/delegate-role' };
    public static get LOAD_ALL_FAMILIES(): string { return this.BASE_URL + '/admin/load-families' };
    public static get UPDATE_FAMILY(): string { return this.BASE_URL + '/admin/update-family' };


    // Upload Files 
    public static get UPLOAD_COST_FILES(): string { return this.BASE_URL + '/upload/cost-files' };
    public static get UPLOAD_EXPENSES_CATEGORIES(): string { return this.BASE_URL + '/upload/expenses-categories' };
    public static get UPLOAD_FAMILIES_MASTER(): string { return this.BASE_URL + '/upload/families' };
    public static get UPLOAD_EXCHANGE_RATE(): string { return this.BASE_URL + '/upload/exchange-rates' };
    public static get UPLOAD_OFFER_DOCUMENTS(): string { return this.BASE_URL + '/upload/offer-documents' };
}