export class mapper {
  public static map<T>(values: Partial<T>, ctor: new () => T): T {
    const instance = new ctor();

    return Object.keys(instance).reduce((acc, key) => {
      if(values[key] != undefined){
        acc[key] = values[key];
      } else {
        //If the object has a default property value, and it doesn't found in the object we converted from
        acc[key] = instance[key]
      }
      return acc;
    }, {}) as T;
  }
}
