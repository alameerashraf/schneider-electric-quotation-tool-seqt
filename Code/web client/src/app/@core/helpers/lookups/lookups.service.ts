import { Injectable } from '@angular/core';
import { httpService } from './../../services/http/http';
import { urls } from './../urls/urls';

@Injectable({
  providedIn: 'root'
})
export class lookupsService {

  constructor(private httpService: httpService) { }

  async getCountries(){
    return this.httpService.Get(urls.GET_COUNTRIES , {} ).toPromise();
  };
  
}
