import { itemsExpenses, expense, offerItem } from '../../models';
import { constants } from '../../../@core';

export class expensesManagerCalculations {

    // Get the values of expenses per each item 
    calculateGeneralExpensesforEachItem(items: offerItem[], addedExpense: expense, addedExpenseValue , totalSP2_totals=0) {
        let allAddedGeneralExpenses = [];

        items.forEach((item) => {
            let newExpense = new expense();
            newExpense.type = addedExpense.type;
            newExpense.valueType = addedExpense.valueType;
            newExpense.IS_SAVED = false;
            
            if (addedExpense.valueType == "Percentage") {
                newExpense.value = item.weight * ((addedExpenseValue / 100) * totalSP2_totals) // total selling price 
            } else {
                newExpense.value = item.weight * addedExpenseValue;
            }

            let itemExpenses = {};
            itemExpenses["itemNo"] = item.itemNo.value;
            itemExpenses["value"] = (newExpense.value);
            allAddedGeneralExpenses.push(itemExpenses);
        });

        return allAddedGeneralExpenses;
    };


    // Combine all general expense to only one
    combineAllGeneralExpenses(expensesList: expense[]) {
        let combinedExpense = new expense();
        let totalValue = 0;

        expensesList.forEach((expense) => {
            totalValue = totalValue + expense.value;
        });

        combinedExpense.name = "General Expense";
        combinedExpense.type = constants.EXPENSES_TYPES["general"];
        combinedExpense.IS_SAVED = false;
        combinedExpense.valueType = "";
        combinedExpense.value = totalValue;

        return combinedExpense;
    };

    filterExpensesCategories(expensesCategories: expensesCategories[] , availability){
        let itemExpenses = expensesCategories;
        for (let index = 0; index < itemExpenses.length; index++) {
          let expensesList = itemExpenses[index].expensesList;
          itemExpenses[index].expensesList = expensesList.filter((item) => {
            if(availability == "items"){
              return item.itemsAvailability == true
            } else {
              return  item.generalAvailability == true;
            }
          });
        }
        return itemExpenses;
    };
};