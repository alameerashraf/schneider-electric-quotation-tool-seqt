import { constants } from '../../../@core';

export class costFilesItemsCalculations {
    // Caclulate SFC% 
    calculateSFC_Percentage(file, cells) {
        if (file == constants.COST_FILES_NAMES.EAS_BO_1) {
            let H19 = cells["H19"].v;
            let H21 = cells["H21"].v;
            let H16 = cells["H16"].v;


            let sfcValue = ((H19 - H21) / H16) * 100;
            return parseFloat(sfcValue.toFixed(2));
        } else if (file == constants.COST_FILES_NAMES.EAS_AUT) {
            let E63 = cells["E63"].v;
            let E72 = cells["E72"].v;

            let sfcValue = (E63 / E72) * 100;
            return parseFloat(sfcValue.toFixed(2));
        }
    };

    // Calculate Upstream%
    calculateUpstream_Percentage(file, cells) {
        if (file == constants.COST_FILES_NAMES.EAS_BO_1) {
            let I19 = cells["I19"].v;

            let upstreamValue = (I19) * 100;
            return parseFloat(upstreamValue.toFixed(2));
        }
        else if (file == constants.COST_FILES_NAMES.EAS_AUT) {
            let E73 = cells["E73"].v;
            let E72 = cells["E72"].v;

            let upstreamValue = (E73 / E72) * 100;
            return parseFloat(upstreamValue.toFixed(2));
        }
    };

    // conversion rate
    convertCurrencies(items, fileType, conversionRate) {
        let fieldsToBeConverted = [];

        if (fileType == constants.COST_FILES_NAMES["EAS_AUT"] || fileType == constants.COST_FILES_NAMES["EAS_BO_1"]) {
            fieldsToBeConverted = ["unitCostExcludingExpenses" , "totalCostExcludingExpenses" , "unitCostIncludingExpenses"];
        } else {
            fieldsToBeConverted = ["unitCostExcludingExpenses" , "totalCostExcludingExpenses" ];
        }

        items.forEach(element => {
            Object.keys(element).forEach((k) => {
                if(fieldsToBeConverted.includes(k)){
                    element[k] = element[k] * conversionRate;
                };
            })
        });

        return items;
    };

    bindCalculatedFieldsOnCostFilesUpload(items, sfc, upstream) {
        items.forEach((element) => {
            // Calculated fields on files uploded (Upstraeam SFC %, Upstream %)
            element["upstreamSFC_Percentage"] = sfc;
            element["Upstream_Percentage"] = upstream;
            element["unitCostIncludingExpenses"] = element.unitCostExcludingExpenses;
            element["CCOGM_Percentage_Threshold"] = "24";
            element["CCOGM_Percentage"] = "24";
        });
        return items;
    };
};


