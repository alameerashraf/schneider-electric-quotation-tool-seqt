import { offerItem, eas, transferPrice, itemsExpenses } from '../../models';
import * as screenfull from 'screenfull';
import { constants, urls } from '../../../@core';
import { itemsViewModel } from '../../models/itemsViewModel';
import { expense } from '../../models/expense';


export class offerItems {
         //#region objectConversionAndBindings
         convertServerObjectsToViewObjects(items, isSaved, isLoadedFromDb) {
           let convertedItems = [];
           // Convert server object to view object!
           items.forEach((element) => {
             let item = new offerItem();
             delete element["_id"];
             item["IS_SAVED"] = isSaved;
             item["IS_LOADED_FROM_DB"] = isLoadedFromDb;
             item["UPLOADED_FROM"] = element["UPLOADED_FROM"];

             Object.keys(element).forEach((k) => {
               if (!constants.OFFER_ITEMS_UI_HELPER_FIELDS.includes(k)) {
                 if (k == "expenses") {
                   element[k].forEach((expense) => {
                     expense.IS_SAVED = true;
                   });

                   item[k] = item[k].concat(element[k]);
                 } else {
                   item[k].value = element[k];

                   if (isSaved == false && isLoadedFromDb == false) {
                     item[k].isChanged = true;
                   }
                 }
               }
             });

             convertedItems.push(item);
           });

           return convertedItems;
         }

         convertViewObjectToServerObject(offerItems) {
           let convertedItems = [];
           let singleOfferItem = {};

           offerItems.forEach((element) => {
             singleOfferItem = {};
             Object.keys(element).forEach((k) => {
               singleOfferItem[k] = element[k].value;

               if (k == "UPLOADED_FROM") {
                 singleOfferItem[k] = element[k];
               }

               if (k == "expenses") {
                 singleOfferItem[k] = [];
                 singleOfferItem[k] = singleOfferItem[k].concat(element[k]);
               }
             });
             convertedItems.push(singleOfferItem);
           });

           return convertedItems;
         }

         convertGridValuesToNumbers(gridValue) {
           if (gridValue == null || gridValue == "") return 0;
           else return parseFloat(gridValue.toString().replace(",", ""));
         }

         // Bind ids for the cost files uploaded items!
         bindItemsIds(itemsLoadedFormFiles: eas[], latestId) {
           let latestItemId = latestId;
           itemsLoadedFormFiles.forEach((element) => {
             latestItemId = latestItemId + 1;
             element["itemNo"] = latestItemId; // adding itemNo for the new uploaded items!
           });

           return itemsLoadedFormFiles;
         }

         convertLoadedItemsToViewObject(items, typeOfUpload) {
           let itemsArr = [];

           items.forEach((element) => {
             let newItem = {};
             newItem = this.toObject(element, typeOfUpload);
             itemsArr.push(newItem);
           });

           return itemsArr;
         }

         // Convert arry to an object!
         toObject(arr, typeOfUpload) {
           let createdItem = null;

           if (typeOfUpload == constants.COST_FILES_NAMES.Transfer_Price) {
             createdItem = new transferPrice();
           } else if (
             typeOfUpload == constants.COST_FILES_NAMES.EAS_AUT ||
             typeOfUpload == constants.COST_FILES_NAMES.EAS_BO_1
           ) {
             createdItem = new eas();
           }

           var itemsArray = Object.keys(createdItem);
           for (let index = 0; index < arr.length; index++) {
             // index of attribute from object, using array indecies.
             createdItem[itemsArray[index]] = arr[index];
           }
           return createdItem;
         }

         // Build items view model for both the clone modal and expenses manager modal.
         constructItemsViewModel(offerItems: offerItem[]) {
           let itemsViewModelList: itemsViewModel[] = [];
           offerItems.forEach((element) => {
             let item = new itemsViewModel();
             item.itemNo = element.itemNo.value;
             item.description = element.description.value;
             item.isSelected = false;
             item.totalCostExcludingExpenses = +element
               .totalCostExcludingExpenses.value;
             item.weight = element.weight;
             item.expenses = item.expenses.concat(element.expenses);

             itemsViewModelList.push(item);
           });

           return itemsViewModelList;
         }

         bindExpensesToMainGridItems(
           offerItems: offerItem[],
           expensesPerItems: itemsExpenses[]
         ) {
           expensesPerItems.forEach((item) => {
             let targetItem = offerItems.find(
               (x) => x.itemNo.value == item.itemNo
             );
             let expensesToBeAdded = item.expenses.filter((x) => {
               return x.IS_SAVED == false && !x.IS_UPDATED && !x.IS_REMOVED;
             });

             let expensesToBeUpdated = item.expenses.filter((x) => {
               return x.IS_UPDATED == true;
             });

             let expensesToBeDeleted = item.expenses.filter((x) => {
               return x.IS_REMOVED == true;
             });

             // Add new expenses
             targetItem.expenses = targetItem.expenses.concat(
               expensesToBeAdded
             );

             // Update new once
             expensesToBeUpdated.forEach((singleExpense) => {
               let selectedExpense = targetItem.expenses.find(
                 (x) => x.name == singleExpense.name
               );
               selectedExpense.value = singleExpense.value;
             });

             //Remove expenses
             expensesToBeDeleted.forEach((singleExpense) => {
               let expenseToRemove = targetItem.expenses.find(
                 (x) => x.name == singleExpense.name
               );
               targetItem.expenses.splice(
                 targetItem.expenses.indexOf(expenseToRemove),
                 1
               );
             });
           });
         }
         //#endregion

         //#region Utilities
         validateItem(item: offerItem) {
           if (item == null) {
             return false;
           }
           if (
             item.description.value == "" ||
             item.family.value == "" ||
             item.product.value == "" ||
             item.quantity.value == 0 ||
             item.unitCostExcludingExpenses.value == "" ||
             item.totalCostExcludingExpenses.value == "" ||
             item.IG_OG.value == ""
           ) {
             return false;
           } else {
             return true;
           }
         }

         goFullScreen() {
           document
             .getElementById("full-screen-btn")
             .addEventListener("click", () => {
               let element = document.getElementById("table-wrapper");

               if (screenfull.isEnabled) {
                 screenfull.toggle(element);
               }
             });
         }

         // return Next item  id !
         getNextItemId(items: offerItem[]) {
           if (items.length == 0) {
             return 1;
           } else {
             items.sort();
             return items[items.length - 1].itemNo.value + 1;
           }
         }

         // return latest item id!
         getLatestItemId(items: offerItem[]) {
           if (items.length == 0) {
             return 0;
           } else {
             items.sort();
             return items[items.length - 1].itemNo.value;
           }
         }

         validateUploadedFiles(addedObjects) {
           let errors = [];
           let filedsToValidateAgainst = [
             "family",
             "product",
             "description",
             "quantity",
             "unitCostExcludingExpenses",
             "IG_OG",
           ];

           addedObjects.forEach((element) => {
             let itemError = {};
             itemError["itemNo"] = element.itemNo;
             itemError["errors"] = [];
             filedsToValidateAgainst.forEach((k) => {
               if (element[k] == undefined) {
                 itemError["errors"].push(k);
               }
             });

             if (itemError["errors"].length > 0) errors.push(itemError);
           });

           return errors;
         }

         //#endregion

         //#region  ItemsDeletion
         //** Remove an Item from the grid. */
         deleteAnItemFromGrid(
           itemNo,
           offerItems: offerItem[],
           changedItems: offerItem[]
         ) {
           let item = offerItems.find((x) => x.itemNo.value == itemNo);

           let indexOfItem = offerItems.indexOf(item);
           let indexOfChangedItem = changedItems.indexOf(item);

           changedItems.splice(indexOfChangedItem, 1);
           offerItems.splice(indexOfItem, 1);
         }

         getItemsToBeDeletedFromServer(offerItems: offerItem[], selectedItems) {
           let itemsToBeDletedFromServer = [];
           selectedItems.forEach((itemNo) => {
             let isItemSavedToServer = offerItems.find(
               (x) => x.itemNo.value == itemNo
             ).IS_SAVED;
             let isAlreadyItemSavedToServer = offerItems.find(
               (x) => x.itemNo.value == itemNo
             ).IS_LOADED_FROM_DB;

             if (isAlreadyItemSavedToServer || isItemSavedToServer) {
               itemsToBeDletedFromServer.push(itemNo);
             }
           });
           return itemsToBeDletedFromServer;
         }

         //** Reorder list of items after deleting. */
         reOrderItems(offerItems: offerItem[]) {
           let newIndexNumber = 1;

           for (let index = 0; index < offerItems.length; index++) {
             offerItems[index].itemNo.value = newIndexNumber;
             ++newIndexNumber;
           }
         }

         //#endregion

         initiateSFC(offerClassififcation) {
           if (offerClassififcation.toLocaleLowerCase() == "rac") return "3";
           else if (offerClassififcation.toLocaleLowerCase() == "hub")
             return "7";
         }

         getChangedFields(changedItems) {
           let fields = [];
           changedItems.forEach((item) => {
             Object.keys(item).forEach((k) => {
               if (item[k].isChanged == true) {
                 let changedField = {};
                 changedField["itemNo"] = item.itemNo.value;
                 changedField["changedField"] = k;
                 changedField["isIterative"] =
                   item[k].isIterative == undefined
                     ? false
                     : item[k].isIterative;
                 changedField["value"] = item[k].value;
                 changedField["isCalulated"] = false;

                 fields.push(changedField);
               }
             });
           });

           return fields;
         }

         isNewExpenseValid(singleExpense: expense) {
           if (singleExpense.name == "" || singleExpense.value == null) {
             return false;
           } else {
             return true;
           }
         }

         calculateExpensePerItem(expenses: expense[]) {
           if (expenses.length > 0) {
             let totalNewelyAddedExpenses = 0;
             expenses.forEach((expense) => {
               if (!expense.IS_SAVED) {
                 if (expense.IS_UPDATED) {
                   let changingValue = expense.value - expense.oldValue;
                   totalNewelyAddedExpenses =
                     totalNewelyAddedExpenses + changingValue;
                 } else if (expense.IS_REMOVED) {
                   totalNewelyAddedExpenses = -expense.value;
                 } else {
                   totalNewelyAddedExpenses =
                     totalNewelyAddedExpenses + expense.value;
                 }
               }
             });

             return totalNewelyAddedExpenses;
           }
         }

         getUnSavedExpenses(expenses: expense[]) {
           return expenses.filter((element) => {
             return element.IS_SAVED == false;
           });
         }

         disableUploadedItems(item, typeFile) {
           let fieldsToBeLocked = [];
           if (typeFile != "") {
             if (
               typeFile == constants.COST_FILES_NAMES.EAS_BO_1 ||
               typeFile == constants.COST_FILES_NAMES.EAS_AUT
             ) {
               fieldsToBeLocked = [
                 "IG_OG",
                 "family",
                 "product",
                 "description",
                 "quantity",
                 "unitCostExcludingExpenses",
                 "totalCostExcludingExpenses",
                 "unitCostIncludingExpenses",
                 "upstreamSFC_Percentage",
                 "Upstream_Percentage",
                 "CCOGM_Percentage_Threshold",
                 "unitSP2",
               ];
             } else if (typeFile == constants.COST_FILES_NAMES.Transfer_Price) {
               fieldsToBeLocked = [
                 "IG_OG",
                 "family",
                 "product",
                 "quantity",
                 "unitCostExcludingExpenses",
                 "upstreamSFC_Percentage",
                 "Upstream_Percentage",
                 "totalCostExcludingExpenses",
                 "unitSP2",
               ];
             }

             fieldsToBeLocked.forEach((key) => {
               item[key].disabled = true;
             });

             return item;
           } else {
             return item;
           }
         }
       }
