import { OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NbDialogService, NbToastrService } from "@nebular/theme";
import {
  localStorageService,
  objectTransfer,
  httpService,
  urls,
  responseModel,
  constants
} from "../../../@core";

import { offerModel, timeLine } from '../../models';
import { ModalBodyComponent } from "../../../@theme/components";
import { updateKPIS } from '../update-kpis/update-kpis';

export class assignOffer implements OnInit {
  loggedInUser: any = {};

  listOfSubordinates: any[] = [];

  offerNumber: string;
  createdAt: string;
  opportunityName: string;
  opportunityLeader: string;
  accountName: string;
  leadingBU: string;
  selectedEmployeeSESA: string = "";
  selectedEmployeeName: any;
  dashboardURL = "";

  constructor(
    private _objectTransfer: objectTransfer,
    private _storageService: localStorageService,
    private _httpService: httpService,
    private _toasterService: NbToastrService,
    private _dialogService: NbDialogService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
  ) {
    this.loggedInUser = this._storageService.getLocalStorage(constants.LOGGED_IN_USER);

  }

  ngOnInit(): void {
  }

  /**
   * Subscribe to the created offer!
   */
  loadCreatedOfferDetails() {
    this._objectTransfer.objectSubscriber.subscribe(
      (offer: { name: string; offer: offerModel }) => {
        if (offer != null) {
          if (offer.name == "createdOfferMetadata") {
            this.bindOfferData(offer.offer);
          }
        }
      }
    );
  }

  bindOfferData(offer) {
    this.offerNumber = offer.offerNumber;
    let date = offer.offerTimeline.find(x => x.isLatest == true).changedAt;
    this.createdAt = new Date(date).toLocaleDateString().toString() || "";

    this.opportunityName = offer.opportunityName;
    this.opportunityLeader = offer.ownerName;
    this.accountName = offer.accountName;
    this.leadingBU = offer.leadingBusiness;
  }

  /**
   * Load the subordinates for the tendering manager
   */
  getTenderingManagerSubOrdinates() {
    let subordinatesURL = `${urls.GET_USERS_SUBORDINATES}/${this.loggedInUser.userSESA}`;
    this._httpService.Get(subordinatesURL, {}).subscribe(subordinates => {
      let serverResponse = subordinates as responseModel;
      if (serverResponse.error) {
        this._toasterService.danger(
          "Subordinates are not loaded correctly",
          "Error loading subordinates",
          {
            hasIcon: true,
            destroyByClick: false
          }
        );
      } else {
        this.listOfSubordinates.push({ "name" : "My Self" , "sesa" : this.loggedInUser.userSESA });
        this.listOfSubordinates = this.listOfSubordinates.concat(serverResponse.data);
      }
    });
  }

  /**
   *
   * @param assignee
   * Select the assignee (Employee)
   */
  selectEmployee(assignee) {
    this.selectedEmployeeSESA = assignee;
    this.selectedEmployeeName =
      (this.listOfSubordinates.find(x => x.sesa == assignee)).name || "";
  }

  /**
   * Post assignement data to server
   */
  assignOffer() {
    this._dialogService
      .open(ModalBodyComponent, {
        context: {
          title: "<i class='fa fa-check'></i> &nbsp; Confirm assigning offer.",
          message: `You're going to assign this offer for ${this.selectedEmployeeName} Tendering Engineer  
                  <br> This action is undone.`
        }
      })
      .onClose.subscribe(async confirmation => {
        if (confirmation) {
          let serverResponse = (await this.postAssignementDataToServer()) as responseModel;
          var updateKpisRequest = new updateKPIS(this._httpService);
          let updatedKpisResponse = await updateKpisRequest.updateUserKpis(this.offerNumber) as responseModel;
          if (serverResponse.error == true && updatedKpisResponse.error == false) {
            this._toasterService.danger(
              `${constants.SERVER_ERROR}`,
              "Assignement failed, try again later",
              {
                hasIcon: true,
                destroyByClick: false
              }
            );
          } else {
            this._toasterService.success(
              `Assignemnet ${constants.SUCCESSFULL_SAVING}`,
              "Offer assigned successfully",
              {
                hasIcon: true,
                destroyByClick: false
              }
            );

            // redierct to dashbaord
            this._router.navigate([this.dashboardURL], {
              relativeTo: this._activatedRoute
            });
          }
        }
      });
  }

  /**
   * Post assignement data to server.
   */
  postAssignementDataToServer() {
    let offerAssignementURL = `${urls.ASSIGN_OFFER}/${this.offerNumber}`;
    let assignementData: timeLine = {
      changedAt: new Date(),
      changedBy: this.loggedInUser.userSESA,
      isLatest: true,
      newStatus: constants.OFFER_STATUS.ASSIGNED,
      oldStatus: constants.OFFER_STATUS.CREATED,
      owner: this.selectedEmployeeSESA
    };
    return this._httpService
      .Post(offerAssignementURL, {}, assignementData)
      .toPromise();
  }

  rediertcToDashboard(dashboardURL: string){
    this.dashboardURL = dashboardURL;
  }
}
