import { constants, urls, httpService } from '../../../@core';

export class updateKPIS {

    constructor(private httpService: httpService){}


    async updateUserKpis(offerNumber){
        let updateKPIsURLS = `${urls.UPDATE_USER_KPIS}/${offerNumber}`;
        return this.httpService.Get(updateKPIsURLS , {}).toPromise();
    }
};
