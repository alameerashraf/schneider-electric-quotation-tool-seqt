export class calculationFields {

    getAffectedFields(fieldName){
        let affectedFields = Object.freeze({
            "quantity" : ["totalCostExcludingExpenses" , "totalCostIncludingExpenses" , "expensesPerUnit" , "totalSP2" , "totalSP1" , "totalSFC_amount" ], //
            "unitCostExcludingExpenses" : ["totalCostExcludingExpenses" , "unitCostIncludingExpenses" , "Upstream_amount" , "upstreamSFC_amount"],
            "expensesPerUnit" : ["unitCostIncludingExpenses"],
            "unitCostIncludingExpenses" : ["totalCostIncludingExpenses" , "Expenses_Percentage" , "unitSP2" , "sellingLocalMargin" , "unitSP1" , "sellingCCOCM_Percentage"] ,
            "totalExpenses" : ["expensesPerUnit" , "Expenses_Percentage"],
            "Upstream_Percentage" : ["Upstream_amount"],
            "unitSP2" : [ "totalSP2" , "sellingLocalMargin" , "sellingCCOCM_Percentage" , "CCOGM_Percentage"], 
            "Upstream_amount" : ["unitSP2" , "unitSP1" , "sellingCCOCM_Percentage"],
            "CCOGM_Percentage" : ["unitSP2"], 
            "CCOGM_Percentage_Threshold" : ["unitSP1"],
            "unitSP1" : ["totalSP1"],
            "fOSFC_Percentage" : ["fOSFC_amount"],
            "upstreamSFC_amount" : ["totalSFC_amount"],
            "fOSFC_amount" : ["totalSFC_amount"],     
            "totalSFC_amount" : ["totalSFC_Percentage"]       
        });


        return affectedFields[fieldName];
    };


    getFormulaFields(fieldName){
        let formulaField = Object.freeze({
            "totalCostExcludingExpenses" : ["quantity" , "unitCostExcludingExpenses"],
            "unitCostIncludingExpenses" : ["unitCostExcludingExpenses" , "expensesPerUnit"],
            "totalCostIncludingExpenses" : ["unitCostIncludingExpenses" , "quantity"],
            "expensesPerUnit" : ["totalExpenses" , "quantity"],
            "Expenses_Percentage" : ["totalExpenses" , "totalCostIncludingExpenses"],
            "Upstream_amount" : ["unitCostExcludingExpenses" , "Upstream_Percentage"],
            "unitSP2" : ["unitCostIncludingExpenses" , "Upstream_amount" , "CCOGM_Percentage"],  
            "CCOGM_Percentage" : [ "unitCostIncludingExpenses" , "Upstream_amount" , "unitSP2" , "CCOGM_Percentage"],
            "totalSP2" : ["quantity" , "unitSP2"],
            "sellingLocalMargin" : ["unitSP2" , "unitCostIncludingExpenses"],
            "unitSP1" : ["unitCostIncludingExpenses" , "Upstream_amount" , "CCOGM_Percentage_Threshold"],
            "totalSP1" : ["quantity" , "unitSP1"],
            "sellingCCOCM_Percentage" : ["unitSP2" , "unitCostIncludingExpenses" , "Upstream_amount"],
            "upstreamSFC_amount" : [ "upstreamSFC_Percentage" , "unitCostExcludingExpenses" ],
            "fOSFC_amount" : ["unitSP2" , "fOSFC_Percentage"],
            "totalSFC_amount" : [ "upstreamSFC_amount" , "fOSFC_amount" , "quantity" ],
            "totalSFC_Percentage" : [ "totalSFC_amount" , "totalSP2" ]
        });

        return formulaField[fieldName];
    }
}