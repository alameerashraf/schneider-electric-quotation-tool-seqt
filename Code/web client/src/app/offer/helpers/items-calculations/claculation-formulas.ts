export class claculationFormulas {

    //** calculate_{{ name of the filed to be calculated }} */
    //** Paramter names should be the fields that forms the equation. */



    calculate_totalCostExcludingExpenses(params){
        // unitCostExcludingExpenses * quantity
        let { unitCostExcludingExpenses , quantity } = params;
        if(unitCostExcludingExpenses != undefined && quantity != undefined){
            let unitCostExc = parseFloat(unitCostExcludingExpenses.toString().replace(/,/g , ''));
            let qty = parseFloat(quantity.toString().replace(/,/g , ''));
    
            
            let totalCostExcludingExpenses = unitCostExc  * qty;

            return totalCostExcludingExpenses;
        }
    };

    calculate_unitCostIncludingExpenses(params) {
        // unitCostExcludingExpenses + expensesPerUnit

        let { unitCostExcludingExpenses, expensesPerUnit } = params;

        if (unitCostExcludingExpenses != undefined && expensesPerUnit != undefined) {
            let untiCostExe = parseFloat(unitCostExcludingExpenses.toString().replace(/,/g , ''));
            let unitCostInc = parseFloat(expensesPerUnit.toString().replace(/,/g , ''));

            let unitCostIncludingExpenses = untiCostExe + unitCostInc;

            return unitCostIncludingExpenses;
        }
    };

    calculate_totalCostIncludingExpenses(params) {
        // unitCostExcludingExpenses * expensesPerUnit
        let { unitCostIncludingExpenses, quantity } = params;
        if (unitCostIncludingExpenses != "" && quantity != "" && !isNaN(unitCostIncludingExpenses) && !isNaN(quantity)) {
            let unitCostInc = parseFloat(unitCostIncludingExpenses.toString().replace(/,/g , ''));
            let qty = parseFloat(quantity.toString().replace(/,/g , ''));

            let totalCostIncludingExpenses = unitCostInc * qty;
            return totalCostIncludingExpenses;
        }
    };

    calculate_expensesPerUnit(params) {
        let { totalExpenses, quantity } = params;

        if(totalExpenses != undefined && quantity != undefined){
            let totalExp = parseFloat(totalExpenses.toString().replace(/,/g , ''));
            let qty = parseFloat(quantity.toString().replace(/,/g , ''));

            let expensesPerUnit = totalExp / qty;
            return expensesPerUnit;
        }
    };

    calculate_Expenses_Percentage(params){
        
        let { totalExpenses , totalCostIncludingExpenses } = params;

        if(totalExpenses != undefined && totalCostIncludingExpenses != undefined){
            let totalExp = parseFloat(totalExpenses.toString().replace(/,/g , ''));
            let totalCostInc = parseFloat(totalCostIncludingExpenses.toString().replace(/,/g , ''));

            let Expenses_Percentage = totalExp / totalCostInc;
            return Expenses_Percentage
        }
    };

    calculate_Upstream_amount(params){
        let { unitCostExcludingExpenses , Upstream_Percentage } = params;

        if(unitCostExcludingExpenses != undefined && Upstream_Percentage != undefined){
            let unitCostExc = parseFloat(unitCostExcludingExpenses.toString().replace(/,/g , ''));
            let upstream_per = parseFloat(Upstream_Percentage.toString().replace(/,/g , ''));

            let Upstream_amount = (unitCostExc * upstream_per) / 100;
            return Upstream_amount
        } else {
            return undefined
        }
    };

    calculate_unitSP2(params){
        let { CCOGM_Percentage , unitCostIncludingExpenses , Upstream_amount } = params;

        if((CCOGM_Percentage == undefined || CCOGM_Percentage == "")){
            CCOGM_Percentage = "0";
        }
        if((Upstream_amount == undefined || Upstream_amount == "")){
            Upstream_amount = "0";
        }

        if(unitCostIncludingExpenses != undefined && Upstream_amount != undefined){
            let CCOGM = parseFloat(CCOGM_Percentage.toString().replace(/,/g , ''));
            let unitCostInc = parseFloat(unitCostIncludingExpenses.toString().replace(/,/g , ''));
            let upstream_amount = parseFloat(Upstream_amount.toString().replace(/,/g , ''));

            let unitSP2 = (unitCostInc - upstream_amount) / (1 - CCOGM);
            return unitSP2;
        } else {
            return undefined
        }
    };

    calculate_CCOGM_Percentage(params){

        let { unitSP2 , unitCostIncludingExpenses , Upstream_amount , CCOGM_Percentage } = params;
        return parseFloat(CCOGM_Percentage.toString().replace(/,/g , ''))

        if(unitSP2 == undefined ){
            unitSP2 = "0";
        }

        if(unitCostIncludingExpenses != undefined && Upstream_amount != undefined){
            let unitsp = parseFloat(unitSP2.toString().replace(/,/g , ''));
            let unitCostInc = parseFloat(unitCostIncludingExpenses.toString().replace(/,/g , ''));
            let upstream_amount = parseFloat(Upstream_amount.toString().replace(/,/g , ''));


            let CCOGM_Percentage = (unitsp - (unitCostInc - upstream_amount))/ unitsp;
            return CCOGM_Percentage;
        } else {
            return undefined
        }
    };

    calculate_totalSP2(params) {
        let { quantity, unitSP2 } = params;

        if (quantity != undefined && unitSP2 != undefined) {
            let qty = parseFloat(quantity.toString().replace(/,/g , ''));
            let unit = parseFloat(unitSP2.toString().replace(/,/g , ''));


            let totalSP2 = qty * unit;
            return totalSP2;
        } else {
            return undefined
        }
    };

    calculate_sellingLocalMargin(params) {
        let { unitSP2 , unitCostIncludingExpenses } = params;

        if (unitCostIncludingExpenses != undefined && unitSP2 != undefined) {
            let unitCostInc = parseFloat(unitCostIncludingExpenses.toString().replace(/,/g , ''));
            let unit = parseFloat(unitSP2.toString().replace(/,/g , ''));


            let sellingLocalMargin = (unit - unitCostInc) / unit;
            return sellingLocalMargin;
        } else {
            return undefined
        }
    };

    calculate_unitSP1(params) {
        let { unitCostIncludingExpenses, Upstream_amount, CCOGM_Percentage_Threshold } = params;

        if (unitCostIncludingExpenses != undefined && Upstream_amount != undefined && CCOGM_Percentage_Threshold != undefined) {
            let unitCostInc = parseFloat(unitCostIncludingExpenses.toString().replace(/,/g , ''));
            let upstream_amount = parseFloat(Upstream_amount.toString().replace(/,/g , ''));
            let CCOGMThreshold = parseFloat(CCOGM_Percentage_Threshold.toString().replace(/,/g , ''));


            let unitSP1 = (unitCostInc - upstream_amount) / (1 - (CCOGMThreshold / 100));
            return unitSP1;
        } else {
            return undefined
        }
    };

    calculate_totalSP1(params) {
        let { quantity, unitSP1 } = params;

        if (quantity != undefined && unitSP1 != undefined) {
            let qty = parseFloat(quantity.toString().replace(/,/g , ''));
            let unit = parseFloat(unitSP1.toString().replace(/,/g , ''));


            let totalSP1 = qty * unit;
            return totalSP1;
        } else {
            return undefined
        }
    };

    calculate_sellingCCOCM_Percentage(params){
        let { unitSP2, unitCostIncludingExpenses , Upstream_amount } = params;

        if (unitSP2 != undefined && unitCostIncludingExpenses != undefined && Upstream_amount != undefined) {
            let unitsp2 = parseFloat(unitSP2.toString().replace(/,/g , ''));
            let unitCostInc = parseFloat(unitCostIncludingExpenses.toString().replace(/,/g , ''));
            let upstream_amount = parseFloat(Upstream_amount.toString().replace(/,/g , ''));


            let sellingCCOCM_Percentage = (unitsp2 - (unitCostInc - upstream_amount)) / unitsp2;
            return sellingCCOCM_Percentage;
        } else {
            return undefined
        } 
    };


    calculate_upstreamSFC_amount(params){
        let { upstreamSFC_Percentage, unitCostExcludingExpenses  } = params;

        if (upstreamSFC_Percentage != undefined && unitCostExcludingExpenses != undefined) {
            let upstreamSFC = parseFloat(upstreamSFC_Percentage.toString().replace(/,/g , ''));
            let unitCostExc = parseFloat(unitCostExcludingExpenses.toString().replace(/,/g , ''));


            let upstreamSFC_amount = (upstreamSFC * unitCostExc) / 100;
            return upstreamSFC_amount;
        } else {
            return undefined
        } 
    };


    calculate_fOSFC_amount(params) {
        let { unitSP2, fOSFC_Percentage } = params;

        if (unitSP2 != undefined && fOSFC_Percentage != undefined) {
            let unitsp2 = parseFloat(unitSP2.toString().replace(/,/g, ''));
            let fosfc_per = parseFloat(fOSFC_Percentage.toString().replace(/,/g, ''));


            let fOSFC_amount = unitsp2 * fosfc_per;
            return fOSFC_amount;
        } else {
            return undefined
        }
    };


    calculate_totalSFC_amount(params) {
        let { upstreamSFC_amount, fOSFC_amount, quantity } = params;

        if (upstreamSFC_amount != undefined && fOSFC_amount != undefined && quantity != undefined) {
            let upstreamSFC = parseFloat(upstreamSFC_amount.toString().replace(/,/g, ''));
            let fosfc_amo = parseFloat(fOSFC_amount.toString().replace(/,/g, ''));
            let qty = parseFloat(quantity.toString().replace(/,/g, ''));


            let totalSFC_amount = (upstreamSFC + fosfc_amo) * qty;
            return totalSFC_amount;
        } else {
            return undefined
        }
    };

    calculate_totalSFC_Percentage(params){
        let { totalSFC_amount, totalSP2 } = params;

        if (totalSFC_amount != undefined && totalSP2 != undefined) {
            let totalSFC = parseFloat(totalSFC_amount.toString().replace(/,/g, ''));
            let totalsp = parseFloat(totalSP2.toString().replace(/,/g, ''));


            let totalSFC_Percentage = totalSFC / totalsp;
            return totalSFC_Percentage;
        } else {
            return undefined
        }
    };



    //** Main calculatio handler! */
    calculate(fieldName , ...params) {
        let calculationHandlers = Object.freeze({
            "Upstream_amount": this.calculate_Upstream_amount,
            "totalCostExcludingExpenses" : this.calculate_totalCostExcludingExpenses,
            "unitCostIncludingExpenses": this.calculate_unitCostIncludingExpenses,
            "totalCostIncludingExpenses" : this.calculate_totalCostIncludingExpenses,
            "expensesPerUnit" : this.calculate_expensesPerUnit,
            "Expenses_Percentage" : this.calculate_Expenses_Percentage,
            "unitSP2": this.calculate_unitSP2,
            "CCOGM_Percentage" : this.calculate_CCOGM_Percentage,
            "totalSP2" : this.calculate_totalSP2,
            "sellingLocalMargin" : this.calculate_sellingLocalMargin,
            "unitSP1" : this.calculate_unitSP1,
            "totalSP1" : this.calculate_totalSP1,
            "sellingCCOCM_Percentage" : this.calculate_sellingCCOCM_Percentage,
            "upstreamSFC_amount" : this.calculate_upstreamSFC_amount,
            "fOSFC_amount": this.calculate_fOSFC_amount,
            "totalSFC_amount" : this.calculate_totalSFC_amount,
            "totalSFC_Percentage" : this.calculate_totalSFC_Percentage
        });

        return calculationHandlers[fieldName](...params);
    }
};

