import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { httpService , urls , objectTransfer} from './../../../@core';
import { responseModel } from './../../../@core/models/response';
import { offerModel } from './../../models/offer';
import { constants } from './../../../@core/helpers/constants/constants';
import { Title } from '@angular/platform-browser';
import { localStorageService } from './../../../@core/services/local-storage/local-storage';


@Component({
  selector: 'configure-offer',
  templateUrl: './configure-offer.component.html',
  styleUrls: ['./configure-offer.component.scss']
})
export class ConfigureOfferComponent implements OnInit {
  offerNumber: any;
  currency: string;
  statusBadge: string;
  status: string;

  bFOLastUpdate = new Date().toLocaleDateString() +" "+ new Date().toLocaleTimeString();
  @ViewChild("offerHeader" , { static: true }) offerHeader;
  assignee: string;
  showOfferNumber: any = true;
  loggedInUser: any;
  badgeTextColor: any;
  currentStatus: any;
  classification: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private objectTransfer: objectTransfer,
    private httpService: httpService,
    private titleService: Title,
    private localStorageService: localStorageService
  ) {
    this.titleService.setTitle("SEQT| Configure offer ");
  }

  ngOnInit() {
    this.objectTransfer.objectPuplisher({ isActive: true , name: 'metadata' });
    this.loggedInUser = this.localStorageService.getLocalStorage(constants.LOGGED_IN_USER);
    this.offerHeader.open();

    this.getOfferData();
    // this.objectTransfer.objectSubscriber.subscribe((date) => {
    //   if(date.name == 'bfo-update'){
    //     this.bFOLastUpdate = new Date().toLocaleDateString() +" "+ new Date().toLocaleTimeString();
    //   }
    // })
  };

  getOfferData(){
    this.route.params.subscribe((params) => {
      this.offerNumber = params['offerNumber'];
      this.loadOfferData(this.offerNumber);
      this.tempPersistenceForOfferData("offerNumber" , this.offerNumber);
    });
  };

  // Load offer data !
  loadOfferData(offerNumber){
    let offerURL = `${urls.GET_OFFER}/${offerNumber}`;
    this.httpService.Get(offerURL , {}).subscribe((offer: responseModel) => {
      if(!offer.error){
        let data = offer.data as offerModel;
        this.currency = data.currencyCode;
        this.status = this.getStatusLabel(data.currentStatus , data.createdBy);
        this.classification = data.classification;
        this.statusBadge = this.offerStatusBadge(data.currentStatus)["color"];
        this.badgeTextColor = this.offerStatusBadge(data.currentStatus)["colorText"];
        this.assignee =  data.offerTimeline.find(x => x.newStatus == "ASSIGNED").owner || "";
        this.currentStatus = data.offerTimeline.find(x => x.isLatest == true).newStatus;


        this.tempPersistenceForOfferData("offerCurrency" , this.currency);
        this.tempPersistenceForOfferData("offerStatus" , this.currentStatus);
        this.tempPersistenceForOfferData("offerClassification" , this.classification);
      }
    })
  };

  /**Saving offer number temp for child components ! */
  tempPersistenceForOfferData(key , data){
    this.localStorageService.setLocalStorage(key , data);
  };

  offerStatusBadge(currentStatus){
    return constants.OFFER_STATUS_COLOR[currentStatus];
  };

  getStatusLabel(status , owner){
    if(owner == this.loggedInUser.userSESA){
      return constants.STATUS_LABEL["OWNER"][status];
    } else {
      return constants.STATUS_LABEL["NOT_OWNER"][status];
    }
  }

  isOfferHeaderChanged(isChanged){
    this.showOfferNumber = false;
  };
}
