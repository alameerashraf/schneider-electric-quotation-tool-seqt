import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Inject, Input ,  Component, OnInit  } from '@angular/core';
import { NbDialogService } from '@nebular/theme';

import { urls, httpService, responseModel, mapper, constants, localStorageService } from '../../../../../@core';
import { offerItems, expensesManagerCalculations } from '../../../../helpers';
import { ModalBodyComponent } from '../../../../../@theme/components';
import { expense , itemsViewModel, itemsExpenses} from '../../../../models';

@Component({
  selector: 'nexpenses-manager',
  templateUrl: './expenses-manager.component.html',
  styleUrls: ['./expenses-manager.component.scss']
})
export class ExpensesManagerComponent implements OnInit {
  btnIcon = "plus-circle-outline";
  itemsHelper = new offerItems();
  expensesHelper = new expensesManagerCalculations();
  items: itemsViewModel[] = [];
  itemPages: itemsViewModel[] = [];

  currentSelectedPage: any;
  maxGeneralExpensesGridSize = 3;
  generalExpensesGridPageSize = 3;
  generalExpensesSelectedPage = 1;
  generalExpensesSkip = 0;
  generalExpensesLimit = 3;

  currentExpensesUnit: string = "unit";
  expensesUnitPlaceHolder: string = "";

  allExpenses: expensesList[] = [];
  expensesCategories: expensesCategories[];
  generalExpenses: expensesCategories[];

  itemsExpenses: itemsExpenses[] = [];

  generalExpense: expense = new expense();
  addedGeneralExpenses: expense[] = [];
  totalAddedGeneralExpenses =0;
  currencyRates: any;
  offerCurrency: string;
  conversionRate: any;

  constructor(
    private httpService: httpService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef:MatDialogRef<ExpensesManagerComponent>,
    private localStorageService: localStorageService,
    private dialogeService: NbDialogService) {

   }

  ngOnInit() {
    this.items = this.itemsHelper.constructItemsViewModel(this.data);
    this.offerCurrency = this.localStorageService.getLocalStorage("offerCurrency");
    this.loadExpenses();

    if(this.offerCurrency !== "EGP"){
      this.loadCurrencies();
    } else {
      this.conversionRate = 0;
    }
  };


  loadExpenses(){
    let expensesURL = `${urls.GET_EXPENSES}`;
    this.httpService.Get(expensesURL).subscribe((expenses: responseModel) => {
      this.expensesCategories = expenses.data as expensesCategories[];
      
      this.expensesCategories.forEach((expense) => {
        this.allExpenses = this.allExpenses.concat(expense.expensesList);
      });

      this.generalExpenses = this.expensesHelper.filterExpensesCategories(this.expensesCategories , "general");
    });
  };

  loadCurrencies() {
    let currenciesURL = `${urls.GET_CURRENCIES}`;
    this.httpService.Get(currenciesURL, {}).subscribe((currencies) => {
      let serverResponse = currencies as responseModel;
      if (!serverResponse.error) {
        this.currencyRates = serverResponse.data[0];
        let selectedCurrencyConversions = this.currencyRates["EGP"];
        this.conversionRate = selectedCurrencyConversions.rates.find(x => x.to == this.offerCurrency)["rate"];
      }
    })
  };

  //** Adding General expenses to every single Items!*/
  addGeneralExpense() {
    let addedExpenseValue = 0;
    if (this.generalExpense.valueType == "Hours") {
      if(this.conversionRate == 0){
        addedExpenseValue = this.generalExpense.value =  this.generalExpense.hourlyRate * this.generalExpense.value;
      }else {
        addedExpenseValue = this.generalExpense.value =  (this.generalExpense.hourlyRate * this.generalExpense.value) * this.conversionRate;
      }
    } else {  
      addedExpenseValue = this.generalExpense.value;
    }

    if (this.isExpenseAlreadyExisted(this.generalExpense)) {
      let alreadyAddedExpense = this.addedGeneralExpenses.find(x => x.name == this.generalExpense.name);
      alreadyAddedExpense.value = alreadyAddedExpense.value + addedExpenseValue;
    } else {
      this.addedGeneralExpenses.push(this.generalExpense);
    }


    if(this.generalExpense.valueType != "Percentage"){
      this.totalAddedGeneralExpenses = this.totalAddedGeneralExpenses + addedExpenseValue;
    }
    
    // Clear the added expense 
    this.generalExpense = new expense();
    this.btnIcon = "plus-circle-outline";
  };

  editGeneralExpense(expenseName) {
    this.btnIcon = "save";
    this.changeExpensesType(expenseName);
    let editedExpense = this.addedGeneralExpenses.find(x => x.name == expenseName);


    // Remove the value from the totals 
    this.totalAddedGeneralExpenses = this.totalAddedGeneralExpenses - editedExpense.value;
    editedExpense.value = 0;

  };

  removeGeneralExpense(expenseName){
    let expenseToRemove = this.addedGeneralExpenses.find(x => x.name == expenseName);

    let expenseIndex = this.addedGeneralExpenses.indexOf(expenseToRemove);
    this.addedGeneralExpenses.splice(expenseIndex , 1);

    this.totalAddedGeneralExpenses = this.totalAddedGeneralExpenses - expenseToRemove.value;
  };

  //** (Adding) (Modifying) the items expenses! */
  modifyItemsExpenses(itemsExpensesList: itemsExpenses[]) {
    let expensesAddedPerItems = itemsExpensesList.filter((element) => {
      return element.expenses.length > 0;
    });

    // concating all expenses (General) and (itemized).
    this.itemsExpenses.forEach((element) => {
      let selectedItem = expensesAddedPerItems.find(x => x.itemNo == element.itemNo);
      element.expenses = selectedItem.expenses.concat(element.expenses);
    });

    this.itemsExpenses = expensesAddedPerItems;
  };

  generalExpenseChangePage(pageNumber){
    this.generalExpensesLimit = this.generalExpensesGridPageSize * pageNumber;
    this.generalExpensesSkip = Math.abs(this.generalExpensesGridPageSize - this.generalExpensesLimit);
    this.currentSelectedPage = pageNumber;
  };

  changeExpensesType(changedName){
    this.generalExpense.type = constants.EXPENSES_TYPES["general"];
    let selectedExpenseType = this.allExpenses.find(x => x.name == changedName);

    this.generalExpense.name = changedName;
    this.generalExpense.valueType = selectedExpenseType.valueType;
    this.generalExpense.hourlyRate = selectedExpenseType.hourlyRate;
    this.currentExpensesUnit = this.generalExpense.valueType = selectedExpenseType.valueType;
    this.expensesUnitPlaceHolder = constants.EXPENSES_VALUE_TYPES[selectedExpenseType.valueType.trim()];
  };

  addAllExpensesToItemsGrid(){
    this.dialogeService.open(ModalBodyComponent, {
      context: {
        title: "<i class='fa fa-check-square' aria-hidden='true'></i> &nbsp; Confirm Adding expenses",
        message: `Expenses will be saved to items, are you sure you want to continue ?`
      }
    }).onClose.subscribe((confirmation) => {
      if(confirmation){
        this.saveAddedExpenses();
      }
    })
  };

  saveAddedExpenses(){
    let expenses = {
      itemsExpenses: [],
      generalExpenses: [] //TODO: to be saved to the offer's metadata.
    };

    expenses.itemsExpenses = this.itemsExpenses;
    expenses.generalExpenses = this.addedGeneralExpenses;

    this.dismiss(expenses);
  };

  dismiss(data = null){
    if(data == null)
      this.dialogRef.close(null);
    else
      this.dialogRef.close(data);
  };

  isExpenseAlreadyExisted(generalExpense: expense){
    let addedExpense = this.addedGeneralExpenses.find(x => x.name == generalExpense.name);
    if(addedExpense == undefined)
      return false;
    else 
      return true;
  };
}

