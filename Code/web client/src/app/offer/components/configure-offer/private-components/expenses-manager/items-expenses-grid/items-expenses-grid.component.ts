import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { slideInOutAnimation, mapper, urls, responseModel, httpService, constants, localStorageService } from '../../../../../../@core';
import { itemsExpenses } from '../../../../../models/ietms-expenses';
import { offerItem } from '../../../../../models';
import { offerItems , expensesManagerCalculations } from '../../../../../helpers';
import { expense } from '../../../../../models/expense';

@Component({
  selector: 'items-expenses-grid',
  templateUrl: './items-expenses-grid.component.html',
  styleUrls: ['./items-expenses-grid.component.scss'],
  animations: [slideInOutAnimation]
})
export class ItemsExpensesGridComponent implements OnInit{
  itemsHelper = new offerItems();
  expensesHelper = new expensesManagerCalculations();
  collapsed = "arrow-right-outline";
  expanded = "arrow-down-outline";

  @Input() offerItems: [] = [];
  @Output() presisteExpenses = new EventEmitter<any[]>();

  pageSize = 9;
  page = 1;
  maxSize = 5;
  totalItems;

  maxNestedGridSize = 3;
  nestedGridPageSize = 3;

  items: itemsExpenses[] = [];

  selectedItem: any = {};
  currentSelectedPage: any;
  itemPages: itemsExpenses [];
  expensesList: expensesCategories[];
  allExpenses: expensesList[] = [];
  itemsExpenses: expensesCategories[] = [];
  currencyRates: any;
  conversionRate: any = 0;
  offerCurrency: string;
  offerNumber: string;

  oldValueForEditedExpense = 0 ;

  constructor(private httpService: httpService, private localStorageService: localStorageService) {}

  async ngOnInit() {
    this.offerCurrency = this.localStorageService.getLocalStorage("offerCurrency");
    this.offerNumber = this.localStorageService.getLocalStorage("offerNumber");
    this.costructItems();
    this.totalItems = this.items.length;
    this.pageChanges(1);
    this.loadExpenses();

    if(this.offerCurrency != "EGP"){
      this.loadCurrencies();
    } else {
      this.conversionRate = 0;
    }
  };


  loadCurrencies() {
    let currenciesURL = `${urls.GET_CURRENCIES}`;
    this.httpService.Get(currenciesURL, {}).subscribe((currencies) => {
      let serverResponse = currencies as responseModel;
      if (!serverResponse.error) {
        this.currencyRates = serverResponse.data[0];
        let selectedCurrencyConversions = this.currencyRates["EGP"];
        this.conversionRate = selectedCurrencyConversions.rates.find(x => x.to == this.offerCurrency)["rate"];
      }
    })
  };
  
  costructItems(){
    this.offerItems.forEach((item) => {
      let gridItem = mapper.map(item , itemsExpenses);
      gridItem.totalExpenses = gridItem.expenses.reduce((a , b) => a + b.value , 0)
      this.items.push(gridItem);
    });
  };

  pageChanges(pageNumber){
    this.currentSelectedPage = pageNumber;
    let limit = this.pageSize * pageNumber;
    let skip = Math.abs(this.pageSize - limit);
    this.itemPages = this.items.slice(skip , limit);
  };

  loadExpenses(){
    let expensesURL = `${urls.GET_EXPENSES}`;
    this.httpService.Get(expensesURL).subscribe((expenses: responseModel) => {
      this.expensesList = expenses.data as expensesCategories[];
      // Construct all expenses
      this.expensesList.forEach((expense) => {
        this.allExpenses = this.allExpenses.concat(expense.expensesList);
      });
      this.itemsExpenses = this.expensesHelper.filterExpensesCategories(this.expensesList , "items");
    })
  };

  emitNewExpenses(){
    let itemsExpenses = [];
    this.items.forEach((item) => {
      let newItem = {};
      newItem["itemNo"] = item["itemNo"];
      newItem["expenses"] = item["expenses"];
      newItem["totalExpenses"] = item["totalExpenses"];

      itemsExpenses.push(newItem);
    });

    this.presisteExpenses.emit(itemsExpenses);
  };

  viewExpenses(itemNo){
    let selectedItem = this.items.find(x => x.itemNo == itemNo);
    selectedItem.isSelected = !selectedItem.isSelected;
    selectedItem.isExpensesOpened = !selectedItem.isExpensesOpened;
  };

  addExpenseToItem(itemNo){
    let selectedItem = this.items.find(x => x.itemNo == itemNo);
    selectedItem.isSelected = selectedItem.isExpensesOpened =  true;

    let newExpense = new expense();
    newExpense = selectedItem.selectedExpense;
    newExpense.IS_SAVED = false;
    newExpense.type = constants.EXPENSES_TYPES["items"];
    
    if (this.itemsHelper.isNewExpenseValid(newExpense)) {
      // get the total amount of money from Hours!
      if (selectedItem.selectedExpense.valueType == "Hours") {
        if(this.conversionRate == 0){
          selectedItem.selectedExpense.value = selectedItem.selectedExpense.hourlyRate * selectedItem.selectedExpense.value;
        } else {
          selectedItem.selectedExpense.value = +((selectedItem.selectedExpense.hourlyRate * selectedItem.selectedExpense.value) * this.conversionRate).toFixed(3);
        }
      }

      if(selectedItem.selectedExpense.valueType == "Percentage"){
        selectedItem.selectedExpense.value = selectedItem.totalCostExcludingExpenses * (selectedItem.selectedExpense.value / 100);
      }

      if (this.isExpenseAlreadyExisted(itemNo, selectedItem.selectedExpense.name)) {
        let targetExpense = selectedItem.expenses.find(x => x.name == selectedItem.selectedExpense.name);
        targetExpense.value = targetExpense.value + selectedItem.selectedExpense.value;
        targetExpense.IS_SAVED = false;
        targetExpense.IS_UPDATED = true;
        targetExpense.oldValue = this.oldValueForEditedExpense;
      } else {
        selectedItem.expenses.push(newExpense);
      }
    };

    // Adding expense value to totals
    selectedItem.totalExpenses = selectedItem.totalExpenses + newExpense.value;

    // Clear the selection 
    selectedItem.selectedExpense = new expense();
    selectedItem.currentlySelectedExpensesPlaceHolder = "";
    this.oldValueForEditedExpense = 0;
    selectedItem.btnIcon = "plus-circle-outline";

    this.emitNewExpenses();
  };

  editItemExpense(itemNo , expenseName){
    let selectedItem = this.items.find(x => x.itemNo == itemNo);
    let expenseToEdit = selectedItem.expenses.find(x => x.name == expenseName);

    // Change the icon for items handler 
    selectedItem.btnIcon = "save";

    // Add the expense to be added to the drop down and to the text box to be edited!
    this.oldValueForEditedExpense = expenseToEdit.value;
    selectedItem.selectedExpense = new expense();
    selectedItem.selectedExpense.name = expenseToEdit.name;
    selectedItem.selectedExpense.value = 0;
    
    // Remove the value from the totals 
    selectedItem.totalExpenses = selectedItem.totalExpenses - expenseToEdit.value;

    // change the expense value it self, from the list!
    expenseToEdit.value = 0;
    this.changePlaceHolderForInput(itemNo)
  };

  removeItemExpense(itemNo , expenseName){
    let selectedItem = this.items.find(x => x.itemNo == itemNo);
    let expenseToRemove = selectedItem.expenses.find(x => x.name == expenseName);

    expenseToRemove.IS_REMOVED = true;
    expenseToRemove.IS_SAVED = false;
    selectedItem.totalExpenses = selectedItem.totalExpenses - expenseToRemove.value;
    this.emitNewExpenses();
  };

  nestedGridPageChanges(itemNo , pageNumber){
    let selectedItem = this.items.find(x => x.itemNo == itemNo);
    selectedItem.limit = this.nestedGridPageSize * pageNumber;
    selectedItem.skip = Math.abs(this.nestedGridPageSize - selectedItem.limit);
    selectedItem.selectedPage = pageNumber;
  };

  changePlaceHolderForInput(itemNo){
    let selectedItem = this.items.find(x => x.itemNo == itemNo);
    let expense = this.allExpenses.find(x => x.name == selectedItem.selectedExpense.name);
    selectedItem.currentlySelectedExpensesPlaceHolder = constants.EXPENSES_VALUE_TYPES[expense.valueType.trim()];
    selectedItem.selectedExpense.valueType = expense.valueType.trim();
    selectedItem.selectedExpense.hourlyRate = expense.hourlyRate;
  };

  isExpenseAlreadyExisted(itemNo , expenseName){
    let selectedItem = this.items.find(x => x.itemNo == itemNo);
    let expense = selectedItem.expenses.find(x => x.name == expenseName);
    if(expense == undefined)
      return false;
    else 
      return true;
  };
}
