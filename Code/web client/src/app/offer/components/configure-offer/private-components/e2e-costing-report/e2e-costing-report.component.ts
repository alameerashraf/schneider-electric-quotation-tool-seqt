import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'e2e-costing-report',
  templateUrl: './e2e-costing-report.component.html',
  styleUrls: ['./e2e-costing-report.component.scss']
})
export class E2eCostingReportComponent implements OnInit {
  tableHeaders = [
    {
      headerTitle: "(Manual Input) Plant name/EQP type :",
      style: {
        width: "300",
        bgColor: "green",
        txtColor: "white"
      }
    },
    {
      headerTitle: "EAS BO 1",
      style: {
        width: "100",
        bgColor: "yellow",
        txtColor: "black"
      }
    },
    {
      headerTitle: "EAS BO 2",
      style: {
        width: "100",
        bgColor: "yellow",
        txtColor: "black"
      }
    },
    {
      headerTitle: "EAS AUT",
      style: {
        width: "100",
        bgColor: "yellow",
        txtColor: "black"
      }
    },
    {
      headerTitle: "Exec Center",
      style: {
        width: "150",
        bgColor: "yellow",
        txtColor: "black"
      }
    },
    {
      headerTitle: "Total",
      style: {
        width: "100",
        bgColor: "lightblue",
        txtColor: "white"
      }
    }
  ];

  columnsRows = [
    {
      name: "IG Purchases",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "OG Purchases Material",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "OG Purchases Design, Engineering, PM",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "OG Purchases Install, Commisioning, Services",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Total DVC Purchasing (111 to 129)",
      style: {
        bgColor: "white",
        txtColor: "black",
        isBold: true
      }
    },
    {
      name: "Project Management",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Engineering & Design",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Procurement & Logistics",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Contract Mgt, Contract Adm & Project Controlling",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Quality Inspection (incl.FAT)",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Site erection, testing & commisionning",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Other Labor",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Total DVC Labour  (131 to 149)",
      style: {
        bgColor: "white",
        txtColor: "black",
        isBold: true
      }
    },
    {
      name: "Packing & Storage",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Transport",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Customs duties, Tax",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Travel & Expense Account",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Currency exchange cover",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "RMI (Raw Material Increase) Cover",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Financial costs",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Commercial fees",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Warranty provision",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Risk reserve",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Other",
      style: {
        bgColor: "white",
        txtColor: "black"
      }
    },
    {
      name: "Total Other DVC (151 to 179)",
      style: {
        bgColor: "white",
        txtColor: "black",
        isBold: true
      }
    },
    {
      name: "TOTAL DVC",
      style: {
        bgColor: "gray",
        txtColor: "black",
      }
    },
    {
      name: "Total MBC Costs (201 to 299)",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },
    {
      name: "Total Cost of Goods Sold (COGS) (100+200)",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },
    {
      name: "SFC R&A and Business",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },
    {
      name: "SFC Country & Global Costs (441 to 449)",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },
    {
      name: "IG Transfer Price",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },
    {
      name: "BO Margin on DVC (900-100)",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },
    {
      name: "M/DVC % IG Transfer Price",
      style: {
        bgColor: "lightgreen",
        txtColor: "black",
      }
    },
    {
      name: "BO Gross Margin (900-300)",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },
    {
      name: "Stat GM % IG Transfer Price",
      style: {
        bgColor: "lightgreen",
        txtColor: "black",
      }
    },
    {
      name: "Upstream GM Received from others BO",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },
    {
      name: "COGS",
      style: {
        bgColor: "lightgreen",
        txtColor: "black",
      }
    },
    {
      name: "CSC (COGS+UpstreamGM)",
      style: {
        bgColor: "lightgreen",
        txtColor: "black",
      }
    },
    {
      name: "BO CCO GM",
      style: {
        bgColor: "green",
        txtColor: "black",
      }
    },
    {
      name: "CCO GM % IG Transfer Price",
      style: {
        bgColor: "green",
        txtColor: "black",
      }
    },
    {
      name: "TOTAL DVC Exec Center Contribution",
      style: {
        bgColor: "lightpink",
        txtColor: "black",
      }
    },
    {
      name: "End Customer Sales Amount",
      style: {
        bgColor: "#00b0f0",
        txtColor: "black",
      }
    },
    {
      name: "Negotiation Margin",
      style: {
        bgColor: "#00b0f0",
        txtColor: "black",
      }
    },
    {
      name: "End Customer Minimum Sales Amount",
      style: {
        bgColor: "#00b0f0",
        txtColor: "black",
      }
    },
    {
      name: "Exec Center MBC",
      style: {
        bgColor: "lightpink",
        txtColor: "black",
      }
    },
    {
      name: "%MBC Exec Center (Negative sign) / OG min. selling price",
      style: {
        bgColor: "lightpink",
        txtColor: "black",
      }
    },
    {
      name: "Selling Stat. Gross Margin",
      style: {
        bgColor: "#00b0f0",
        txtColor: "black",
      }
    },
    {
      name: "% Stat GM / Min selling price",
      style: {
        bgColor: "#00b0f0",
        txtColor: "black",
      }
    },
    {
      name: "COGS E2E",
      style: {
        bgColor: "#00b0f0",
        txtColor: "black",
      }
    },
    {
      name: "Upstream GM received from EAS BO",
      style: {
        bgColor: "white",
        txtColor: "black",
      }
    },

  ]

  constructor() { }

  ngOnInit() {
  }

}
