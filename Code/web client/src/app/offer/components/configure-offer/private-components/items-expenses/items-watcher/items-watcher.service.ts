import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { offerItem } from './../../../../../models/offer-Item';

@Injectable({
  providedIn: 'root'
})
export class ItemsWatcherService {

  public items = new Subject<offerItem[]>();

  change_item(val) {
    this.items.next(val);
  }
}
