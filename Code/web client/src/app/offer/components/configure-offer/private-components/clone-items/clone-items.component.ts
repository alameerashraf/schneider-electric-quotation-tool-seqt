import { Component, OnInit } from '@angular/core';
import {  itemsViewModel } from '../../../../models/itemsViewModel';
import { NbToastrService } from '@nebular/theme';
import { urls, localStorageService, httpService, responseModel, mapper, constants } from '../../../../../@core';
import { offerItem } from '../../../../models';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'clone-items',
  templateUrl: './clone-items.component.html',
  styleUrls: ['./clone-items.component.scss']
})
export class CloneItemsComponent implements OnInit {
  offerNumber;
  
  selectedItem;
  items: itemsViewModel[] = [];

  itemPages: itemsViewModel[] = [];
  pageSize = 5;
  page = 1;
  maxSize = 5;
  currentSelectedPage: any;
  numberOfPages = [];
  totalItems;
  selectedItems: any[] = [];
  currentOfferNumber: string;

  constructor(
    private localStorageService: localStorageService,
    private httpService: httpService,
    private toasterService: NbToastrService,
    private dialogRef:MatDialogRef<CloneItemsComponent>,) { }

  ngOnInit() {
    this.currentOfferNumber = this.localStorageService.getLocalStorage("offerNumber");
  };

  selectItem(itemNumber) {
    let selectedItem = this.items.find(x => x.itemNo == itemNumber);
    selectedItem.isSelected = !selectedItem.isSelected;
    if(selectedItem.isSelected){
      this.selectedItems.push(itemNumber);
    } else {
      this.selectedItems.splice(this.selectedItems.indexOf(itemNumber) , 1);
    }
  };

  searchByOfferNumber(offerNumber){
    this.offerNumber = offerNumber;
    this.loadItems();
  };
  
  loadItems(){
    let skip = 0;
    let limit = this.pageSize;
    let loadingItemsURL = `${urls.LOAD_OFFER_ITEMS}/${this.offerNumber}`;
    this.httpService.Get(loadingItemsURL).subscribe((items: responseModel) =>{
      if(!items.error){
        let loadedItems = items.data;
        this.totalItems = items.total;
        this.items = [];

        loadedItems.forEach(element => {
          let item: itemsViewModel = mapper.map(element,itemsViewModel);
          this.items.push(item);
        });
      } else {
        this.items = [];
        if(items.code !== constants.ERROR_CODES.DATA_NOT_FOUND){
          this.toasterService.danger("Couldn't load offer data" , "Something went wrong on the server" , {
            destroyByClick: true,
            hasIcon: true
          })
        }
      }

      this.itemPages = this.items.slice(skip , limit);
    })
  };

  pageChanges(pageNumber) {
    this.currentSelectedPage = pageNumber;
    let limit = this.pageSize * pageNumber;
    let skip = Math.abs(this.pageSize - limit);
    this.itemPages = this.items.slice(skip , limit);
  };

  convertClonedObjectsToOfferObjects(items , isSaved , isLoadedFromDb){
    let convertedItems = [];
    // Convert server object to view object!
    items.forEach(element => {
        let item = new offerItem();
        delete element["_id"];
        item["IS_SAVED"] = isSaved;
        item["IS_LOADED_FROM_DB"] = isLoadedFromDb;


        Object.keys(item).forEach(k => {
            if (!constants.OFFER_ITEMS_UI_HELPER_FIELDS.includes(k)) {
                item[k].value = element[k] == undefined ? "" : element[k];

                if (isSaved == false && isLoadedFromDb == false) {
                    item[k].isChanged = true;
                }
            }
        });

        convertedItems.push(item);
    });

    return convertedItems;
  }

  cloneItems(){
    if(this.offerNumber == this.currentOfferNumber){
      this.toasterService.warning("Unable to clone from the same offer" , "Invalid clone operation" , {
        hasIcon : true,
        destroyByClick: true
      })
    } else {
      let cloneURL = `${urls.CLONE_ITEMS}/${this.offerNumber}`;
      this.httpService.Post(cloneURL , {} , { "itemIds" : this.selectedItems }).subscribe((clonedItems) => {
        let serverResponse = clonedItems as responseModel;
        if(!serverResponse.error){
          let convertedItems = this.convertClonedObjectsToOfferObjects(serverResponse.data , false , false);
          this.dialogRef.close(convertedItems);
        }
      });
    }
  }
}
