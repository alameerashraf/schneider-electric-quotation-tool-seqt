import { Component, OnInit, Input } from '@angular/core';
import { httpService, urls, excelService, constants, localStorageService, responseModel } from '../../../../../@core';
import { eas, offerItem } from '../../../../models';
import { MatDialogRef } from '@angular/material';
import { offerItems , costFilesItemsCalculations } from '../../../../helpers';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'upload-eas-file',
  templateUrl: './upload-eas-file.component.html',
  styleUrls: ['./upload-eas-file.component.scss'],
})
export class UploadEasFileComponent implements OnInit {
  itemsHelper = new offerItems();
  costFilesItemsCalculationsHelper = new costFilesItemsCalculations();

  @Input() title: string;
  numberOfFiles: any;
  progress: string;
  isUploading: boolean;
  selectedType: string;
  items: any[] = [];
  sfc: any;
  upstream: any;

  isCurrencyExchangeDisabled: boolean = false;
  selectedCurrency: string = "";
  offerCurrency: string = "";
  currencyRates = {};
  conversionRate = 0;

  constructor(
    private httpService: httpService,
    private localStorageService: localStorageService,
    private excelService: excelService,
    private toaster: NbToastrService,
    private dialogRef:MatDialogRef<UploadEasFileComponent>) {

    }

  ngOnInit(){
    this.offerCurrency = this.selectedCurrency = this.localStorageService.getLocalStorage("offerCurrency");
    this.loadCurrencies();
  }

  loadCurrencies(){
    let currenciesURL = `${urls.GET_CURRENCIES}`;
    this.httpService.Get(currenciesURL , {}).subscribe((currencies) => {
      let serverResponse = currencies as responseModel;
      if(!serverResponse.error){
        this.currencyRates = serverResponse.data[0];
      }
    })
  }



  onFileChanged(event) {
    let correspondingInfoPerSheet = constants.COST_FILES[this.selectedType];
    let itemsData: any[][] = [];
    let sfc = 0;
    let upstream = 0;

    correspondingInfoPerSheet.forEach(async element => {
      if(element.rowsCols){
        let rowsAndCols = await this.excelService.initReader(event , element.sheetName , constants.EXCEL_REQUESTD_DATA_TYPES.ROWS_AND_COLS);
        itemsData = rowsAndCols;
        // Remove headers and totals from the sheet (last and first item)
        itemsData = itemsData.slice(1 , -1);
        this.items = this.itemsHelper.convertLoadedItemsToViewObject(itemsData , this.selectedType);
      }
      else{
        let cellValues = await this.excelService.initReader(event , element.sheetName , constants.EXCEL_REQUESTD_DATA_TYPES.CELL_VALUE , element.cells);
        this.sfc = this.calculate(this.selectedType , "SFC%" , cellValues);
        this.upstream = this.calculate(this.selectedType , "Upstream%" , cellValues);
      }
    });
  };


  // Prepare files request.
  upload(files) {
    this.numberOfFiles = files.length;
    if (files.length === 0) {
      return;
    };

    this.progress = "Uploading ...";
    this.isUploading = true;

    const formData = new FormData();
    for (let file of files) {
      let fileExt = file.name.split('.')[1];
      let fileName = `costFile.${fileExt}`;
      formData.append(fileName, file);
    };

    let costFileURL = `${urls.UPLOAD_COST_FILES}`;
    let params = { fileType: this.selectedType };

    this.httpService.Post(costFileURL, params, formData).subscribe((x) => {
    });
  };

  dismiss() {
    let items = null;
    if(this.selectedType == constants.COST_FILES_NAMES.EAS_AUT || this.selectedType == constants.COST_FILES_NAMES.EAS_BO_1){
      items = this.costFilesItemsCalculationsHelper.bindCalculatedFieldsOnCostFilesUpload(this.items , this.sfc , this.upstream);
    } else {
      items = this.items;
    };

    if(this.conversionRate != 0){
      items = this.costFilesItemsCalculationsHelper.convertCurrencies(items , this.selectedType , this.conversionRate);
    }

    items.forEach(element => {
      element["UPLOADED_FROM"] = this.selectedType;
    });

    let result = {
      uploadedItems: items,
      typeFile: this.selectedType
    };
    this.dialogRef.close(result);
  };




  calculate(fileType , field , cells){
    if(field == "SFC%")
    return this.costFilesItemsCalculationsHelper.calculateSFC_Percentage(fileType , cells);
    if(field == "Upstream%")
    return this.costFilesItemsCalculationsHelper.calculateUpstream_Percentage(fileType , cells);
  };

  changeCurrency(currency) {
    if (currency == this.offerCurrency) {
      this.toaster.info("You selected the same offer currency, conversions will not be applied.", "Currency conversion is not needed", {
        hasIcon: true,
        destroyByClick: true
      })
    } else {
      this.selectedCurrency = currency;
      let selectedCurrencyConversions = this.currencyRates["EGP"];
      this.conversionRate = selectedCurrencyConversions.rates.find(x => x.to == this.offerCurrency)["rate"];
    }
  }
}
