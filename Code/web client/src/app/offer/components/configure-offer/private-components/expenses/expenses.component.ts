import { Component, OnInit, ViewChild } from '@angular/core';
import { objectTransfer, urls, httpService, localStorageService, constants, responseModel } from './../../../../../@core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { ModalBodyComponent } from '../../../../../@theme/components';

@Component({
  selector: 'expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.scss']
})
export class ExpensesComponent implements OnInit {
  offerNumber;
  

  tabs: any[] = [
    {
      title: 'Items expenses',
      route: '../configure/items-expense',
    },
    {
      title: 'Family Summary',
      route: '../configure/family-summary',
    },
    {
      title: 'E2E Costing',
      route: '../configure/e2e'
    }
  ];
  loggedInUser: any;
  isApprovalCycleActivated: boolean = false;
  offerStatus: any;



  constructor(
    private objectTransfer: objectTransfer,
    private toaster: NbToastrService,
    private router: Router, 
    private route: ActivatedRoute,
    private httpService: httpService,
    private storageService: localStorageService,
    private activatedRoute: ActivatedRoute,
    private dialogeService: NbDialogService) { 
      this.activatedRoute.queryParams.subscribe((queryParams) => {
        this.isApprovalCycleActivated = queryParams["approval"];
      })
     }

  ngOnInit() {
    this.objectTransfer.objectPuplisher({ isActive: true , name: 'configure' });
    this.loggedInUser = this.storageService.getLocalStorage("loggedInUser");
    this.offerStatus = this.storageService.getLocalStorage("offerStatus");
    this.route.parent.params.subscribe((params) => {
      this.offerNumber = params.offerNumber;
    })
  };

  backToOfferDetails(){
    if(this.isApprovalCycleActivated)
      this.router.navigate(['offers/configure-offer' , this.offerNumber , 'details'] , { queryParams : { approval: true }});
    else 
      this.router.navigate(['offers/configure-offer' , this.offerNumber , 'details'] );
  }


  submitForApproval(){
    this.dialogeService.open(ModalBodyComponent, {
      context: {
        title: "<i class='fa fa-check-square' aria-hidden='true'></i> &nbsp; Confirm Submitting offer",
        message: `Are you sure you want to submit the offer for approval ?`
      }
    }).onClose.subscribe((confirmation) => {
      if(confirmation){
        this.submitToServer();
      }
    })
  };

  submitToServer(){
    let submitForApprovalURL = `${urls.SUBMIT_OFFER}/${this.offerNumber}`;
    let offerTimeLine = {
      "changedAt": new Date(),
      "changedBy": this.loggedInUser.userSESA,
      "newStatus": constants.OFFER_STATUS.SUBMITTED,
      "oldStatus": constants.OFFER_STATUS.ASSIGNED,
    };

    this.httpService.Post(submitForApprovalURL , {} , offerTimeLine ).subscribe((serverResponse) => {
      let response = serverResponse as responseModel;
      if(!response.error){
        this.router.navigate(['../users/dashboard']);
        this.toaster.success("Offer has been submitted successfully", "Offer submitted" , {
          destroyByClick: true, 
          hasIcon: true
        });
      }
    })
  };


  confirmApprovalAction(action){
    let title = "";
    let message = "";

    if(action == "aprove"){
      title = "<i class='fa fa-check' aria-hidden='true'></i> &nbsp; Confirm Approving the offer";
      message = `This action is permenant <br> Are you sure you want to approve the offer and proceed with the approval cycle?`;
    } else if (action == "reject"){
      title = "<i class='fa fa-ban' aria-hidden='true'></i> &nbsp; Confirm rejecting the offer";
      message = `This action is permenant <br> Are you sure you want to reject the offer and proceed with the approval cycle?`;
    }

    this.dialogeService.open(ModalBodyComponent, {
      context: {
        title: title,
        message: message
      }
    }).onClose.subscribe((confirmation) => {
      if(confirmation){
        if(action == "approve"){
          this.approveOffer();
        } else if (action == "reject") {
          this.rejectOffer();
        }
      }
    })
  };

  approveOffer(){
    let approveOfferURL = `${urls.APPROVE_OFFER}/${this.offerNumber}`;

    this.httpService.Get(approveOfferURL , {}).subscribe((serverResponse) => {
      let response = serverResponse as responseModel;

      if(!response.error){
        this.router.navigate(['../users/dashboard']);
        this.toaster.success("Offer has been approved", "Offer Approved" , {
          destroyByClick: true, 
          hasIcon: true
        });
      }
    })
  };

  rejectOffer(){

    let rejectOfferURL = `${urls.REJECT_OFFER}/${this.offerNumber}`;

    this.httpService.Get(rejectOfferURL , {}).subscribe((serverResponse) => {
      let response = serverResponse as responseModel;

      if(!response.error){
        this.router.navigate(['../users/dashboard']);
        this.toaster.success("Offer has been rejected and notification sent to the tendering engineer.", "Offer Rejected" , {
          destroyByClick: true, 
          hasIcon: true
        });
      }
    })
  };
  
  
  
}
