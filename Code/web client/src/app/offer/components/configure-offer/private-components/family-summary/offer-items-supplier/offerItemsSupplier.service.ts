import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
  
export class offerItemsSupplier {
    supplyOfferItems$: Observable<any>;
    private offerItems = new Subject<any>();

    constructor() {
        this.supplyOfferItems$ = this.offerItems.asObservable();
    }

    supplyOfferItems(data) {
        this.offerItems.next(data);
    }
}