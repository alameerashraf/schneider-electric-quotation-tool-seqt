import { Component, OnInit} from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ModalBodyComponent } from '../../../../../@theme/components';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { ItemsWatcherService } from './items-watcher/items-watcher.service';
import { urls ,localStorageService , httpService , responseModel, constants } from './../../../../../@core';
import { offerItems, claculationFormulas, calculationFields, expensesManagerCalculations } from '../../../../helpers'
import { CloneItemsComponent } from '../clone-items/clone-items.component';
import { ExpensesManagerComponent } from '../expenses-manager/expenses-manager.component';
import { UploadEasFileComponent } from '../upload-eas-file/upload-eas-file.component';
import { itemsExpenses , offerItem, itemsGridTotals, expense } from '../../../../models';
import { offerItemsSupplier } from '../family-summary/offer-items-supplier/offerItemsSupplier.service';

@Component({
  selector: "items-expenses",
  templateUrl: "./items-expenses.component.html",
  styleUrls: ["./items-expenses.component.scss"]
})
export class ItemsExpensesComponent implements OnInit {
  itemsHelper = new offerItems();
  expensesCalculationsHelper = new expensesManagerCalculations();
  itemCalculationsFormulasHandler = new claculationFormulas();
  itemCalculationsFieldsHandler = new calculationFields();

  inValidIds = [];
  unSavedIds = [];
  changedIds = [];
  savedIds = [];
  itemHasChanges = false;
  itemsTableHeaders = [
    {
      headerTitle: "Item No",
      style: {
        width: "50"
      }
    },
    {
      headerTitle: "Activity",
      style: {
        width: "90"
      }
    },
    {
      headerTitle: "Product",
      style: {
        width: "90"
      }
    },
    {
      headerTitle: "Description",
      style: {
        width: "180"
      }
    },
    {
      headerTitle: "Qty",
      style: {
        width: "80"
      }
    },
    {
      headerTitle: "Unit Cost excluding expenses",
      style: {
        width: "110"
      }
    },
    
    {
      headerTitle: "Total Cost excluding expenses",
      style: {
        width: "110"
      }
    },
    {
      headerTitle: "Unit Cost including expenses",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Total Cost including expenses",
      style: {
        width: "110"
      }
    },
    {
      headerTitle: "Expenses per unit",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Total expenses",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Expenses %",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Unit SP2",
      style: {
        width: "120"
      }
    },
    {
      headerTitle: "Total SP2",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Selling CCO GM margin % (SP2)", 
      style: {
        width: "120"
      }
    },
    {
      headerTitle: "Selling Local margin - statory % (SP2)", 
      style: {
        width: "120"
      }
    },
    {
      headerTitle: "CCOGM% Threshold",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Unit SP1 (Guide)",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Total SP1 (Guide)",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "IG/OG",
      style: {
        width: "60"
      }
    },
    {
      headerTitle: "Number of cells",
      style: {
        width: "80"
      }
    },
    {
      headerTitle: "Upstream %",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Upstream amount/unit",
      style: {
        width: "120"
      }
    },
    // new 
    {
      headerTitle: "Selling CCO CM%", //CCOCM_Percentage
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Upstream SFC %",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Upstream SFC amount/unit",
      style: {
        width: "120"
      }
    },
    {
      headerTitle: "FO SFC%",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "FO SFC amount/unit",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Total SFC amount",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Total SFC %",
      style: {
        width: "100"
      }
    },
    {
      headerTitle: "Package",
      style: {
        width: "60"
      }
    },
    {
      headerTitle: "Source",
      style: {
        width: "80"
      }
    },
    {
      headerTitle: "Comment",
      style: {
        width: "200"
      }
    }
  ];


  offerItems: offerItem[] = [];
  changedOfferItems: offerItem[] = [];

  totalCostExcludingExpensesForTheOffer = 0;
  offerNumber: any;
  changedFieldsPerChangedItem: any[];
  itemOnEdit: any;

  itemsTotals = new itemsGridTotals();

  allExpensesAddedToItems: itemsExpenses[] = [];
  allGeneralExpenses: expense[] = [];
  isAllItemsSelected: boolean = false;
  selectedItems: any[] = [];
  
  tempAddedGeneralExpenses: any[] = [];
  offerClassification: any;

  constructor(
    private dialogeService: NbDialogService,
    private toaster: NbToastrService,
    public dialog: MatDialog,
    private itemsWatcher: ItemsWatcherService,
    private toasterService: NbToastrService,
    private localStorageService: localStorageService,
    private httpService: httpService,
    private itemsSupplier: offerItemsSupplier
  ) {
    this.offerClassification = localStorageService.getLocalStorage("offerClassification");
  }

  ngOnInit() {
    this.itemsHelper.goFullScreen();
    this.offerNumber = this.localStorageService.getLocalStorage("offerNumber");
    this.loadOfferItemsFromDB();
    // Get changes everytime user changes a value!
    this.itemsWatcher.items.subscribe(changes => {

      let unSavedChnages = changes.filter(x => x.IS_SAVED == false && x.IS_LOADED_FROM_DB == false);

      unSavedChnages.forEach(element => {
        if(this.changedOfferItems.length == 0){
          this.changedOfferItems.push(element);
        } else {
          if(this.changedOfferItems.find(x => x.itemNo.value == element.itemNo.value) == undefined){
            this.changedOfferItems.push(element);
          }
        }
      });

      
      let changedFieldsPerChangedItem = this.itemsHelper.getChangedFields(this.changedOfferItems);
      this.calculateAllChangedFields(changedFieldsPerChangedItem);
      this.itemsSupplier.supplyOfferItems(this.offerItems)
    });

  };

  /**
   * Commit changes in each cell in the grid!
   */
  commitChange(itemId, changedField, changedValue) {
    if(this.offerItems.length > 0){
      this.itemHasChanges = true
    };

    this.itemOnEdit = itemId.value;
    if(changedField == "CCOGM_Percentage")
      changedValue = changedValue / 100;
    
    this.offerItems.find(x => x.itemNo.value == itemId.value)[changedField].value = changedValue;

    this.offerItems.find(x => x.itemNo.value == itemId.value)[changedField].isChanged = true;
    this.offerItems.find(x => x.itemNo.value == itemId.value)['IS_SAVED'] = false;
    this.offerItems.find(x => x.itemNo.value == itemId.value)['IS_LOADED_FROM_DB'] = false;
    this.itemsWatcher.change_item(this.offerItems);

    // Calculations 
    this.calculate(changedField , itemId.value);
    this.itemsWatcher.change_item(this.offerItems);

    this.calculateGridTotals();
    this.calculateItemsWeight();
  };


  calculateAllChangedFields(changedFields){
    let changedFieldsPerItem = changedFields.filter((field) =>{
      return field.itemNo == this.itemOnEdit;
    });


    changedFieldsPerItem.forEach(element => {
      if(!element.isCalulated){
        this.calculate(element.changedField , element.itemNo);
        element.isCalulated = true;
      }
    });

  };


  calculate(fieldName , itemId , max=0){
    let fields = {};
    let maxIterationNumber = max;
    let isIterative = false;
    let item = this.offerItems.find(x => x.itemNo.value == itemId);
    //1) get affected fileds by a currently changed filed 
    //2) get the formula fields for each affected field

    // 1))
    let affectedFields = this.itemCalculationsFieldsHandler.getAffectedFields(fieldName);

    //*))
    let selfFormulaFields = this.itemCalculationsFieldsHandler.getFormulaFields(fieldName);

    if (selfFormulaFields != undefined && affectedFields != undefined) {
      let selfFieldsParams = [];
      let iterativeFields = [];
      var isIterativeEnabled = selfFormulaFields.filter(function (val) {
        return affectedFields.indexOf(val) != -1;
      });

      // there are depandancies 
      if (isIterativeEnabled.length > 0) {
        isIterative = true;
        maxIterationNumber = maxIterationNumber + 1;

        // Remove iterative fields from being recalculated!
        isIterativeEnabled.forEach((duplicateElement) => {
          affectedFields.splice(affectedFields.indexOf(duplicateElement), 1);
          iterativeFields.push(duplicateElement);
        });

        selfFormulaFields.forEach((field) => {
          selfFieldsParams[field] = item[field].value;
        });

        let valueOfCurrentField = this.itemCalculationsFormulasHandler.calculate(fieldName, selfFieldsParams);
        item[fieldName].value = valueOfCurrentField;
        item[fieldName].isChanged = true;


        iterativeFields.forEach(element => {
          let fieldsForIterative = [];
          let formulaFields = this.itemCalculationsFieldsHandler.getFormulaFields(element);
          formulaFields.forEach((field) => {
            fieldsForIterative[field] = item[field].value;
          });
          let valueOfCurrentField = this.itemCalculationsFormulasHandler.calculate(element, fieldsForIterative);
          item[element].value = valueOfCurrentField;
          item[element].isChanged = true;

        });
      }

      if (maxIterationNumber <= 100 && isIterativeEnabled.length > 0)
        this.calculate(fieldName, itemId, maxIterationNumber);
    };

    if (affectedFields !== undefined) {
      affectedFields.forEach(element => {
        let formulaFields = this.itemCalculationsFieldsHandler.getFormulaFields(element);
        formulaFields.forEach((field) => {
          fields[field] = item[field].value;
        });

        let calculatedValue = this.itemCalculationsFormulasHandler.calculate(element, fields);
        if (calculatedValue != undefined && calculatedValue != "" && !isNaN(calculatedValue)) {
          item[element].value = calculatedValue;
          item[element].isChanged = true;
        }
      });
    }

    if (affectedFields !== undefined) {
      affectedFields.forEach((field) => {
        this.calculate(field, itemId);
      })
    }
  };

  //** calculate Items totals */
  calculateGridTotals(){
    this.itemsTotals = new itemsGridTotals();

    this.offerItems.forEach((item) => {
      Object.keys(item).forEach((k) => {
        if (!constants.OFFER_ITEMS_UI_HELPER_FIELDS.includes(k)) {
          if (item[k].value == null || item[k].value == undefined) {
            item[k].value = 0;
          }
        }
      });
    });

    this.offerItems.forEach((item) => {
      this.itemsTotals.totals_totalCostExcludingExpenses =
        this.itemsTotals.totals_totalCostExcludingExpenses + parseFloat(item.totalCostExcludingExpenses.value.toString().replace(/,/g , ''));

      this.itemsTotals.totals_totalCostIncludingExpenses =
        this.itemsTotals.totals_totalCostIncludingExpenses + parseFloat(item.totalCostIncludingExpenses.value.toString().replace(/,/g , '')); 

      this.itemsTotals.totals_totalExpenses =
        this.itemsTotals.totals_totalExpenses + parseFloat(item.totalExpenses.value.toString().replace(/,/g , '')); 

      this.itemsTotals.totals_totalSP2 =
        this.itemsTotals.totals_totalSP2 + parseFloat(item.totalSP2.value.toString().replace(/,/g , ''));

      this.itemsTotals.totals_CCO_GM = 
        this.itemsTotals.totals_CCO_GM + parseFloat(item.CCOGM_Percentage.value.toString().replace(/,/g , ''));

      this.itemsTotals.totals_totalSP1 =
        this.itemsTotals.totals_totalSP1 + parseFloat(item.totalSP1.value.toString().replace(/,/g , '')); 

      this.itemsTotals.totals_Upstream_amount =
        this.itemsTotals.totals_Upstream_amount + parseFloat(item.Upstream_amount.value.toString().replace(/,/g , '')); 

      this.itemsTotals.totals_sellingCCOCM_Percentage =
        this.itemsTotals.totals_sellingCCOCM_Percentage + parseFloat(item.sellingCCOCM_Percentage.value.toString().replace(/,/g , '')); 

      this.itemsTotals.totals_upstreamSFC_amount =
        this.itemsTotals.totals_upstreamSFC_amount + parseFloat(item.upstreamSFC_amount.value.toString().replace(/,/g , '')); 

      this.itemsTotals.totals_fOSFC_amount =
        this.itemsTotals.totals_fOSFC_amount + parseFloat(item.fOSFC_amount.value.toString().replace(/,/g , '')); 

      this.itemsTotals.totals_totalSFC_amount =
        this.itemsTotals.totals_totalSFC_amount + parseFloat(item.totalSFC_amount.value.toString().replace(/,/g , '')); 

      this.itemsTotals.totals_totalSFC_Percentage =
        this.itemsTotals.totals_totalSFC_Percentage + parseFloat(item.totalSFC_Percentage.value.toString().replace(/,/g , '')); 
    });
  };

  //** Item weight bring calculated based on (total cost excluding) */
  calculateItemsWeight() {
    this.offerItems.forEach((item) => {
      item.weight = ((+item.totalCostExcludingExpenses.value / this.itemsTotals.totals_totalCostExcludingExpenses));
    });
  };

  // Load saved items
  loadOfferItemsFromDB(){ 
    let loadingOfferItemsURL = `${urls.LOAD_OFFER_ITEMS}/${this.offerNumber}`;
    this.httpService.Get(loadingOfferItemsURL , {} ,  ).subscribe((offerItems: responseModel) => {
      if(!offerItems.error){
        let items = offerItems.data;

        // Bind items dierctly to the grid
        let allConvertedItems = this.itemsHelper.convertServerObjectsToViewObjects(items , false , true);

        let allItems = [];
        allConvertedItems.forEach((item) => {
          let disabledItem = this.itemsHelper.disableUploadedItems(item , item["UPLOADED_FROM"]);
          allItems.push(disabledItem);
        });

        this.offerItems = this.offerItems.concat(allItems);

        // calculate weight
        this.calculateGridTotals();
        this.calculateItemsWeight();
      } else {
        this.toasterService.danger("Error loading offer items, try again later!" , 
        "Something went wrong" , {
          hasIcon: true,
          destroyByClick: true
        })
      }
    });
  };

  // Add items uploaded from files
  addUploadedItemsFromCostFiles(items, fileType){
    let convertedFieldsValidations = this.itemsHelper.validateUploadedFiles(items);
    if(convertedFieldsValidations.length > 0){
      this.toasterService.danger("EAS file is not valid and containes errors, fix issues and try again." , "Error uploading file" ,{
        hasIcon: true,
        destroyByClick: true,
      });
    } else {
      let convertedItems = this.itemsHelper.convertServerObjectsToViewObjects(items , false , false);


      let allConvertedItems = [];
      convertedItems.forEach(item => {
        let disabledItem = this.itemsHelper.disableUploadedItems(item , fileType);
        allConvertedItems.push(disabledItem);
      });
  
      this.offerItems = this.offerItems.concat(allConvertedItems);
      this.offerItems.forEach((element) => {
        element["UPLOADED_FROM"] = fileType
      });

      this.itemsWatcher.change_item(this.offerItems);
    }
  };

  /** Save chnages of items to server */
  async saveItemsChanges(){
    let allUnsaveOfferItems = [];
    let singleOfferItem = {};

    // Convert view object to server object!
    this.changedOfferItems.forEach(element => {
      if(!element.IS_SAVED){
        singleOfferItem = {};
        Object.keys(element).forEach((k) => {
          singleOfferItem[k] = element[k].value;

          if(k == "UPLOADED_FROM"){
            singleOfferItem[k] = element[k];
          }

          if(k == "expenses"){
            singleOfferItem[k] = [];
            singleOfferItem[k] = singleOfferItem[k].concat(element[k]);
          }
        });
        allUnsaveOfferItems.push(singleOfferItem);
      }
    });

    let result = await this.postItemsToServer(allUnsaveOfferItems) as responseModel;
    this.markSavedItems(result.data);
    this.itemsSupplier.supplyOfferItems(this.offerItems)

    if(!result.error){
      this.toasterService.success("Offer data has been updates successfully", "Data saved successfully" , {
        destroyByClick: true,
        hasIcon: true
      })
    }
  };

  saveExpensesChanges() {
    let expenses = {
      "itemsExpenses": this.allExpensesAddedToItems,
      "generalExpenses": this.allGeneralExpenses
    };


    let itemizedURL = `${urls.ADD_EXPENSES}/${this.offerNumber}`;
    this.httpService.Post(itemizedURL, {}, expenses).subscribe((x) => {
    });
  };


  /**
   * Mark items as saved from 
   * @param savedResultsFromServer 
   */
  markSavedItems(savedResultsFromServer){
    savedResultsFromServer.forEach(id => {
      //Mark item as not changes, since they just loaded!
      let itemsList = this.offerItems.find(x => x.itemNo.value == id);
      Object.keys(itemsList).forEach((k) => {
        if(!constants.OFFER_ITEMS_UI_HELPER_FIELDS.includes(k))
          itemsList[k].isChanged = false
      });
      // Remove already saved items from changing tree!
      let savedItem = this.changedOfferItems.find(x => x.itemNo.value == id);
      let indexOfAlreadySavedItem = this.changedOfferItems.indexOf(savedItem);
      this.changedOfferItems.splice(indexOfAlreadySavedItem , 1);


      this.offerItems.find(x => x.itemNo.value == id)["IS_SAVED"] = true;
      this.offerItems.find(x => x.itemNo.value == id)["expenses"].forEach((savedExpense) => {
        savedExpense.IS_SAVED = true;
      });
    });
  };

  /**
   * Presiste Items to server!
   */
  async postItemsToServer(allUnsaveOfferItems){
    let itemSavingURL = `${urls.BULK_INSERT_ITEMS_TO_OFFER}/${this.offerNumber}`;
    return this.httpService.Post(itemSavingURL , {} , allUnsaveOfferItems ).toPromise();
  };


  /** Add new Item to grid */
  addNewItem(anItem = null){
    let newItem = new offerItem();
    if(anItem == null){
      newItem = new offerItem();
    } else {
      newItem = anItem;
    }
    
    let lastItem: offerItem = null;

    newItem.itemNo.value = this.itemsHelper.getNextItemId(this.offerItems);
    lastItem = newItem.itemNo.value == 1 ? null : this.offerItems.find(x => x.itemNo.value == newItem.itemNo.value - 1);
    newItem.upstreamSFC_Percentage.value = this.itemsHelper.initiateSFC(this.offerClassification);


    if(this.itemsHelper.validateItem(lastItem) || lastItem == null){
      this.offerItems.push(newItem);
    } else if(anItem != null){
      this.offerItems.push(newItem)
    }else {
      // Set warning for the empty fields!
      Object.keys(lastItem).forEach((k) => {
        if(!constants.OFFER_ITEMS_UI_HELPER_FIELDS.includes(k)){
          lastItem[k].hasWarning = true;
        }
      });

      this.toasterService.warning("New item can't be added, previous item is not valid! fix the issues or remove it." , 
      "Warning, Process prevented", {
        hasIcon: true,
        destroyByClick: true,
        duration: 5000,
      })
    };
  };

  deleteSelectedItems() {
    this.dialogeService.open(ModalBodyComponent, {
      context: {
        title: "<i class='fa fa-question-circle' aria-hidden='true'></i> &nbsp; Delete an Item",
        message: `Are you sure you want to delete the selected items? <br> items will be deleted permenantly.`
      }
    }).onClose.subscribe(async (confirmation) => {
      if (confirmation) {
        let itemsToBeRemovedFromServer = this.itemsHelper.getItemsToBeDeletedFromServer(this.offerItems, this.selectedItems);
        let serverResponse = await this.deleteItemsFromServer(this.offerNumber, itemsToBeRemovedFromServer) as responseModel;

        // If done remove all items from the grid and reOrder the grid
        if (!serverResponse.error) {
          
          this.selectedItems.forEach((itemNo) => {
            this.itemsHelper.deleteAnItemFromGrid(itemNo, this.offerItems, this.changedOfferItems);
          });

          this.isAllItemsSelected = false;
          this.selectedItems = [];
          this.itemsHelper.reOrderItems(this.offerItems);
          this.calculateGridTotals();
          this.calculateItemsWeight();
          this.itemsSupplier.supplyOfferItems(this.offerItems)
          this.toasterService.success("Seleted items deleted permenantly from offer.",
            "Items have been seleted successfully", {
            hasIcon: true,
            destroyByClick: true
          })
        }
      }
    })
  };

  //** Delete items from server */
  async deleteItemsFromServer(offerNumber , selectedItems){
    let deletingItemURL = `${urls.DELETE_ITEMS_FROM_OFFER}/${offerNumber}`;
    return this.httpService.Post(deletingItemURL , {} , { itemIds: selectedItems }).toPromise();
  };


  addItemizedExpenses(itemsExpensesList: itemsExpenses[]) {
    if (itemsExpensesList != undefined ) {
      itemsExpensesList.forEach((element) => {
        if (element.expenses.length > 0) {
          let item = this.offerItems.find(x => x.itemNo.value == element.itemNo);
          this.itemOnEdit = element.itemNo;

          let totalNewelyAddedExpenses = this.itemsHelper.calculateExpensePerItem(element.expenses);
          if (totalNewelyAddedExpenses != 0) {
            item["totalExpenses"].value = (this.itemsHelper.convertGridValuesToNumbers(item.totalExpenses.value)
              + totalNewelyAddedExpenses).toString();
            item["totalExpenses"].isChanged = true;
            item['IS_SAVED'] = false;
            item['IS_LOADED_FROM_DB'] = false;
          }
          this.itemsWatcher.change_item(this.offerItems);
        }
      });

      // Presiste expenses
      this.allExpensesAddedToItems = this.allExpensesAddedToItems.concat(itemsExpensesList);
      this.itemsHelper.bindExpensesToMainGridItems(this.offerItems , this.allExpensesAddedToItems);
    }
  };

  addGeneralExpenses(generalExpensesList: expense[]){

    if(generalExpensesList != undefined && generalExpensesList.length != 0 ){
      this.allGeneralExpenses = generalExpensesList;
      this.tempAddedGeneralExpenses = [];


      let addedGeneralExpensesPercentage = generalExpensesList.filter((singleGeneralExpense) => {
        return singleGeneralExpense.valueType == "Percentage"
      });
  
  
      let addedGeneralExpensesNonPercentage = generalExpensesList.filter((singleGeneralExpense) => {
        return (singleGeneralExpense.valueType == "Hours" || singleGeneralExpense.valueType == "Amount");
      });
  
  
      let combinedNonPercentageGeneralExpense = this.expensesCalculationsHelper.combineAllGeneralExpenses(addedGeneralExpensesNonPercentage);
      this.calculateNonPercentageGeneralExpenses(combinedNonPercentageGeneralExpense , combinedNonPercentageGeneralExpense.value);
  
      let combinedPercentageGeneralExpense = this.expensesCalculationsHelper.combineAllGeneralExpenses(addedGeneralExpensesPercentage);
      this.calculatePercentageGeneralExpenses(combinedPercentageGeneralExpense , combinedPercentageGeneralExpense.value);
    }
  };


  //#region General Expenses   

  calculatePercentageGeneralExpenses(addedExpense: expense, addedExpenseValue, max = 0) {
    let maxIteration = max + 1;


    let totalSP2_totals = this.itemsTotals.totals_totalSP2;
    let expensesListPerItem =
      this.expensesCalculationsHelper.calculateGeneralExpensesforEachItem(this.offerItems, addedExpense, addedExpenseValue, totalSP2_totals);

    if (expensesListPerItem.length != 0) {
      expensesListPerItem.forEach((element) => {

        let expensePerItem = {
          itemNo: element.itemNo,
          currentIterationValue: element.value
        };

        let oldIterationValue = this.updateTempStorageForGeneralExpenses(expensePerItem);

        let item = this.offerItems.find(x => x.itemNo.value == element.itemNo);
        this.itemOnEdit = element.itemNo;

        let totalNewelyAddedExpenses = Math.abs(element.value - oldIterationValue);

        item["totalExpenses"].value = (this.itemsHelper.convertGridValuesToNumbers(item.totalExpenses.value)
          + totalNewelyAddedExpenses).toString();
        item["totalExpenses"].isChanged = true;
        item['IS_SAVED'] = false;
        item['IS_LOADED_FROM_DB'] = false;

        // Add "General Expense" to the items expenses
        this.addGeneralExpenseToItemsExpenses(element.itemNo , totalNewelyAddedExpenses);
        this.itemsWatcher.change_item(this.offerItems);
      });
    };

    this.calculateGridTotals();
    this.calculateItemsWeight();
    if (maxIteration <= 10 && addedExpense.valueType == "Percentage") {
      this.calculatePercentageGeneralExpenses(addedExpense, addedExpenseValue, maxIteration);
    }
  };

  calculateNonPercentageGeneralExpenses(addedExpense: expense, addedExpenseValue){
    let expensesListPerItem =
      this.expensesCalculationsHelper.calculateGeneralExpensesforEachItem(this.offerItems, addedExpense, addedExpenseValue);

    if (expensesListPerItem.length != 0) {
      expensesListPerItem.forEach((element) => {

        let expensePerItem = {
          itemNo: element.itemNo,
          value: element.value
        };

        let item = this.offerItems.find(x => x.itemNo.value == element.itemNo);
        this.itemOnEdit = element.itemNo;

        let totalNewelyAddedExpenses = expensePerItem.value;

        item["totalExpenses"].value = (this.itemsHelper.convertGridValuesToNumbers(item.totalExpenses.value)
          + totalNewelyAddedExpenses).toString();
        item["totalExpenses"].isChanged = true;
        item['IS_SAVED'] = false;
        item['IS_LOADED_FROM_DB'] = false;

        // Add "General Expense" to the items expenses
        this.addGeneralExpenseToItemsExpenses(element.itemNo , totalNewelyAddedExpenses);
        this.itemsWatcher.change_item(this.offerItems);
      });
    };
  }


  updateTempStorageForGeneralExpenses(addedExpensePerItem){
    let targetItem = this.tempAddedGeneralExpenses.find(x => x.itemNo == addedExpensePerItem.itemNo);
    let perValue = 0;
    if(targetItem == undefined){
      this.tempAddedGeneralExpenses.push(addedExpensePerItem);
      return 0;
    } else {
      perValue = targetItem.currentIterationValue;
      targetItem.currentIterationValue = addedExpensePerItem.currentIterationValue;

      return perValue;
    }
  };

  addGeneralExpenseToItemsExpenses(itemNo, amountOfExpenseToAdd) {
    let item = this.allExpensesAddedToItems.find(x => x.itemNo == itemNo);
    if(item == undefined){
      item = new itemsExpenses();
    }

    let isItemHasGeneralExpenses = item.expenses.find(x => x.name == "General Expense");

    let generalExpense = new expense();
    generalExpense.name = "General Expense";
    generalExpense.type = constants.EXPENSES_TYPES["general"];
    generalExpense.IS_SAVED = false;
    generalExpense.valueType = "";

    if (isItemHasGeneralExpenses == undefined) {
      generalExpense.value = amountOfExpenseToAdd;
      item.expenses.push(generalExpense);
    } else {
      isItemHasGeneralExpenses.value = isItemHasGeneralExpenses.value + amountOfExpenseToAdd;
    }
  };
  //#endregion


  openUploadCostFilesModal() {
    const dialogRef = this.dialog.open(UploadEasFileComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        let uploadedItems = result.uploadedItems;
        let selectedFileType = result.typeFile;

        let latestId = this.itemsHelper.getLatestItemId(this.offerItems);
        let itemWithIds = this.itemsHelper.bindItemsIds(uploadedItems , latestId);
        this.addUploadedItemsFromCostFiles(itemWithIds , selectedFileType);
        this.calculateGridTotals();
        this.calculateItemsWeight();
      }
    });
  };

  openCloneItemsModal(){
    const dialogRef = this.dialog.open(CloneItemsComponent ,  { data : this.offerItems });
    dialogRef.afterClosed().subscribe((result) => {
      this.addClonedItemsToGrid(result);
      this.itemsWatcher.change_item(this.offerItems);
      this.calculateGridTotals();
      this.calculateItemsWeight();
    })
  };

  openExpensesManagerModal(){
    const dialogRef = this.dialog.open(ExpensesManagerComponent , { data: this.offerItems });
    dialogRef.afterClosed().subscribe((result) => {

      if(result != null){
        this.allExpensesAddedToItems = [];
        this.addItemizedExpenses(result["itemsExpenses"]);
        this.addGeneralExpenses(result["generalExpenses"]);
      }
    })
  };

  addClonedItemsToGrid(items) {
    items.forEach(element => {
      this.addNewItem(element);
    });
  }

  selectAllItems() {
    this.isAllItemsSelected = !this.isAllItemsSelected;
    this.offerItems.forEach((item) => {
      item.IS_SELECTED = this.isAllItemsSelected;
      if(item.IS_SELECTED)
        this.selectedItems.push(item.itemNo.value);
      else 
        this.selectedItems.splice(this.selectedItems.indexOf(item.itemNo.value) , 1)
    });
  };

  selectItem(itemNo){
    let item = this.offerItems.find(x => x.itemNo.value == itemNo);
    item.IS_SELECTED = !item.IS_SELECTED;

    if(item.IS_SELECTED)
      this.selectedItems.push(item.itemNo.value)
    else 
      this.selectedItems.splice(this.selectedItems.indexOf(item.itemNo.value) , 1)

    if(this.isAllItemsSelected)
      this.isAllItemsSelected = false;
  };


};