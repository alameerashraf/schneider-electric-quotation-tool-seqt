import { Component, OnInit } from '@angular/core';
import { offerItems } from './../../../../helpers/offer-items/offer-items';
import { offerItemsSupplier } from './offer-items-supplier/offerItemsSupplier.service';
import { localStorageService } from './../../../../../@core/services/local-storage/local-storage';
import { itemsFamilySummary } from './../../../../models/items-family-summary';

@Component({
  selector: "family-summary",
  templateUrl: "./family-summary.component.html",
  styleUrls: ["./family-summary.component.scss"],
})
export class FamilySummaryComponent implements OnInit {
  itemsHelper = new offerItems();
  familyHeaderElements = [
    {
      headerTitle: "Family",
      style: {
        width: "90",
      },
    },
    {
      headerTitle: "Sales Family",
      style: {
        width: "90",
      },
    },
    {
      headerTitle: "Total Cost Excluding Expenses",
      style: {
        width: "100",
      },
    },
    {
      headerTitle: "Total Cost Including Expenses",
      style: {
        width: "100",
      },
    },
    {
      headerTitle: "Total SP",
      style: {
        width: "90",
      },
    },
    {
      headerTitle: "Local Margin",
      style: {
        width: "90",
      },
    },
    {
      headerTitle: "SP CCO GM%",
      style: {
        width: "90",
      },
    },
    {
      headerTitle: "Threeshold CCO GM%",
      style: {
        width: "90",
      },
    },
  ];
  data = [];
  currentOfferItems = [];
  summaryItems: itemsFamilySummary[] = [];

  constructor(
    private itemsSupplier: offerItemsSupplier,
    private localStorageService: localStorageService
  ) {
    this.itemsSupplier.supplyOfferItems$.subscribe((data) => {
      this.localStorageService.eraseLocalStorage("CurrentItems");
      this.localStorageService.setLocalStorage("CurrentItems", data);
    });

    this.loadCurrentOfferItems();
    this.initFamilySummary();
  }

  ngOnInit() {}


  loadCurrentOfferItems() {
    this.data = this.localStorageService.getLocalStorage("CurrentItems");
    this.currentOfferItems = this.itemsHelper.convertViewObjectToServerObject(this.data);
    console.log(this.currentOfferItems)
  };

  initFamilySummary(){
    this.currentOfferItems.forEach((item) => {
      let summaryItem = new itemsFamilySummary();

      let isFamilyAdded = this.summaryItems.find(x => x.family == item.family);
      if(isFamilyAdded){
        isFamilyAdded.totalCostExcludingExpenses = +item.totalCostExcludingExpenses 
          + parseFloat(isFamilyAdded.totalCostExcludingExpenses.toString().replace(/,/g , ''));
        isFamilyAdded.totalCostIncludingExpenses = +item.totalCostIncludingExpenses + parseFloat(isFamilyAdded.totalCostIncludingExpenses.toString().replace(/,/g , ''));
        isFamilyAdded.totalSP = +item.totalSP2 + parseFloat(isFamilyAdded.totalSP.toString().replace(/,/g , ''));
        isFamilyAdded.spCCOGM_Percentage = +item.CCOGM_Percentage + 
            isFamilyAdded.spCCOGM_Percentage + parseFloat(isFamilyAdded.spCCOGM_Percentage.toString().replace(/,/g , ''));
        isFamilyAdded.threesholdCCOGM_Percentage = +item.CCOGM_Percentage_Threshold + parseFloat(isFamilyAdded.threesholdCCOGM_Percentage.toString().replace(/,/g , ''));
        isFamilyAdded.localMargin = +item.sellingLocalMargin + parseFloat(isFamilyAdded.localMargin.toString().replace(/,/g , ''));

      } else {
        summaryItem.family = item.family;
        summaryItem.salesFamily = item.family;
        summaryItem.totalCostExcludingExpenses = item.totalCostExcludingExpenses;
        summaryItem.totalCostIncludingExpenses = item.totalCostIncludingExpenses;
        summaryItem.totalSP = item.totalSP2;
        summaryItem.spCCOGM_Percentage = item.CCOGM_Percentage;
        summaryItem.threesholdCCOGM_Percentage = item.CCOGM_Percentage_Threshold;
        summaryItem.localMargin = item.sellingLocalMargin;

        this.summaryItems.push(summaryItem);
      }

    })
  }

}
