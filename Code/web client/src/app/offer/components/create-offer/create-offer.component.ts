import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  objectTransfer,
  localStorageService
} from '../../../@core'
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'seqt-create-offer',
  templateUrl: './create-offer.component.html',
  styleUrls: ['./create-offer.component.scss']
})
export class CreateOfferComponent implements OnInit {

  emptyForm: FormGroup;
  
  forms = {
    metadataForm: this.emptyForm,
    assignmentForm: this.emptyForm
  };

  @ViewChild('stepper', { static: true }) stepper;

  constructor(
    private formBuilder: FormBuilder,
    private objectTransfer: objectTransfer,
    private storageService: localStorageService,
    private titleService: Title
  ) {
    this.titleService.setTitle("Create new offer | SEQT");

  }

  ngOnInit() {

    // Listen to the forms stauts changing, 
    this.objectTransfer.objectSubscriber.subscribe((form: { formName: string, form: FormGroup }) => {
      if (form != null) {
        this.forms[form.formName] = form.form;
      }
    });
  }

  /**
   * Controlled by another component to go to the next step programmatically.
   * @param object {}
   */
  nextStep() {
    this.stepper.next();
    this.stepper.disableStepNavigation = true;
  };



}
