import { Component, OnInit, ViewChild, Output, EventEmitter, AfterViewInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { NbToastrService, NbGlobalPhysicalPosition, NbDialogService } from '@nebular/theme';
import { ModalBodyComponent } from '../../../../../@theme/components/modal-body/modal-body.component';
import { bFOOpportunityModel , paymentTerms, timeLine, offerModel } from '../../../../models';
import { updateKPIS } from '../../../../helpers';

import {
  matchThreshold,
  objectTransfer,
  httpService,
  urls,
  responseModel,
  constants,
  mapper,
  localStorageService,
  lookupsService
} from "../../../../../@core";
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: "main-metadata-form",
  templateUrl: "./main-metadata-form.component.html",
  styleUrls: ["./main-metadata-form.component.scss"]
})
export class MainMetadataFormComponent implements OnInit {
  loggedInUser: any = {};

  // Form variables
  mainMetaDataForm: FormGroup;
  submissionMode: string = "save";
  requiredFieldsOnSubmission = [
    "quotationType1",
    "projectType",
    "paymentMethod",
    "destination",
    "incoTerms",
    "ig_og",
    "standardWarranty"
  ];
  customPatterns = { "0": { pattern: new RegExp("[A-Z]{3}") } };
  // UI variables
  cascadedFromClassification = false;
  @ViewChild("bfo", { static: true }) bfoTab;

  // Events variables
  @Output() nextStepEmitter: any = new EventEmitter();

  // Data Submission
  offer = {};

  updateUserKPIS;
  amountText: string;
  countries: Object;

  constructor(
    private formBuilder: FormBuilder,
    private objectTransfer: objectTransfer,
    private httpService: httpService,
    private toasterService: NbToastrService,
    private dialogService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private storageService: localStorageService,
    private lookupService: lookupsService
  ) {}

  ngOnInit() {
    this.updateUserKPIS = new updateKPIS(this.httpService);

    this.loggedInUser = this.storageService.getLocalStorage(
      constants.LOGGED_IN_USER
    );
    this.bfoTab.toggle();
    this.getCountries();

    // initalizing main form data
    this.initMainMetaDataForm();
    // Push form object when status changed.
    this.mainMetaDataForm.statusChanges.subscribe(result => {
      this.objectTransfer.objectPuplisher({
        formName: "metadataForm",
        form: this.mainMetaDataForm
      });
    });
  }

  initMainMetaDataForm() {
    this.mainMetaDataForm = this.formBuilder.group(
      {
        seRefrence: ["", Validators.required],
        opportunityName: [""],
        ownerName: [""],
        accountName: [""],
        endUserAccount: [""],
        // countryOfDestination: [""],
        marketSegment: [""],
        marketSubSegment: [""],
        solutionCenter: [""],
        rfpReceivedOn: [""],
        quoteSubmissionDate: [""],
        issudQuoteDateToAccount: [""],
        closedDate: [""],
        category: ["", Validators.required],
        leadingBusiness: ["", Validators.required],
        salesStage: ["", Validators.required],
        currencyCode: ["", Validators.required],
        amount: ["", Validators.required],
        classification: ["", Validators.required],
        classification_HUB: [""],
        quotationType1: [""],
        projectType: [""],
        paymentMethod: [""],
        destination: [""],
        ig_og: [""],
        standardWarranty: [""],
        incoTerms: [""],
        advanced: [""],
        dwg: [""],
        fatCad: [""],
        progress: [""],
        delivery: [""],
        sat: [""],
        late1_money: [""],
        late1_period: [""],
        late2_money: [""],
        late2_period: [""],
        late3_money: [""],
        late3_period: [""]
      },
      {
        validators: [
          matchThreshold([
            "advanced",
            "dwg",
            "fatCad",
            "progress",
            "delivery",
            "sat",
            "late1_money",
            "late2_money",
            "late3_money"
          ])
        ]
      }
    );

    // Adding default values for delivery and advanced in paymenmt terms.
    this.mainMetaDataForm.get("advanced").setValue(50);
    this.mainMetaDataForm.get("delivery").setValue(50);
    this.mainMetaDataForm.get("seRefrence").setValue("OP-");
  }

  get f() {
    return this.mainMetaDataForm.controls;
  }


  async getCountries(){
    let countries = await this.lookupService.getCountries() as responseModel;
    this.countries = countries.data;
  };

  /**
   * Form submission..
   */
  async offerSubmission() {
    if (this.submissionMode == "submit") {
      // fully submitting the form and assign
      this.moveToNextStep();
    } else {
      this.dialogService
        .open(ModalBodyComponent, {
          context: {
            title:
              "<i class='fa fa-question-circle' aria-hidden='true'></i> &nbsp; Save offer as drfat.",
            message:
              "Are you sure you want to save this offer as draft? <br> Draft offers will not be submitted or assigned."
          }
        })
        .onClose.subscribe(async confirmation => {
          if (confirmation) {
            // POST offer to server
            var serverResponse = (await this.postOfferToServer()) as responseModel;
            var updateKpisRequest = new updateKPIS(this.httpService);
            let updatedKpisResponse = await updateKpisRequest.updateUserKpis(serverResponse.data.offerNumber) as responseModel;
            if (serverResponse.error == false && updatedKpisResponse.error == false) {
              this.toasterService.success(
                `Offer ${constants.SUCCESSFULL_SAVING}`,
                "Offer saved as draft",
                {
                  hasIcon: true,
                  destroyByClick: false
                }
              );

              // redierct to dashbaord
              this.router.navigate([`../../${urls.USER_DASHBOARD_INTERNAL}`], {
                relativeTo: this.activatedRoute
              });
            } else {
              this.toasterService.danger(
                `${constants.SERVER_ERROR}, offer ${constants.FAILURE_SAVING}`,
                "Error saving offer",
                {
                  hasIcon: true,
                  destroyByClick: false
                }
              );
            }
          }
        });
    }
  }

  /**
   * Submit offer to server
   */
  async postOfferToServer() {
    let offerStauts = constants.OFFER_STATUS.CREATED;
    let payemnetTerms: paymentTerms = {
      advanced: parseInt(this.f["advanced"].value) || 0,
      delivery: parseInt(this.f["delivery"].value) || 0,
      dwgApproval: parseInt(this.f["dwg"].value) || 0,
      fat_cad: parseInt(this.f["fatCad"].value) || 0,
      progress: parseInt(this.f["progress"].value) || 0,
      sat: parseInt(this.f["sat"].value) || 0,
      late1: {
        amount: parseInt(this.f["late1_money"].value) || 0,
        time: parseInt(this.f["late1_period"].value) || 0
      },
      late2: {
        amount: parseInt(this.f["late2_money"].value) || 0,
        time: parseInt(this.f["late2_period"].value) || 0
      },
      late3: {
        amount: parseInt(this.f["late3_money"].value) || 0,
        time: parseInt(this.f["late3_period"].value) || 0
      }
    };

    let move: timeLine = {
      newStatus: offerStauts,
      oldStatus: "",
      changedAt: new Date(),
      changedBy: this.loggedInUser.userSESA,
      owner: this.loggedInUser.userSESA,
      isLatest: true
    };

    let createdOffer: offerModel = mapper.map(
      this.mainMetaDataForm.value,
      offerModel
    );

    createdOffer.offerNumber = await this.createOfferNumber(
      this.f["seRefrence"].value
    );
    createdOffer.amount = parseInt(this.f["amount"].value);
    createdOffer.currentStatus = offerStauts;
    createdOffer.paymentTerms = payemnetTerms;
    createdOffer.createdBy = this.loggedInUser.userSESA;
    createdOffer.offerTimeline = [];
    createdOffer.offerTimeline.push(move);

    // POST TO SERVER
    let postURL = urls.POST_OFFER_TO_SERVER;
    return this.httpService
      .Post(
        postURL,
        {},
        {
          offer: createdOffer,
          user: this.loggedInUser.userSESA
        }
      )
      .toPromise();
  }

  /**
   * Move the stepper to the next step (Offer assignement)
   */

  async moveToNextStep() {
    this.addSubmissionModeValidation();
    // Don't move to the other form until validation ends.
    if (this.mainMetaDataForm.valid) {
      // In submit create mode only post offer to server when the form is valid.
      let serverResponse = (await this.postOfferToServer()) as responseModel;
      if (serverResponse.error == false) {
        this.objectTransfer.objectPuplisher({
          name: "createdOfferMetadata",
          offer: serverResponse.data
        });
        this.nextStepEmitter.emit({});
      }
    }
  }

  /**
   * Add required validations to fields required on submission.
   */
  addSubmissionModeValidation() {
    this.requiredFieldsOnSubmission.forEach(element => {
      this.mainMetaDataForm.controls[element].setValidators(
        Validators.required
      );
      this.mainMetaDataForm.controls[element].updateValueAndValidity();
    });
  }

  /**
   * Load data of opportunity from bFO!
   */
  loadbFOData() {
    let seRefrence = this.f["seRefrence"].value;
    let bFODataURL = `${urls.LOAD_DATA_FROM_BFO}/${seRefrence}`;
    this.httpService.Get(bFODataURL, {}).subscribe(
      (bFOData: responseModel) => {
        if (!bFOData.error) {
          let data = bFOData.data as bFOOpportunityModel;
          this.f["opportunityName"].setValue(data.opportunityName);
          this.f["ownerName"].setValue(data.ownerName);
          this.f["category"].setValue(data.projectCategoryS1 || "C");
          this.f["accountName"].setValue(data.account.accountName);
          this.f["endUserAccount"].setValue(data.endUserCustomer);
          // this.f["countryOfDestination"].setValue(data.countryOfDestination);
          this.f["marketSegment"].setValue(data.marketSegment);
          this.f["marketSubSegment"].setValue(data.marketSubSegment);
          this.f["solutionCenter"].setValue(data.solutionCenter || "MISSING"); //TODO: MISSING
          this.f["rfpReceivedOn"].setValue(new Date()); //TODO: MISSING
          this.f["quoteSubmissionDate"].setValue(new Date()); //TODO: MISSING
          this.f["issudQuoteDateToAccount"].setValue(new Date()); //TODO: MISSING
          this.f["closedDate"].setValue(new Date(data.closeDate));
          this.f["leadingBusiness"].setValue(data.leadingBusiness);
          this.f["salesStage"].setValue(data.salesStage);
          this.f["amount"].setValue(data.amount);
          this.amountText = this.convertNumToText(data.amount);
          this.f["currencyCode"].setValue(data.currencyCode);
        } else {
          this.toasterService.danger(
            bFOData.message,
            constants.BFO_LOADING_ERROR,
            {
              destroyByClick: false,
              hasIcon: true,
              position: NbGlobalPhysicalPosition.TOP_RIGHT
            }
          );
        }
      },
      (error: Error) => {
        this.toasterService.danger(
          constants.SERVER_ERROR,
          constants.BFO_LOADING_ERROR,
          {
            destroyByClick: false,
            hasIcon: true,
            position: NbGlobalPhysicalPosition.TOP_RIGHT
          }
        );
      }
    );
  }

  /**
   * Crete a random offer number
   */
  async createOfferNumber(opportunityNumber) {
    var latestOfferURL = `${urls.GET_LATEST_OFFER_NUMBER}/${opportunityNumber}`;
    let latestOfferNumberResponse = await this.httpService.Get(latestOfferURL , {}).toPromise();
    let latestOfferNumber = (latestOfferNumberResponse as responseModel).data;

    latestOfferNumber = parseInt(latestOfferNumber) + 1;
    latestOfferNumber = latestOfferNumber.toString().padStart(3 , "0");

    return `${opportunityNumber}*${latestOfferNumber}`;
  }

  /**
   * Change UI when classification changes to show another DD.
   */
  onClassificationChange(selected) {
    if (selected.toLowerCase() == "hub") {
      this.cascadedFromClassification = true;
      this.mainMetaDataForm.controls["classification_HUB"].setValidators(
        Validators.required
      );
      this.mainMetaDataForm.controls[
        "classification_HUB"
      ].updateValueAndValidity();
    } else {
      this.cascadedFromClassification = false;
      this.mainMetaDataForm.controls["classification_HUB"].clearValidators();
      this.mainMetaDataForm.controls[
        "classification_HUB"
      ].updateValueAndValidity();
    }
  }

  convertNumToText(number) {
    const first = [
      "",
      "One ",
      "Two ",
      "Three ",
      "Four ",
      "Five ",
      "Six ",
      "Seven ",
      "Eight ",
      "Nine ",
      "Ten ",
      "Eleven ",
      "Twelve ",
      "Thirteen ",
      "Fourteen ",
      "Fifteen ",
      "Sixteen ",
      "Seventeen ",
      "Eighteen ",
      "Nineteen "
    ];
    const tens = [
      "",
      "",
      "Twenty",
      "Thirty",
      "Forty",
      "Fifty",
      "Sixty",
      "Seventy",
      "Eighty",
      "Ninety"
    ];
    const mad = ["", "Thousand", "Million", "Billion", "Trillion"];
    let word = "";

    for (let i = 0; i < mad.length; i++) {
      let tempNumber = number % (100 * Math.pow(1000, i));
      if (Math.floor(tempNumber / Math.pow(1000, i)) !== 0) {
        if (Math.floor(tempNumber / Math.pow(1000, i)) < 20) {
          word =
            first[Math.floor(tempNumber / Math.pow(1000, i))] +
            mad[i] +
            " " +
            word;
        } else {
          word =
            tens[Math.floor(tempNumber / (10 * Math.pow(1000, i)))] +
            "-" +
            first[Math.floor(tempNumber / Math.pow(1000, i)) % 10] +
            mad[i] +
            " " +
            word;
        }
      }

      tempNumber = number % Math.pow(1000, i + 1);
      if (Math.floor(tempNumber / (100 * Math.pow(1000, i))) !== 0)
        word =
          first[Math.floor(tempNumber / (100 * Math.pow(1000, i)))] +
          "hunderd " +
          word;
    }
    return word;
  };

  convertNumToTextOnKeyUp(number){
    number = number.split(',').join('')
    this.amountText = this.convertNumToText(number);
  }
}
