import { Component, OnInit } from "@angular/core";
import { offerModel, timeLine } from '../../../../models';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { ModalBodyComponent } from '../../../../../@theme/components';
import { Router, ActivatedRoute } from '@angular/router';
import { assignOffer } from '../../../../helpers';

import { 
  objectTransfer ,
  localStorageService,
  httpService,
  urls,
  responseModel,
  constants
 } from "./../../../../../@core";

@Component({
  selector: "assignment-form",
  templateUrl: "./assignment-form.component.html",
  styleUrls: ["./assignment-form.component.scss"]
})
export class AssignmentFormComponent extends assignOffer implements OnInit {


  constructor(
    private objectTransfer: objectTransfer,
    private storageService: localStorageService,
    private httpService: httpService,
    private toasterService: NbToastrService,
    private dialogService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    super(
      objectTransfer,
      storageService,
      httpService,
      toasterService,
      dialogService,
      router,
      activatedRoute
    );
  }

  ngOnInit() {
    this.rediertcToDashboard(`../../${urls.USER_DASHBOARD_INTERNAL}`);
    this.loadCreatedOfferDetails();
    this.getTenderingManagerSubOrdinates();
  }
}
