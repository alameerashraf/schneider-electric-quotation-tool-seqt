import { Component, OnInit, Input, ViewChild, ChangeDetectionStrategy, AfterContentChecked, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService, NbDialogService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';
import { assignOffer } from '../../helpers';
import { offerModel, bFOOpportunityModel } from '../../models';


import{
  httpService,
  localStorageService,
  matchThreshold,
  urls,
  responseModel,
  constants,
  objectTransfer
} from '../../../@core'
import { Title } from '@angular/platform-browser';
import { lookupsService } from './../../../@core/helpers/lookups/lookups.service';
import { ModalBodyComponent } from '../../../@theme/components';


@Component({
  selector: "offer-details",
  templateUrl: "./offer-details.component.html",
  styleUrls: ["./offer-details.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OfferDetailsComponent extends assignOffer implements OnInit {
  mainMetaDataForm: FormGroup;
  requiredFieldsOnSubmission = [
    "quotationType1",
    "projectType",
    "paymentMethod",
    "destination",
    "incoTerms",
    "ig_og",
    "standardWarranty"
  ];
  customPatterns = { "0": { pattern: new RegExp("[A-Z]{3}") } };

  @ViewChild("bfo", { static: true }) bfoTab;
  @ViewChild("opportunityNumber" , { static: true }) opportunityNumberField: ElementRef;
  cascadedFromClassification: boolean;
  offerNumber: any;
  configureMode: string;
  isApprovalCycleActivated: boolean = false;

  selectedEmployeeSESA: string = "";
  selectedEmployeeName: any;

  listOfSubordinates: any[] = [];
  loggedInUser: any;

  currrentTitle: string = "";
  currentTitleIcon: string = "";
  amountText: string;
  countries: any;

  currentOfferFormValue: any = {};
  isChnagedOnPreview = false;

  notAllowedToSubmitAfterChnages = true;
  changes: any[] = [];

  constructor(
    private objectTransfer: objectTransfer,
    private formBuilder: FormBuilder,
    private httpService: httpService,
    private toasterService: NbToastrService,
    private dialogService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private storageService: localStorageService,
    private titleService: Title,
    private lookupService: lookupsService,
    private toaster: NbToastrService
  ) {
    super(
      objectTransfer,
      storageService,
      httpService,
      toasterService,
      dialogService,
      router,
      activatedRoute
    );
  }

  ngOnInit() {
    this.getCountries();
    this.rediertcToDashboard(`../../../${urls.USER_DASHBOARD_INTERNAL}`);
    this.initMainMetaDataForm();
    this.getOpportunityNumberFromURL();
  }

  async getCountries() {
    let countries = (await this.lookupService.getCountries()) as responseModel;
    this.countries = countries.data;
    this.loadOfferData();
  }

  getOpportunityNumberFromURL() {
    this.loggedInUser = this.storageService.getLocalStorage(
      constants.LOGGED_IN_USER
    );

    let urlParts = this.router.url;
    if (urlParts.includes("configure-offer")) {
      this.configureMode = "configure";
      this.currrentTitle = "Configure offer metadata";
      this.currentTitleIcon = "settings-2-outline";
    }
     else if (urlParts.includes("offer-details")) {
      this.titleService.setTitle("Preview offer details | SEQT");
      this.configureMode = "details";
      this.currrentTitle = "Preview an offer";
      this.currentTitleIcon = "eye-outline";
    } 
    else if (urlParts.includes("assign-offer")) {
      this.titleService.setTitle("Assign offer to engineer | SEQT");
      this.configureMode = "assignment";
      this.currrentTitle = "Assign offer to tendering Engineer";
      this.currentTitleIcon = "person-done-outline";
    }

    if (this.configureMode == "configure") {
      this.activatedRoute.parent.params.subscribe(params => {
        this.offerNumber = params["offerNumber"];
        this.f["seRefrence"].setValue(this.offerNumber.split("*")[0]);

        this.activatedRoute.queryParams.subscribe((queryParams) => {
          this.isApprovalCycleActivated = queryParams['approval'];
          if(this.isApprovalCycleActivated){
            this.startApprovalCycle();
          }
        });

      });

    } else {
      this.getTenderingManagerSubOrdinates();
      this.activatedRoute.params.subscribe(params => {
        this.offerNumber = params["offerNumber"];
        this.f["seRefrence"].setValue(this.offerNumber.split("*")[0]);
      });
    }
  }

  /**
   * Init main metadata
   */
  initMainMetaDataForm() {
    this.mainMetaDataForm = this.formBuilder.group(
      {
        seRefrence: ["", Validators.required],
        opportunityName: [""],
        ownerName: [""],
        accountName: [""],
        endUserCustomer: [""],
        countryOfDestination: [""],
        marketSegment: [""],
        marketSubSegment: [""],
        solutionCenter: [""],
        rfpReceivedOn: [""],
        quoteSubmissionDate: [""],
        issudQuoteDateToAccount: [""],
        closedDate: [""],
        category: ["", Validators.required],
        leadingBusiness: ["", Validators.required],
        salesStage: ["", Validators.required],
        currencyCode: ["", Validators.required],
        amount: ["", Validators.required],
        classification: ["", Validators.required],
        classification_HUB: [""],
        quotationType1: [""],
        projectType: [""],
        paymentMethod: [""],
        destination: [""],
        ig_og: [""],
        standardWarranty: [""],
        incoTerms: [""],
        advanced: [""],
        dwg: [""],
        fatCad: [""],
        progress: [""],
        delivery: [""],
        sat: [""],
        late1_money: [""],
        late1_period: [""],
        late2_money: [""],
        late2_period: [""],
        late3_money: [""],
        late3_period: [""]
      },
      {
        validators: [
          matchThreshold([
            "advanced",
            "dwg",
            "fatCad",
            "progress",
            "delivery",
            "sat",
            "late1_money",
            "late2_money",
            "late3_money"
          ])
        ]
      }
    );
  }

  get f() {
    return this.mainMetaDataForm.controls;
  };


  loadOfferData(){
    Promise.all([
      this.loadbFOData(),
      this.loadManualData(),
    ]).then((results) => {
      let bfoData = results[0] as responseModel;
      let manualData = results[1] as responseModel;

      if (!bfoData.error || !manualData.error) {
        let bfo = bfoData.data as bFOOpportunityModel;
        let manual = manualData.data as offerModel;

        this.f["opportunityName"].setValue(bfo.opportunityName);
        this.f["ownerName"].setValue(bfo.ownerName);
        this.f["category"].setValue(bfo.projectCategoryS1 || "C");
        this.f["accountName"].setValue(bfo.account.accountName);
        this.f["endUserCustomer"].setValue(bfo.endUserCustomer);
        this.f["countryOfDestination"].setValue(bfo.countryOfDestination);
        this.f["marketSegment"].setValue(bfo.marketSegment);
        this.f["marketSubSegment"].setValue(bfo.marketSubSegment);
        this.f["solutionCenter"].setValue(bfo.solutionCenter || "MISSING"); //TODO: MISSING
        this.f["rfpReceivedOn"].setValue(new Date()); //TODO: MISSING
        this.f["quoteSubmissionDate"].setValue(new Date()); //TODO: MISSING
        this.f["issudQuoteDateToAccount"].setValue(new Date()); //TODO: MISSING
        this.f["closedDate"].setValue(new Date(bfo.closeDate));
        this.f["leadingBusiness"].setValue(bfo.leadingBusiness);
        this.f["salesStage"].setValue(bfo.salesStage);
        this.f["amount"].setValue(bfo.amount);
        this.amountText = this.convertNumToText(bfo.amount);
        this.f["currencyCode"].setValue(bfo.currencyCode);

        this.f["classification"].setValue(manual.classification);
        this.f["classification_HUB"].setValue(manual.classification_HUB);
        this.f["quotationType1"].setValue(manual.quotationType1);
        this.f["projectType"].setValue(manual.projectType);
        this.f["paymentMethod"].setValue(manual.paymentMethod);
        this.f["incoTerms"].setValue(manual.incoTerms);
        this.f["destination"].setValue(manual.destination);
        this.f["ig_og"].setValue(manual.ig_og);
        this.f["standardWarranty"].setValue(manual.standardWarranty);
        this.f["advanced"].setValue(manual.paymentTerms.advanced);
        this.f["dwg"].setValue(manual.paymentTerms.dwgApproval);
        this.f["fatCad"].setValue(manual.paymentTerms.fat_cad);
        this.f["progress"].setValue(manual.paymentTerms.progress);
        this.f["delivery"].setValue(manual.paymentTerms.delivery);
        this.f["sat"].setValue(manual.paymentTerms.sat);
        this.f["late1_money"].setValue(manual.paymentTerms.late1.amount);
        this.f["late1_period"].setValue(manual.paymentTerms.late1.time);
        this.f["late2_money"].setValue(manual.paymentTerms.late2.amount);
        this.f["late2_period"].setValue(manual.paymentTerms.late2.time);
        this.f["late3_money"].setValue(manual.paymentTerms.late3.amount);
        this.f["late3_period"].setValue(manual.paymentTerms.late3.time);
        
        if (this.configureMode == "details") {
          this.selectedEmployeeSESA =
            manual.offerTimeline.find(
              x => x.newStatus == constants.OFFER_STATUS.ASSIGNED
            ).owner || "";
        }

        this.currentOfferFormValue = this.mainMetaDataForm.value;
        this.checkForFormChanges();
      }
      else{
        this.toasterService.danger(constants.SERVER_ERROR , "Error loading offer data" , {
          destroyByClick: false,
          hasIcon: true,
          position: NbGlobalPhysicalPosition.TOP_RIGHT
        })
      }

    })
  }

  /**
   * Load bFO Data
   */
  loadbFOData() {
    let seRefrence = this.f["seRefrence"].value;
    let bFODataURL = `${urls.LOAD_DATA_FROM_BFO}/${seRefrence}`;
    return this.httpService.Get(bFODataURL, {}).toPromise();
  }

  /**
   * Bind manula entry data.
   */
  loadManualData() {
    let loadOfferData = `${urls.GET_OFFER}/${this.offerNumber}`;
    return this.httpService.Get(loadOfferData, {}).toPromise();
  }

  /**
   * Change UI when classification changes to show another DD.
   */
  onClassificationChange(selected) {
    if (selected.toLowerCase() == "hub") {
      this.cascadedFromClassification = true;
      this.mainMetaDataForm.controls["classification_HUB"].setValidators(
        Validators.required
      );
      this.mainMetaDataForm.controls[
        "classification_HUB"
      ].updateValueAndValidity();
    } else {
      this.cascadedFromClassification = false;
      this.mainMetaDataForm.controls["classification_HUB"].clearValidators();
      this.mainMetaDataForm.controls[
        "classification_HUB"
      ].updateValueAndValidity();
    }
  }

  convertNumToText(number) {
    const first = [
      "",
      "One ",
      "Two ",
      "Three ",
      "Four ",
      "Five ",
      "Six ",
      "Seven ",
      "Eight ",
      "Nine ",
      "Ten ",
      "Eleven ",
      "Twelve ",
      "Thirteen ",
      "Fourteen ",
      "Fifteen ",
      "Sixteen ",
      "Seventeen ",
      "Eighteen ",
      "Nineteen "
    ];
    const tens = [
      "",
      "",
      "Twenty",
      "Thirty",
      "Forty",
      "Fifty",
      "Sixty",
      "Seventy",
      "Eighty",
      "Ninety"
    ];
    const mad = ["", "Thousand", "Million", "Billion", "Trillion"];
    let word = "";

    for (let i = 0; i < mad.length; i++) {
      let tempNumber = number % (100 * Math.pow(1000, i));
      if (Math.floor(tempNumber / Math.pow(1000, i)) !== 0) {
        if (Math.floor(tempNumber / Math.pow(1000, i)) < 20) {
          word =
            first[Math.floor(tempNumber / Math.pow(1000, i))] +
            mad[i] +
            " " +
            word;
        } else {
          word =
            tens[Math.floor(tempNumber / (10 * Math.pow(1000, i)))] +
            "-" +
            first[Math.floor(tempNumber / Math.pow(1000, i)) % 10] +
            mad[i] +
            " " +
            word;
        }
      }

      tempNumber = number % Math.pow(1000, i + 1);
      if (Math.floor(tempNumber / (100 * Math.pow(1000, i))) !== 0)
        word =
          first[Math.floor(tempNumber / (100 * Math.pow(1000, i)))] +
          "hunderd " +
          word;
    }
    return word;
  };

  convertNumToTextOnKeyUp(number) {
    number = number.split(",").join("");
    this.amountText = this.convertNumToText(number);
  };


  startApprovalCycle(){
    let startingCycleURL = `${urls.STARTING_APPROVAL_CYCLE}/${this.offerNumber}`;
    this.httpService.Get(startingCycleURL , null , null).subscribe((serverResponse: responseModel) => {
      if(serverResponse.error){
        this.toasterService.danger("Something went wrong on the server, try again later!", "Internal Server Error" , {
          destroyByClick: true,
          hasIcon: true
        })
      }
    })
  }

  /**
   * Check for changes in offer form!
   */
  checkForFormChanges() {
    this.mainMetaDataForm.valueChanges.subscribe(x => {
      let difference = Object.keys(this.currentOfferFormValue).filter(k => this.currentOfferFormValue[k] !== 
        this.mainMetaDataForm.value[k]);

        if(difference.length > 0){
          this.isChnagedOnPreview = true;
        } else {
          this.isChnagedOnPreview = false;
        };

        this.notAllowedToSubmitAfterChnages = !(this.isChnagedOnPreview && this.mainMetaDataForm.valid);
    });
  };

  getChanges(){
    let changes = {};
    Object.keys(this.currentOfferFormValue).filter(k => this.currentOfferFormValue[k] !== 
      this.mainMetaDataForm.value[k]).map(
      k => {
        let change = {};
        change[k] = this.mainMetaDataForm.value[k];
        changes = Object.assign(changes , change);
      }
    );
  };


  saveChanges(){
    this.getChanges();

  };

  
  confirmApprovalAction(action){
    let title = "";
    let message = "";

    if(action == "approve"){
      title = "<i class='fa fa-check' aria-hidden='true'></i> &nbsp; Confirm Approving the offer";
      message = `This action is permenant <br> Are you sure you want to approve the offer and proceed with the approval cycle?`;
    } else if (action == "reject"){
      title = "<i class='fa fa-ban' aria-hidden='true'></i> &nbsp; Confirm rejecting the offer";
      message = `This action is permenant <br> Are you sure you want to reject the offer and proceed with the approval cycle?`;
    }

    this.dialogService.open(ModalBodyComponent, {
      context: {
        title: title,
        message: message
      }
    }).onClose.subscribe((confirmation) => {
      if(confirmation){
        if(action == "approve"){
          this.approveOffer();
        } else if (action == "reject") {
          this.rejectOffer();
        }
      }
    })
  };

  approveOffer(){
    let approveOfferURL = `${urls.APPROVE_OFFER}/${this.offerNumber}`;

    this.httpService.Get(approveOfferURL , {}).subscribe((serverResponse) => {
      let response = serverResponse as responseModel;

      if(!response.error){
        this.router.navigate(['../users/dashboard']);
        this.toaster.success("Offer has been approved", "Offer Approved" , {
          destroyByClick: true, 
          hasIcon: true
        });
      }
    })
  };

  rejectOffer(){

    let rejectOfferURL = `${urls.REJECT_OFFER}/${this.offerNumber}`;

    this.httpService.Get(rejectOfferURL , {}).subscribe((serverResponse) => {
      let response = serverResponse as responseModel;

      if(!response.error){
        this.router.navigate(['../users/dashboard']);
        this.toaster.success("Offer has been rejected and notification sent to the tendering engineer.", "Offer Rejected" , {
          destroyByClick: true, 
          hasIcon: true
        });
      }
    })
  };




};
