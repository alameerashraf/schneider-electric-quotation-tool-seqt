export class offerModel {
  constructor(init?: Partial<offerModel>){
    Object.assign(this , init)
  }

  offerNumber?: string = "";
  seRefrence?: string = "";
  currentStatus?: string = "";
  opportunityName?: string = "";
  ownerName?: string = "";
  category?: string = "";
  leadingBusiness?: string = "";
  accountName?: string = "";
  endUserCustomer?: string = "";
  countryOfDestination?: string = "";
  salesStage?: string = "" ;
  marketSegment?: string= "";
  marketSubSegment?: string = "";
  solutionCenter?: string = "";
  rfpReceivedOn?: Date = new Date();
  quoteSubmissionDate?: Date = new Date();
  issudQuoteDateToAccount?: Date = new Date();
  closedDate?: Date = new Date();
  amount?: number = 0;
  currencyCode?: string = "";
  classification?: string = "";
  classification_HUB?: string = "";
  quotationType1?: string = "";
  projectType?: string = "";
  paymentMethod?: string = "";
  incoTerms?: string = "";
  destination?: string = "";
  ig_og?: string = "";
  standardWarranty?: string = "";
  paymentTerms?: paymentTerms;
  offerTimeline?: timeLine[] = [];
  createdBy: string = "";
  assignedTo: string = "";
}


export class paymentTerms {
  advanced: number;
  dwgApproval: number;
  fat_cad: number;
  progress: number;
  delivery: number;
  sat: number;
  late1: {
    amount: number;
    time: number;
  };
  late2: {
    amount: number;
    time: number;
  };
  late3: {
    amount: number;
    time: number;
  };
};


export class timeLine {
  newStatus: string = "";
  oldStatus: string = "";
  changedBy: string = "";
  changedAt: Date;
  isLatest: boolean = false;
  owner: string = "";
}
