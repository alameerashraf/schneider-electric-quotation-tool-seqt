import { expense } from './expense';

export class itemsExpenses{
    itemNo: number=0;
    description?: string="";
    totalCostExcludingExpenses?: number=0;
    totalExpenses?: number =0;
    expenses?: expense[]=[]

    // Ui handling attributes
    isExpensesOpened?:boolean=false;
    isSelected?:boolean=false;
    currentlySelectedExpensesPlaceHolder?:string="";
    selectedExpense?: expense=new expense();
    btnIcon?:string ="plus-circle-outline";
    selectedPage?:number=1;
    skip?:number=0;
    limit?:number=3;
}