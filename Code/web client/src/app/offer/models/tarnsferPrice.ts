export class transferPrice {
    IG_OG?: string ="";
    family?: string="";
    product?:string="";
    description?: string="";
    quantity?: number=0;
    unitCostExcludingExpenses?: number=0;
    upstreamSFC_Percentage?: number=0;
    Upstream_Percentage?: number=0;
    totalCostExcludingExpenses?: number=0;
}