class expensesCategories {
    groupName: string;
    expensesList: expensesList[]
};


class expensesList {
    name: string;
    valueType: string;
    itemsAvailability: boolean;
    generalAvailability: boolean;
    hourlyRate: number;
}