export class expense {
    type:string=""; // items or General 
    name:string="";
    value: number;
    valueType:string=""; // value type (hours, amount ..)
    hourlyRate:number=0;

    IS_SAVED?:boolean=false;
    IS_UPDATED?:boolean=false;
    IS_REMOVED?:boolean=false;
    oldValue?:number=0;
}