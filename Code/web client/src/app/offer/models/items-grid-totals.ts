export class itemsGridTotals {
    totals_totalCostExcludingExpenses?:number=0;
    totals_totalCostIncludingExpenses?:number=0;
    totals_totalExpenses?:number=0;
    totals_totalSP2?:number=0;
    totals_CCO_GM?:number=0;
    totals_totalSP1?:number=0;
    totals_Upstream_amount?:number=0;
    totals_sellingCCOCM_Percentage?:number=0;
    totals_upstreamSFC_amount?:number=0;
    totals_fOSFC_amount?:number=0;
    totals_totalSFC_amount?:number=0;
    totals_totalSFC_Percentage?:number=0;
};

