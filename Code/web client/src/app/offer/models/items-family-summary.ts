export class itemsFamilySummary { 
    family?: string;
    salesFamily?: string;
    totalCostExcludingExpenses?: number;
    totalCostIncludingExpenses?: number;
    totalSP?: number;
    localMargin?: number;
    spCCOGM_Percentage?: number;
    threesholdCCOGM_Percentage?: number;
}