import { expense } from './expense';

export class itemsViewModel {
    itemNo: number=0;
    description: string="";
    isSelected?: boolean = false;
    totalCostExcludingExpenses?: number=0;
    weight?: number =0;
    expenses?: expense[] = [];
}