import { expense } from './expense'

export class offerItem {
    constructor(){
        this.unitSP2.isIterative = true;
        this.CCOGM_Percentage.isIterative = true;
    }

    itemNo:{
        value: number,
        isChanged: boolean,
        hasWarning: boolean,
    }= { value:0 , isChanged:false , hasWarning:false }
    family:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean;
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false }
    product:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false }
    description:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false }
    quantity:{
        value: number,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:0 , isChanged:false , hasWarning:false , disabled:false }
    unitCostExcludingExpenses:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false }
    totalCostExcludingExpenses:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    unitCostIncludingExpenses:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    totalCostIncludingExpenses:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    expensesPerUnit:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    totalExpenses:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    Expenses_Percentage:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    unitSP2:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        isIterative?:boolean;
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    totalSP2:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value: "" , isChanged:false , hasWarning:false , disabled:false };
    CCOGM_Percentage:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        isIterative?:boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    sellingLocalMargin:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"", isChanged:false , hasWarning:false , disabled:false };
    CCOGM_Percentage_Threshold:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    unitSP1:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"", isChanged:false , hasWarning:false , disabled:false };
    totalSP1:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    IG_OG:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"", isChanged:false , hasWarning:false ,disabled:false }
    numberOfCells:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    Upstream_Percentage:{ // fixed total removed
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    Upstream_amount:{// fixed total removed
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    sellingCCOCM_Percentage:{ // added selling
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    upstreamSFC_Percentage:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    upstreamSFC_amount:{// fixed total removed
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    fOSFC_Percentage:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    fOSFC_amount:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    totalSFC_amount:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    totalSFC_Percentage:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false };
    package:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"" , isChanged:false , hasWarning:false , disabled:false }
    source:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"", isChanged:false , hasWarning:false , disabled:false};
    comment:{
        value: string,
        isChanged: boolean,
        hasWarning: boolean,
        disabled:boolean,
    }= { value:"", isChanged:false , hasWarning:false , disabled:false};
    expenses?:expense[]=[];


    IS_SAVED:boolean= false;
    IS_SELECTED:boolean= false;
    UPLOADED_FROM:string= "";
    IS_LOADED_FROM_DB:Boolean= false;
    weight:number = 0;
};