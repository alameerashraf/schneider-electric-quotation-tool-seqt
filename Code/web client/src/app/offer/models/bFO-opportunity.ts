export class bFOOpportunityModel {
    seReference: string;
    ownerName: string;
    opportunityName: string;
    projectCategoryP0: string;
    projectCategoryS0: string;
    projectCategoryS1: string;
    leadingBusiness: string;
    account: { seAccountID: number , accountName: string };
    endUserCustomer: string;
    countryOfDestination: string;
    salesStage: string;
    marketSegment: string;
    marketSubSegment: string;
    /**
     * comments
     */
    solutionCenter: { id: number , name: string };
    rfpReceivedOn: string;
    plannedQuoteSubmitDecisionDate: string;
    customerQuoteIssuedDate: string;
    /**
     * comments
     */
    closeDate: string;
    currencyCode: string;
    amount: string;
}