export class eas {
    IG_OG?: string ="";
    family?: string="";
    product?:string="";
    description?: string="";
    quantity?: number=0;
    unitCostExcludingExpenses?: number=0;
    totalCostExcludingExpenses?: number=0;
    unitCostIncludingExpenses?:number=0;
}