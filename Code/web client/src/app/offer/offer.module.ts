import { ModuleWithProviders ,  NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { OfferRoutingModule } from './offer-routing.module';

import * as offerComponents from './components';

import { NBULAR_THEME , NgxMask , MAT_MODAL , NgbModules  } from './offer.imports';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from './../@theme/theme.module';

import {
  objectTransfer,
  httpService,
  localStorageService,
  lookupsService,
  excelService,
  headerInterceptor
} from '../@core';

// import { FsIconComponent } from './components';
import { HTTP_INTERCEPTORS } from '@angular/common/http';



@NgModule({
  declarations: [
    offerComponents.CreateOfferComponent,
    offerComponents.MainMetadataFormComponent,
    offerComponents.AssignmentFormComponent,
    offerComponents.ConfigureOfferComponent,
    offerComponents.OfferDetailsComponent,
    offerComponents.ItemsExpensesComponent,
    offerComponents.ExpensesComponent,
    offerComponents.UploadEasFileComponent,
    offerComponents.AddGeneralExpenseComponent,
    offerComponents.CloneItemsComponent,
    offerComponents.ExpensesManagerComponent,
    offerComponents.ItemsExpensesGridComponent,
    offerComponents.E2eCostingReportComponent,
    offerComponents.FamilySummaryComponent
    // FsIconComponent,
  ],
  entryComponents: [
    offerComponents.UploadEasFileComponent,
    offerComponents.AddGeneralExpenseComponent,
    offerComponents.CloneItemsComponent,
    offerComponents.ExpensesManagerComponent,
    offerComponents.ItemsExpensesGridComponent,
    offerComponents.E2eCostingReportComponent,
  ],
  imports: [
    MAT_MODAL,
    ThemeModule,
    NgxMask.forRoot(),
    ...NBULAR_THEME,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    OfferRoutingModule,
    NgbModules,
  ],
  providers: [
    objectTransfer,
    httpService,
    localStorageService,
    lookupsService,
    excelService,
    { provide: HTTP_INTERCEPTORS , useClass : headerInterceptor , multi : true  },
  ],

})
export class OfferModule { }
