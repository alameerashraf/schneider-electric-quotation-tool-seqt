import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import * as offerComponents from './components';

const routes: Routes = [
  {
    path: 'create-offer',
    component: offerComponents.CreateOfferComponent
  },
  {
    path: 'offer-details/:offerNumber',
    component: offerComponents.OfferDetailsComponent
  },
  {
    path: 'assign-offer/:offerNumber',
    component: offerComponents.OfferDetailsComponent
  },
  {
    path: 'configure-offer/:offerNumber',
    component: offerComponents.ConfigureOfferComponent,
    children: [
      {
        path: 'details',
        component: offerComponents.OfferDetailsComponent
      },
      {
        path: 'configure',
        component: offerComponents.ExpensesComponent,
        children: [
          {
            path: 'items-expense',
            component: offerComponents.ItemsExpensesComponent
          },
          {
            path: 'family-summary',
            component: offerComponents.FamilySummaryComponent
          },
          {
            path: 'e2e',
            component: offerComponents.E2eCostingReportComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferRoutingModule { }
