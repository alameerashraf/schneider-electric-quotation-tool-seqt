export class offerViewModel {
  offerNumber: string;
  seRefrence: string;
  currentStatus: string;
  statusBadge: string;
  actions: any[];
  createdAt: string;
  currencyCode: string;
  createdBy: string;
  opportunityName?:string;
}
