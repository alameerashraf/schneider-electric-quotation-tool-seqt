import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import * as userComponents from './components';


const routes: Routes = [
  {
    path: 'dashboard',
    component: userComponents.DashboardLandingComponent,
  },
  {
    path: 'tendering',
    component: userComponents.DashboardTenderingComponent
  },
  {
    path: 'sales',
    component: userComponents.DashboardSalesComponent
  },
  {
    path: 'pm',
    component: userComponents.DashboardPMComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
