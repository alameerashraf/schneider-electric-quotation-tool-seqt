import { Component, OnInit } from '@angular/core';
import { offerViewModel } from '../../models';
import { Router, ActivatedRoute } from '@angular/router';

import { 
  authorizationService,
  localStorageService,
  responseModel,
  urls,
  httpService,
  constants
 } from './../../../@core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'ngx-dashboard-tendering',
  templateUrl: './dashboard-tendering.component.html',
  styleUrls: ['./dashboard-tendering.component.scss']
})
export class DashboardTenderingComponent implements OnInit {
  offersPages: any[] = [];
  numberOfCarouselPages: number = 0;
  pageSize = 5;
  isManager : boolean = false;

  wedgitList: any[] = [
    {
      type : "info",
      title : "",
      number : 0,
      label : ""
    },
    {
      type : "warning",
      title : "",
      number : 0,
      label : ""
    },
    {
      type : "success",
      title : "",
      number : 0,
      label : ""
    },
    {
      type : "danger",
      title : "",
      number : 0,
      label : ""
    }
  ];
  currentView: string = "";
  alterKPIActionLabel = "";
  managerLabel = "Go to Employee view";
  employeeLabel  = "Back to Manager view";

  numberOfOffers: number = 0 ;
  listOfOffers: offerViewModel [] = [];
  offersCount =  {
    "CREATED_BY_ME_COUNT" : 0,
    "APPROVED_COUNT" : 0,
    "WAITING_FOR_APPROVAL_COUNT" : 0,
    "REJECTED_COUNT" : 0,
    "ASSIGNED_TO_ME_COUNT" : 0
  };
  searchFilter = {
    "CREATED" : false,
    "APPROVED" : false,
    "IN_APPROVAL_CYCLE" : false,
    "REJECTED" : false,
    "ASSIGNED" : false,
    "from" : "",
    "to" : ""
  };
  urlParams = {};

  loggedInUser: any;
  toSkip: number = 0;
  currentSelectedPage: any;

  constructor(
    private storageService: localStorageService,
    private httpService: httpService,
    private router: Router,
    private authorizationService: authorizationService,
    private route: ActivatedRoute,
    private titleService: Title
  ) {
    this.titleService.setTitle("Dashboard | SEQT");
  }

  ngOnInit() {
    this.loggedInUser = this.storageService.getLocalStorage(constants.LOGGED_IN_USER);
    this.isManager = this.authorizationService.isTenderingManager();
    this.loadQueryParams();
    this.alterKPILabel(constants.ROLE_NAMES.TENDERING_MANAGER);
    this.loadKPIs(this.isManager ? constants.ROLE_NAMES.TENDERING_MANAGER : constants.ROLE_NAMES.TENDERING_ENGINEER);
    this.loadFiltersCount();
    this.loadOffers(1 , {});
  };

  loadQueryParams() {
    this.route.queryParams.subscribe((params) => {
      if(params != {}){
        this.loadOffers(1 , params);
      }
    });
  }

  loadOffers(pageNumber , filters){
    this.currentSelectedPage = pageNumber;
    let limit = this.pageSize * pageNumber;
    let skip = Math.abs(this.pageSize - limit);

    let urlParams = this.urlParams = filters == {} ? {} : filters;

    let dashboardOffersURL = `${urls.GET_USERS_OFFERS}/${this.loggedInUser.userSESA}?skip=${skip}&limit=${limit}`;
    this.httpService.Get(dashboardOffersURL , urlParams).subscribe((offers: responseModel) => {
      if(!offers.error){
        this.numberOfOffers = offers.total; 
        this.listOfOffers = offers.data;
        this.listOfOffers.forEach(element => {
          element.statusBadge = this.addStatusLabel(element.currentStatus);
          element.createdAt = new Date(element.createdAt).toLocaleDateString();
          element.actions = this.loadHighestPriorityAction(element.actions);
          element.actions = this.buildActionsBtns(element.actions);
          element.currentStatus = this.getStatusLabel(element.currentStatus , element.createdBy);
        });

        
        this.offersPages = Array.from(Array(Math.ceil(this.numberOfOffers / this.pageSize)).keys());
        if(this.numberOfCarouselPages == 0){
          this.pagingCarousel(this.offersPages);
        }
      }
    });
  };

  loadKPIs(role){
    let kpisUrl = `${urls.GET_USERS_KPIS}/${this.loggedInUser.userSESA}`;
    this.httpService.Get(kpisUrl , {} ).subscribe((kpis: responseModel) => {
      if(!kpis.error){
        let kpisData = kpis.data;
        let loadedKPIs = {};
        if(role == constants.ROLE_NAMES.TENDERING_MANAGER){
          loadedKPIs =  kpisData["manager"];
          this.alterKPILabel(constants.ROLE_NAMES.TENDERING_MANAGER);
        } else if (role == constants.ROLE_NAMES.TENDERING_ENGINEER){
          loadedKPIs = kpisData["employee"];
          this.alterKPILabel(constants.ROLE_NAMES.TENDERING_ENGINEER);
        }

        this.wedgitList = [];
        Object.keys(loadedKPIs).forEach((val , index) => {
          let singleKPI = {
            type : constants.KPIS_COLORS[val],
            title : constants.KPIS_NAMES[val],
            number : loadedKPIs[val],
            label : constants.KPIS_LABELS[val]
          }
          this.wedgitList.push(singleKPI);
        });
      } else {

      }
    })
  };

  loadFiltersCount(){
    let filtersCountURL = `${urls.GET_FILTERS_COUNT}/${this.loggedInUser.userSESA}`;
    this.httpService.Get(filtersCountURL , {} ).subscribe((filters : responseModel) => {
      if(!filters.error){
        this.offersCount = filters.data;
      }
    })
  };

  alterKPILabel(role){
    this.currentView = role == constants.ROLE_NAMES.TENDERING_MANAGER ? constants.ROLE_NAMES.TENDERING_ENGINEER : constants.ROLE_NAMES.TENDERING_MANAGER;
    this.alterKPIActionLabel = role == constants.ROLE_NAMES.TENDERING_MANAGER ? this.managerLabel : this.employeeLabel;

  };

  applySearchFilter(){
    let searchFilters = this.searchFilter;
    searchFilters.from = searchFilters.from != "" && searchFilters.from != undefined ? new Date(searchFilters.from).toLocaleDateString() : "";
    searchFilters.to = searchFilters.to != ""  && searchFilters.to != undefined ? new Date(searchFilters.to).toLocaleDateString() : "";

    if(searchFilters.from == "")
      delete searchFilters.from
    if(searchFilters.to == "")
      delete searchFilters.to

    Object.keys(searchFilters).forEach((x) => {
      if(searchFilters[x] == false){
        delete searchFilters[x]
      };
    });
    
    this.router.navigate(['/users/dashboard'] , { queryParams : searchFilters});

  };

  clearSearchFilter(){
    this.router.navigate(['/users/dashboard']);
    this.searchFilter = {
      "CREATED" : false,
      "APPROVED" : false,
      "IN_APPROVAL_CYCLE" : false,
      "REJECTED" : false,
      "ASSIGNED" : false,
      "from" : "",
      "to" : ""
    }
  };

  toggle(checked , filterOption){
    this.searchFilter[filterOption] = checked;
  };

  // Abstracted for any action button !
  doAction(action , offerNumber){
    if(action.toLowerCase() == "assign"){
      this.assignOffer(offerNumber);
    } else if(action.toLowerCase() == "preview"){
      this.previewOffer(offerNumber);
    } else if(action.toLowerCase() == "configure"){
      this.configureOffer(offerNumber);
    } else if (action.toLowerCase() == "approve"){
      this.startApprovalCycle(offerNumber);
    }
  };

  assignOffer(offerNumber){
    this.router.navigate(['../offers/assign-offer' , offerNumber]);

  };

  previewOffer(offerNumber){
    this.router.navigate(['../offers/offer-details' , offerNumber]);

  };

  configureOffer(offerNumber){
    this.router.navigate(['../offers/configure-offer' , offerNumber , 'details']);
  };

  startApprovalCycle(offerNumber){
    this.router.navigate(['../offers/configure-offer' , offerNumber , 'details'] , { queryParams: { "approval": true } });
  };
  

  //#region Helpers Mehtods

  redierctToCreateOffer(){
    this.router.navigate(['/offers/create-offer']);
  };

  addStatusLabel(currentStatus){
    return constants.OFFER_STATUS_COLOR[currentStatus]["color"]
  };

  loadHighestPriorityAction(actions: any[]){
    let allActions = [];
    let highestAction = "";
    let highestPriority: any = 0;

    actions.forEach(action => {
      let priority = constants.ACTIONS_PRIORITY_MAP[action];
      if(priority > highestPriority){
        highestAction = action;
        highestPriority = constants.ACTIONS_PRIORITY_MAP[action];
      }
    });

    allActions.push(highestAction);
    let allEqualActions = Object.keys(constants.ACTIONS_PRIORITY_MAP).filter(function(key) {
      return constants.ACTIONS_PRIORITY_MAP[key] === highestPriority && key !== highestAction && actions.includes(key)
    });

    allActions = allActions.concat(allEqualActions);
    return allActions;
  };

  buildActionsBtns(actions: any[]){
    let actionBtnLabels = [];
    actions.forEach((action) => {
      let btnLabel = constants.DASHBOARD_ACTIONS[action];
      actionBtnLabels.push({ label: btnLabel , action: action });
    });

    return actionBtnLabels;
  };

  getStatusLabel(status , owner){
    if(owner == this.loggedInUser.userSESA){
      return constants.STATUS_LABEL["OWNER"][status];
    } else {
      return constants.STATUS_LABEL["NOT_OWNER"][status];
    }
  }

  pagingCarousel(offerPages: any[]){
    this.numberOfCarouselPages = Math.ceil(offerPages.length / 3);
    for (let index = 0; index < offerPages.length; index= index+3) {
      let sliceTo = index+3;
    }
  };

  //#endregion
}
