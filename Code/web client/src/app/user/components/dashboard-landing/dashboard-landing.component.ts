import { Component, OnInit } from '@angular/core';
import { authorizationService } from './../../../@core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: "dashboard-landing",
  template: "<p></p>",
})
export class DashboardLandingComponent implements OnInit {
  constructor(
    private authService: authorizationService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.redierctToDashboardByRole();
  }

  redierctToDashboardByRole() {
    if (this.authService.isTenderingEngineer() ||this.authService.isTenderingManager()) {
      this.router.navigateByUrl('/users/tendering')
    }
  }
}
