import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';

import * as userComponents from './components';

import { NBULAR_THEME } from './user.imports'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from './../@theme/theme.module';

import {
  localStorageService,
  httpService,
  headerInterceptor,
  authenticationService
} from '../@core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';


@NgModule({
  declarations: [
    userComponents.DashboardLandingComponent,
    userComponents.DashboardTenderingComponent,
    userComponents.DashboardSalesComponent,
    userComponents.DashboardPMComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    ...NBULAR_THEME,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ThemeModule
  ],
  providers: [
    localStorageService,
    httpService,
    authenticationService,
    { provide: HTTP_INTERCEPTORS , useClass : headerInterceptor , multi : true  },
  ]
})
export class UserModule { }
