import {
    NbAccordionModule,
    NbButtonModule,
    NbCardModule,
    NbListModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbTabsetModule, 
    NbUserModule,
    NbInputModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    NbCheckboxModule
} from '@nebular/theme';


export const NBULAR_THEME = [
    NbAccordionModule,
    NbButtonModule,
    NbCardModule,
    NbListModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbTabsetModule, 
    NbUserModule,
    NbInputModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    NbCheckboxModule
];