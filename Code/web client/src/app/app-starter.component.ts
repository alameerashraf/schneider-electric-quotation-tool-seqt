import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { urls , authorizationService, localStorageService, constants } from './@core';

@Component({
  selector: 'seqt-app-starter',
  template: `
    <seqt-one-column-layout>
      <nb-menu [items]="MENU_ITEMS"></nb-menu>
      <router-outlet></router-outlet>
    </seqt-one-column-layout>`,
})
export class AppStarterComponent implements OnInit {
  allMenuItems: any[] = [
    {
      title: 'My Dashbaord',
      icon: 'bar-chart-2-outline',
      link: '/users/dashboard',
      home: true,
      id : "DASHBOARD"
    },
    {
      title: 'Offers',
      group: true,
      id: "OFFERS_SPLITTER"
    },
    {
      title: 'Create new offer',
      icon: 'file-add',
      link: '/offers/create-offer',
      id: 'CREATE_NEW_OFFER'
    },
    {
      title: 'Manage & Control',
      group: true,
      id: "ADMIN_SPLITTER"
    },
    {
      title: 'Admin',
      icon: 'award-outline',
      link: '/admin/control-panel',
      id: 'ADMIN'
    }
  ];
  MENU_ITEMS: NbMenuItem[] = [];

  constructor(private authorizationService: authorizationService , 
      private storageService: localStorageService) { }

  ngOnInit() {

    // Laod the menu items dynamically
    this.initMenuItems();

  }

  initMenuItems() {
    let allLinks = [];
    allLinks = allLinks.concat(constants.MENU_ITEMS_PER_ROLE.Dashboard);

    if (this.authorizationService.isTenderingManager()) {
      allLinks = allLinks.concat(constants.MENU_ITEMS_PER_ROLE.TenderingManager);
    }

    if(this.authorizationService.isSuperAdmin()){
      allLinks = allLinks.concat(constants.MENU_ITEMS_PER_ROLE.SuperAdmin);
    }


    allLinks.forEach((link) => {
      let linkItem = this.allMenuItems.find(x => x.id == link);
      this.MENU_ITEMS.push(linkItem);
    });
  }


}


