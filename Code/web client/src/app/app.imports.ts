import { AppComponent } from './app.component';
import { AppLandingComponent } from './app-landing.component';
import { AppStarterComponent } from './app-starter.component';


import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';

import {
    NbChatModule,
    NbDatepickerModule,
    NbDialogModule,
    NbMenuModule,
    NbSidebarModule,
    NbToastrModule,
    NbWindowModule,
  } from '@nebular/theme';


export const COMPONENTS = {
    AppComponent,
    AppLandingComponent,
    AppStarterComponent
};

export const NBULAR_THEME = {
    NbChatModule,
    NbDatepickerModule,
    NbDialogModule,
    NbMenuModule,
    NbSidebarModule,
    NbToastrModule,
    NbWindowModule,
    CoreModule,
    ThemeModule
};


