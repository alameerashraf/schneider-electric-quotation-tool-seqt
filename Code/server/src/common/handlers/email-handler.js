import emailConstatnts from '../constants/email-constatnts';


function prepareEmail(topic , paramters , additionalParams){
    let loadedEmailParams = Object.assign({}, emailConstatnts.emailBody[topic]);

    if(topic == "offerSubmitted"){
        loadedEmailParams.message = loadedEmailParams.message.replace("#########" , paramters.offerNumber);
        loadedEmailParams.btnLink = loadedEmailParams.btnLink.replace("#########" , paramters.offerNumber);

        loadedEmailParams.btnLink = `${process.env.HOST}${loadedEmailParams.btnLink}`;
        

        return {...loadedEmailParams , ...additionalParams};
    }
};


const actions = {
    prepareEmail
};

export default actions;