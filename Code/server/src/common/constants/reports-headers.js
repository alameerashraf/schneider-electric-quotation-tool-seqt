const detailedSummaryHeaders = {
    itemNo: {type: 'number', columnName: 'Item No'},
    family : {type: 'string', columnName: 'Family'},
    product : {type: 'string', columnName: 'Product'},
    description : {type: 'string', columnName: 'Description'},
    quantity : {type: 'number', columnName: 'QTY'},
    unitCostExcludingExpenses : {type: 'number', columnName: 'Unit Cost Excluding EXP'},
    totalCostExcludingExpenses : {type: 'number', columnName: 'Total Cost Excluding EXP'},
    totalExpenses :{type: 'number', columnName: 'Expenses'},
    unitCostIncludingExpenses: {type: 'number', columnName: 'Unit Cost Including EXP'},
    totalCostIncludingExpenses : {type: 'number', columnName: 'Total Cost Including EXP'},
    Upstream_Percentage : {type: 'number', columnName: 'Upstream %'},
    Upstream_amount : {type: 'number', columnName: 'Upstream'},
    CCOGM_Percentage : {type: 'number', columnName: 'Statory Margin %'},
    sellingLocalMargin : {type: 'number', columnName: 'Selling CCO %'},
    CCOGM_Percentage_Threshold : {type: 'number', columnName: 'Threashold CCO %'},
    unitSP1 : {type: 'number', columnName: 'Unit SP EGP'},
    totalSP1: {type: 'number', columnName: 'Total SP EGP'},
}

const generalExpensesSummary = {
    name: {type: 'string', columnName: 'Item'},
    quantity: {type: 'number', columnName: 'QTY'},
    value:  {type: 'number', columnName: 'value'},
    netValue: {type: 'number', columnName: 'Net Value'}
}

const familySummary = {
    family: {type: 'string', columnName: 'Family'},
    salesFamily: {type: 'string', columnName: 'Sales Family'},
    totalCostExcludingExpenses: {type: 'number', columnName: 'Total Cost Excluding Expenses (EGP)'},
    totalCostIncludingExpenses: {type: 'number', columnName: 'Total Cost Including Expenses (EGP)'},
    totalSP:  {type: 'number', columnName: 'Total SP (EGP)'},
    localMargin: {type: 'number', columnName: 'Local Margin'},
    SP_CCO_GM: {type: 'number', columnName: 'SP CCO GM %'},
    thresholdCCO_GM: {type: 'number', columnName: 'Threshold CCO GM %'},
}

export default {
    detailedSummaryHeaders,
    generalExpensesSummary,
    familySummary,
}