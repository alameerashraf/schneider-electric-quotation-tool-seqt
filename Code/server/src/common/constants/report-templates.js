
const allExpensesReportPDF = ({reportName}, doc) => {
    generateHeader(reportName, doc)
    generateFooter(doc)
    
}

const generateHeader = (reportName, doc) => {
    const date = new Date()
    const today = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear()
    
    doc
      .image("logo.jpg", 50, 45, { width: 100 })
      .fillColor('green')
      .fontSize(10)
      .text(today, 200, 65, { align: "right" })
      .font('Times-Bold')
      .fontSize(16)
      .text(reportName, 100, 120, { align: "center" })
      .moveDown()
      .font('Times-Roman')
}

const generateFooter = (doc) => {
    doc
      .fillColor('black')
      .fontSize(10)
      .text(
        "this is a footer till we agree upon a footer.",
        50,
        700,
        { align: "center", width: 500 }
      );
  }

export default {
    allExpensesReportPDF
}