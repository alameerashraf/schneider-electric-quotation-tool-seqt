export default {
    "All Expenses Report": "allExpensesReport",
    "Detailed Summary Report": "detailedSummaryReport",
    "General Expenses Summary Report": "generalExpensesSummary",
    "Family Summary Report": "familySummary",
}