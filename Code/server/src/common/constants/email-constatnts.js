

export default { 
    emailBody: {
        offerSubmitted : {
            "subject" : "An offer has been submitted",
            "title" : "Tendering Engineer has submit an offer.",
            "message" : "One of your tendering engineers has submit an offer with number: ######### , Review the offer and approve/reject.",
            "btnMessage" : "Preview the offer and approve/reject",
            "btnLink" : "/offers/configure-offer/#########/details",
            "optionalMessage" : ""
        },
        offerWaitingApproval : {
            "subject" : "Offer is pending approval.",
            "title" : "Offer is waiting your approval.",
            "message" : "Offer with number: ######### is waiting your approval, Review the offer and approve/reject.",
            "btnMessage" : "Preview the offer and approve/reject",
            "btnLink" : "/offers/configure-offer/#########/details",
            "optionalMessage" : ""
        },
        offerRejected : {
            "subject" : "Offer has been rejected.",
            "title" : "Offer has been rejected.",
            "message" : "Offer with number: ######### has been rejected.",
            "btnMessage" : "My Dashboard",
            "btnLink" : "/users/dashboard",
            "optionalMessage" : ""
        }
    }
};

