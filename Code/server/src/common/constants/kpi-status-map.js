export default { 
    manager : {
        CREATED : "DRAFT",
        ASSIGNED : "IN_PROGRESS",
        CONFIGURED : "IN_PROGRESS",
        IN_APPROVAL_CYCLE : "PENDING_APPROVAL",
        APPROVED : "APPROVED",   
        CANCELED : "CANCELED",   
        WON : "WON",
    },
    employee : {
        ASSIGNED : "IN_PROGRESS",
        CONFIGURED : "IN_PROGRESS",
        SUBMITTED : "SUBMITTED",
        IN_APPROVAL_CYCLE : "WAITING_FOR_APPROVAL",
        APPROVED : "APPROVED",
        WON : "WON",
        CANCELED: "CANCELED"
    }
}