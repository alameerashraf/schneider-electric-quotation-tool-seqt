export default { 
    "ASSIGNED": {
        "OWNER" : ["PREVIEW"],
        "NOT_OWNER" : ["CONFIGURE"]
    },
    "CREATED" : {
        "OWNER" : ["ASSIGN"]
    },
    "SUBMITTED" : {
        "OWNER" : ["APPROVE"],
        "NOT_OWNER" : ["CONFIGURE" , "RECALL"]
    },
    "IN_PROGRESS" : {
        "OWNER" : ["PREVIEW"],
        "NOT_OWNER" : ["CONFIGURE"]
    },
    "IN_APPROVAL_CYCLE" : {
        "OWNER" : ["APPROVE"],
        "NOT_OWNER" : ["CONFIGURE" , "RECALL"]
    },
    "APPROVED" : {
        "OWNER" : ["PREVIEW"],
        "NOT_OWNER" : ["PREVIEW"]
    },
    "REJECTED" : { 
        "OWNER" : ["PREVIEW"],
        "NOT_OWNER" : ["PREVIEW"]
    }
}