import Service from "../../services/Service";

class bFOService extends Service {
    constructor(model) {
        super(model)
        this.model = model;
        this.getLatest = this.getLatest.bind(this);
    }

    /**
     * Get the latest record in the current collection.
     */
    async getLatest() {
        try {
            let item = await this.model
                .find()
                .limit(1)
                .sort({
                    $natural: -1
                });   

            return {
                error: false,
                statusCode: 200,
                data: item,
            }
        } catch (error) {
            return {
                error: exception,
                statusCode: 500,
                exception
            }
        }
    };

}

export default bFOService;