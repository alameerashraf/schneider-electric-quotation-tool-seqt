import mongoose, { Schema } from 'mongoose';

class bFOTokens {
    intiSchema(){
        var schema = new Schema({
            gapimp : {
                token : {type : String},
                issued_at : { type : Number },
                expires_at : { type : Number }
            } , 
            bfo : {
                token : {type : String},
                issued_at : { type : Number },
                expires_date : { type: String }
            }
        });

        schema.pre('save' , function(next){
            let record = this;
            let issued = record.gapimp.issued_at;
            let issuedDate = new Date(issued);
            record.gapimp.expires_at = new Date(issuedDate.setSeconds(issuedDate.getSeconds() + 3599)).getTime()
            next();
        });

        return mongoose.model("bFOTokens" , schema);
    };  

    constructModel(){
        return this.intiSchema();
    };
};

export default bFOTokens;