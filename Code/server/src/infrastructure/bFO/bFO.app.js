import bFOTokensModel from './models/bFOTokensModel';
import bFOService from './bFOService';

// Use Cases..!
import obtainAccessTokens from './obtain-access-tokens/obatin-access-tokens';
import loadDataFromBFO from './load-data/load-data-from-bFO';

const tokenService = new bFOService(new bFOTokensModel().constructModel());


export default ()=> {
    return Object.freeze({
        obtainAccessTokens: new obtainAccessTokens(tokenService),
        loadData : new loadDataFromBFO(tokenService),
    })
};
