import loggers from '../../../loaders/logger';
import request from 'request';

var fs = require('fs');
var path = require('path');


async function loadTokens() {
    try {
        let latestToken = await this.service.getLatest();
        return {
            data: latestToken.data[0],
            error: false
        };
    } catch (error) {
        loggers.error(`${error}   ## LOCATION:   loadTokens`);
        return {
            data: null,
            error: true,
            exception: error
        };
    }
};

/**
 * Different between the dates is 3 minutes, then it is valid.
 */
function validateGAPIMPToken(GAPIMP){
    let expiryDate = new Date(GAPIMP.expires_at);
    let currentDate = new Date();

    var dateCheck = expiryDate.getDate() == currentDate.getDate();
    var diff = expiryDate - currentDate;
    var minutes = Math.floor((diff/1000)/60);


    if(minutes > 3 && dateCheck){
        return true;
    } else{
        return false;
    }
};


function validateXBFOToken(XBFO){
    return true;
};


async function loadDataFromBFO_fake(type, id) {
    let opportunityDataFile = path.join(__dirname, '../../../common/data/opportunity-data-from-bfo.json');
    return new Promise((resolve, reject) => {
        fs.readFile(opportunityDataFile, 'utf8' , (err, data) => {
            if (!err) {
                let opportunityData = JSON.parse(data);
                if(opportunityData.seReference == id){
                    let actualData = {
                        data: JSON.parse(data),
                        error: false
                    };
                    resolve(actualData);
                } else {
                    resolve({
                        error: true,
                        exception: "SeRefrence Doesn't Exist."
                    })
                }
            } else {
                resolve({
                    error: true,
                    exception: err
                })
            }
        })
    });
};

function loadbFOUrl(){
    return  process.env.bfoURL;
};

function loadDataFromBFO(latestAccessTokens , seReference){
    let bFOUrl = loadbFOUrl()+seReference;
    let access_token = latestAccessTokens.data.gapimp.token;
    let bearerAuth = 'Bearer ' + access_token;

    let requestOptions = {
        url: bFOUrl,
        headers: {
            'Authorization' : bearerAuth,
            'Accept' : 'application/json'
        },
        'hostname' : process.env.PROXY,
        'port' : process.env.PROXY_PORT 
    };

    return new Promise((resolve , reject) =>{
        try{
            request.get({
                headers: requestOptions.headers,
                url: requestOptions.url,
                hostname: requestOptions.hostname,
                port: requestOptions.port 
            }, (error , response , body)=> {

                if(error){
                    resolve({
                        data: null,
                        error: true,
                        exception: error
                    });
                } else {
                    resolve({
                        data: JSON.parse(body),
                        error: false,
                    });
                }
            });
        } 
        catch(exception) {
            loggers.error(`${exception}    ## LOCATION:   obtainAPIEGToken`);
            reject(exception);
        }
    });

};


export default {
    loadTokens,
    validateGAPIMPToken,
    validateXBFOToken,
    loadDataFromBFO,
    loadDataFromBFO_fake
}


