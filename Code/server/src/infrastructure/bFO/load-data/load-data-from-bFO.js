import methods from "./methods";
import errors from '../../../common/constants/error-codes';
import loggers from '../../../loaders/logger';


class loadDataFromBFO {
    constructor(service) {
        this.service = service;
        methods.loadTokens = methods.loadTokens.bind(this);

    }

    async execute(requestedResource, seRefrence) {

        // let data = await methods.loadDataFromBFO(latestAccessTokens , seRefrence);
        // return data;
        let latestAccessTokens = await methods.loadTokens();

        try {
            // Latest token loaded from DB.
            if (latestAccessTokens.error == false && latestAccessTokens.data != undefined) {
                let isGapimpValid = methods.validateGAPIMPToken(latestAccessTokens.data.gapimp);
                let isBfoValid = methods.validateXBFOToken(latestAccessTokens.data.bfo);

                // Token is still valid.
                if (isGapimpValid && isBfoValid) {
                    let bfoData = await methods.loadDataFromBFO(latestAccessTokens , seRefrence);
                    // Data loaded correctly from bFO API.
                    
                    if (bfoData.error == false) {
                        return bfoData;
                    } else {
                        return {
                            error: true,
                            code: errors.ERROR_LOADING_DATA_FROM_EXTERNAL_SOURCE,
                            message: bfoData.exception || "Couldn't load data from bFO"
                        };
                    }
                } else {
                    return {
                        error: true,
                        code: errors.INVALID_ACCESS_TOKEN,
                        message: "Access token aren't valid."
                    }
                }
            } else {
                return {
                    error: true,
                    code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                    message: latestAccessTokens.error || "Couldn't load G_API_M_P and xBFO tokens from DB."
                };
            }
        } catch (error) {
            loggers.error(`${error}   ## LOCATION:   load-data-from-bFO.execute`);
            return {
                error: true,
                code: 500,
                message: error.toString()
            };
        }
    };
}

export default loadDataFromBFO;