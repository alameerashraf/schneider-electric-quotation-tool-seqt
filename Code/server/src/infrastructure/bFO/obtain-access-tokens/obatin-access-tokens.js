import methods from "./methods";
import loggers from "../../../loaders/logger";
import bFOTokens from "../models/bFOTokensModel";
import errors from '../../../common/constants/error-codes';

class obtainAccessTokens {
  constructor(service) {
    this.service = service;
    methods.saveTokens = methods.saveTokens.bind(this);
  }
  
  async execute() {

    let GAPIMPToken = (await methods.obtainGAPIMPToken()) || null;

    if (!GAPIMPToken.error) {
      let tokens = {
        gapimp: {
          token: GAPIMPToken.data.access_token,
          issued_at: GAPIMPToken.data.issued_at,
          expires_in: GAPIMPToken.data.expires_in
        },
        bfo: {}
      };

      let isTokenSaved = await methods.saveTokens(tokens);
      if (isTokenSaved.error) {
        return {
          error: true,
          code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
          message: isTokenSaved.exception,
        };
      } else {
        return {
          error: false
        }
      }
    } else {
      return {
        error: true,
        code: errors,
        message: GAPIMPToken.error + bFOTokens.error || "Couldn't obtain tokens successfully",
      };
    }

  }
}

export default obtainAccessTokens;
