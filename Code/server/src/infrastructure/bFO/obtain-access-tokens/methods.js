import request from 'request';
import loggers from '../../../loaders/logger';

/**
 * Load creds from env variables.
 */
function loadClientCredentials() {
    let client_id = process.env.GAPIMP_client_id || null; 
    let client_secret = process.env.GAPIMP_client_secret || null;
    return {
        clientID: client_id,
        clientSecret: client_secret
    }
};

function loadGAPIMPUrl(){
    return process.env.GAPIMP_URL;
};

function obtainGAPIMPToken(){
    let { clientID , clientSecret } = loadClientCredentials();
    let hostURLForGAPIMP = loadGAPIMPUrl();

    let basicAuth = 'Basic ' + new Buffer.from(clientID + ':' + clientSecret).toString('base64'); 

    let requestOptions = {
        url: hostURLForGAPIMP,
        data: { 
            "grant_type" : "client_credentials"
        },
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            "Authorization" : basicAuth
        },
        'hostname' : process.env.PROXY,
        'port' : process.env.PROXY_PORT 
    };

    return new Promise((resolve , reject) =>{
        try{
            request.post({
                headers: requestOptions.headers,
                url: requestOptions.url,
                form: requestOptions.data,
                hostname: requestOptions.hostname,
                port: requestOptions.port 
            }, (error , response , body)=> {
                if(error){
                    resolve({
                        data: null,
                        error: true,
                        exception: error
                    });
                    loggers.error(`${error}   ## LOCATION:   obtainAPIEGToken.Promise`);
                } else {
                    resolve({
                        data: JSON.parse(body),
                        error: false,
                    });
                }
            });
        } 
        catch(exception) {
            loggers.error(`${exception}    ## LOCATION:   obtainAPIEGToken`);
            reject(exception);
        }
    });
};


async function saveTokens(tokens){
    try{
        let result = await this.service.create(tokens);
        return {
            data: result.data,
            error: false
        };
    } 
    catch (error) {
        loggers.error(`${error}   ## LOCATION:   saveTokens`);
        return {
            error: true,
            exception: error
        };
    }
};      


export default {
    obtainGAPIMPToken,
    saveTokens
};