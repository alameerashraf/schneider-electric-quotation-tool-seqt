import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import path from 'path'
import fs from 'fs'
import xlsx from 'xlsx'
import { lookup } from 'dns';
import { compile } from 'handlebars';
import e2eAttributes from '../../../common/constants/e2e-attributes'

class getCostingSummary {

   async execute(input) {
      try {

         let offerNumber = input.replace("*", "$");

         let filePath = path.join(__dirname + '../../../../../uploads/Cost-files/' + offerNumber + '/');
         let costFiles = fs.readdirSync(filePath);

         let costingSummaryInstance = {
            "offerNumber": input,
            "sheets": []
         };

         costFiles.forEach(file => {
            let costFilePath = path.join(__dirname + '../../../../../uploads/Cost-files/' + offerNumber + '/' + file);
            let fileWorkBook = xlsx.readFile(costFilePath);
            let sheetName = fileWorkBook.SheetNames[0];
            let fileSheet = fileWorkBook.Sheets[sheetName];

            let sheetInstance = {
               "sheetName": sheetName,
               "attributes": []
            }

            let e2eLookupArguments = e2eAttributes.e2eLookupArguments;
            e2eLookupArguments.forEach((argument) => {
               let attributeInstance;
               let argumentName = argument.arg;
               let argumentValue = 0;

               if (!sheetName.includes("AUT")) {

                  argument.eas.forEach((cellPosition) => {
                     if (fileSheet[cellPosition.v] != undefined) {
                        argumentValue += fileSheet[cellPosition.v].v;
                     }
                  });
               }
               else {
                  argument.aut.forEach((cellPosition) => {
                     if (fileSheet[cellPosition.v] != undefined) {
                        argumentValue += fileSheet[cellPosition.v].v;
                     }
                  });
               }

               attributeInstance = {
                  "key": argumentName,
                  "value": argumentValue
               }
               sheetInstance.attributes.push(attributeInstance);
            });

            costingSummaryInstance.sheets.push(sheetInstance);
         });

         if (costingSummaryInstance.error) {
            return {
               error: true,
               message: costingSummaryInstance.message
            }
         } else {
            return costingSummaryInstance;
         }
      } catch (exception) {
         return {
            error: true,
            message: exception
         }
      }
   }
};

export default getCostingSummary;