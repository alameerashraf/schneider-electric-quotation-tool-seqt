import getCostingSummary from './get-costing-summary/getCostingSummary'
export default () => {
    return Object.freeze({
        getCostingSummary: new getCostingSummary()
    })
};
