import errors from '../../../common/constants/error-codes';

class getOfferByOfferNumber {
    constructor(offerService) {
        this.service = offerService
    };

    async execute(offerNumber) {
        try {
            let result = await this.service.read({
                offerNumber: offerNumber
            });


            if (result.error) {
                return {
                    error: true,
                    code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: exception
            }
        }
    }
};

export default  getOfferByOfferNumber;