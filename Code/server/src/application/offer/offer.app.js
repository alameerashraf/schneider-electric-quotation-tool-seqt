import offerService from '../../services/offer.service';
import offer from '../../models/offer/offer';

import userService from '../../services/user.service';
import user from '../../models/user/user';

// Use cases !
import createOffer from './create-offer/create-offer';
import assignOffer from './assign-offer/assign-offer';
import getOfferByOfferNumber from './get-offer-by-offer-number/get-offer-by-offer-number';
import defineOfferActions from './define-offer-actions/define-offer-actions';
import getOffersCountPerFilter from './get-offers-count-per-filter/get-offers-count-per-filter';
import getUserOffers from './get-user-offers/get-user-offers';
import getLatestCreatedOfferNumber from './get-latest-created-offer-number/get-latest-created-offer-number';
import updateOffer from './update-offer/update-offer';
import addOfferItems from './add-offer-items/add-offer-items';
import getOfferItems from './get-offer-items/get-offer-items';
import deleteItemFromOffer from './delete-item-from-offer/deleteItemFromOffer';
import submitOffer from './submit-offer/submit-offer'
import startingApprovalCycle from './starting-approval-cycle/starting-approval-cycle'
import approveOffer from './approve-offer/approve-offer'
import rejectOffer from './reject-offer/reject-offer'
import cloneItemsFromOffer from './clone-items-from-offer/cloneItemsFromOffer';
import addExpenseToOffer from './add-expenses-to-offer/addExpenseToOffer';
import recallOffer from './recall-offer/recallOffer'


const offerServiceInstance = new offerService(new offer().constructModel());
const userServiceInstance = new userService(new user().constructModel());

export default () => {
    return Object.freeze({
        createOffer: new createOffer(offerServiceInstance),
        assignOffer: new assignOffer(offerServiceInstance),
        getOfferByOfferNumber: new getOfferByOfferNumber(offerServiceInstance),
        defineOfferActions: new defineOfferActions(),
        getOffersCountPerFilter: new getOffersCountPerFilter(offerServiceInstance),
        getUserOffers: new getUserOffers(offerServiceInstance),
        getLatestCreatedOfferNumber: new getLatestCreatedOfferNumber(offerServiceInstance),
        updateOffer: new updateOffer(offerServiceInstance),
        addOfferItems: new addOfferItems(offerServiceInstance),
        getOfferItems: new getOfferItems(offerServiceInstance),
        deleteItemFromOffer: new deleteItemFromOffer(offerServiceInstance),
        submitOffer: new submitOffer(offerServiceInstance),
        startingApprovalCycle: new startingApprovalCycle(offerServiceInstance),
        approveOffer: new approveOffer(offerServiceInstance),
        rejectOffer: new rejectOffer(offerServiceInstance),
        cloneItemsFromOffer: new cloneItemsFromOffer(offerServiceInstance),
        addExpenseToOffer: new addExpenseToOffer(offerServiceInstance),
        recallOffer: new recallOffer(offerServiceInstance, userServiceInstance)
    });   
};



