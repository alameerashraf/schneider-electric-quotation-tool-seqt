class updateOffer{
    constructor(offerService){
        this.service = offerService;
    }

    async execute(offerNumber , updatedProperties){
        let query = { offerNumber: offerNumber };
        let result = await this.service.updateByQuery(query , updatedProperties);

        if (result.error) {
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE ,
                message: result.message
            }
        } else {
            return {
                error: false,
                data: result.data
            }
        }
    }
}

export default updateOffer;