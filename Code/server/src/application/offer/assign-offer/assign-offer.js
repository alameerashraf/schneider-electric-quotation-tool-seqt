import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class assignOffer{
    constructor(offerService){
        this.service = offerService;
    };

    async execute(offer , assignmentData){
        // reset all timeline isRecent ==> false 
        // add new move to the time line 
        if(!offer.error){
            offer.offerTimeline.forEach(element => {
                element.isLatest = false;
            });

            offer.currentStatus = "ASSIGNED";
            offer.assignedTo = assignmentData.owner;
            offer.offerTimeline.push(assignmentData);

            let result = await this.service.updateByQuery(
                { offerNumber: offer.offerNumber },
                offer
            );

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE ,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } else {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: offer.message || ""
            }
        }


        
    };
};


export default assignOffer;