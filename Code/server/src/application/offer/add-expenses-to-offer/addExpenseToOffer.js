class addExpenseToOffer{
    constructor(offerService){
        this.service = offerService;
    }

    async execute(offer , generalExpenses){
        
        let unSavedGeneralExpenses = generalExpenses.filter((expense) => {
            return expense.IS_SAVED == false;
        });
    
        try{
            let result = await this.service.updateByQuery({
                offerNumber: offer.offerNumber
            } , {
                $push: {
                    "expenses": unSavedGeneralExpenses
                }   
            });

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                }
            }
            
        } catch(exception){
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};

export default addExpenseToOffer;