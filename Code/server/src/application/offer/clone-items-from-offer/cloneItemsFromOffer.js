import errors from "../../../common/constants/error-codes";

class cloneItemsFromOffer {
  constructor(offerService) {
    this.service = offerService;
  }

  async execute(offer, itemIds) {
    try {
      let offerItems = offer.items;
      let itemsToBeCloned = [];
      let requestedItems = [];
      let fieldsToClone = [
        "family",
        "product",
        "description",
        "quantity",
        "unitCostExcludingExpenses",
        "CCOGM_Percentage_Threshold",
        "IG_OG",
        "Upstream_Percentage",
        "Upstream_amount",
        "upstreamSFC_Percentage",
        "fOSFC_Percentage",
      ];

      itemIds.forEach((itemId) => {
        let item = offerItems.find((x) => x.itemNo == itemId);
        itemsToBeCloned.push(item);
      });


      itemsToBeCloned.forEach((item) => {
        let itemObject = item.toObject(item);
        Object.keys(itemObject).forEach((k) => {
          if (!fieldsToClone.includes(k)) {
            delete itemObject[k];
          }
        });

        requestedItems.push(itemObject);
      });

      return {
        error: false,
        data: requestedItems,
      };
    } catch (exception) {
      return {
        error: true,
        code: errors["PROCESS_TERMINATED"],
        message: exception,
      };
    }
  }
}

export default cloneItemsFromOffer;
