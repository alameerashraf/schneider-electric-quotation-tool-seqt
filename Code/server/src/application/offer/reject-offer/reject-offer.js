import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import offerStatus from '../../../common/constants/offer-status';

class approveOffer{
    constructor(offerService){
        this.service = offerService;
    };

    async execute(offer){
        if(!offer.error){
            //making sure it wasn't rejected before
            for (const move of offer.offerTimeline){
                if(move.newStatus == offerStatus.APPROVED || move.newStatus == offerStatus.REJECTED) 
                    return {
                        error: false,
                        data: null
                    }
            }
            const oldTimeline = offer.offerTimeline.find(element => element.isLatest)

            
            const newTimeline = {
                "changedAt": Date.now(),
                "changedBy": oldTimeline.owner,
                "owner": offer.assignedTo,
                "isLatest": true,
                "newStatus": offerStatus.REJECTED,
                "oldStatus": oldTimeline.newStatus,
            }
            // reset all timeline isRecent ==> false 
            // add new move to the timeline 
            offer.offerTimeline.forEach(element => {
                element.isLatest = false;
            });

            offer.currentStatus = offerStatus.CANCELED;
            offer.offerTimeline.push(newTimeline);
            const result = await this.service.updateByQuery(
                { offerNumber: offer.offerNumber },
                offer
            );

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE ,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: newTimeline
                }
            }
        } else {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: offer.message || ""
            }
        }
    };
};


export default approveOffer;