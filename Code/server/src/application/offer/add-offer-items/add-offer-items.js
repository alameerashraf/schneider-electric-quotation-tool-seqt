import errors from '../../../common/constants/error-codes';
import methods from './methods';
class addOfferItems {
    constructor(offerService){
        this.service = offerService;
    };
    
    async execute(offer, itemsList) {
        let offerNumber = offer.offerNumber;
        let savedItems = offer.items;

        itemsList.forEach((item) => {

            let isItemFound = methods.checkItemExistance(savedItems , item);
            if(isItemFound !== null){
                // replace item from current index with new item!
                savedItems.splice(isItemFound , 1 , item);
            } else {
                // push item
                savedItems.push(item)
            }
        });

        try {
            let result = await this.service.updateByQuery({
                offerNumber: offerNumber
            }, {
                $set: {
                    "items": savedItems
                }
            });

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                let savedIds = [];
                itemsList.forEach(element => {
                    savedIds.push(element.itemNo);
                });

                return {
                    error: false,
                    data: savedIds
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    };
};

export default addOfferItems;