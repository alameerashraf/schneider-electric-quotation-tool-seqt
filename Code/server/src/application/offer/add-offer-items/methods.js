
function checkItemExistance(offerSavedItems , offerItem){
    // if offer has no items 
    if(offerSavedItems.length == 0){
        return null;
    } else {
        let item = offerSavedItems.find(x => x.itemNo == offerItem.itemNo);
        return item == undefined ? null : offerSavedItems.indexOf(item);
    }
};




const methods = {
    checkItemExistance,
}
export default methods;