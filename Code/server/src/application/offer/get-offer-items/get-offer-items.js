import errors from '../../../common/constants/error-codes';

class getOfferItems {
    constructor(offerService) {
        this.service = offerService;
    }

    async execute(offerNumber) {
        try {
            let result = await this.service.read({
                offerNumber: offerNumber
            });

            if (result.error ) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else if (result.data == null){
                return {
                    error: true,
                    code: errors.DATA_NOT_FOUND,
                    message: "DATA_NOT_FOUND"
                }
            } else {
                return {
                    error: false,
                    data: result.data.items
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};


export default getOfferItems;