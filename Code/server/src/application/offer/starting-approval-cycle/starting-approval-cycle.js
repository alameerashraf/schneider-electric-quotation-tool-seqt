import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import offerStatus from '../../../common/constants/offer-status';
import user from '../../user/user.app'

let userApps = user()

class startingApprovalCycle{
    constructor(offerService){
        this.service = offerService;
    };

    async execute(offer){
        if(!offer.error){
            //making sure approval cycle wasn't started before
            for (const move of offer.offerTimeline){
                if(move.newStatus == offerStatus.IN_APPROVAL_CYCLE) 
                    return {
                        error: false,
                        data: null
                    }
            }
            //constructing the new offer timeline
            // change to be with is latest 
            const oldTimeline = offer.offerTimeline.find(x => x.isLatest)

            const newTimeline = {
                "changedAt": Date.now(),
                "changedBy": oldTimeline.owner,
                "owner": oldTimeline.owner,
                "isLatest": true,
                "newStatus": offerStatus.IN_APPROVAL_CYCLE,
                "oldStatus": oldTimeline.newStatus,
            }
            // reset all timeline isRecent ==> false 
            // add new move to the timeline 
            offer.offerTimeline.forEach(element => {
                element.isLatest = false;
            });

            offer.currentStatus = offerStatus.IN_APPROVAL_CYCLE;
            offer.offerTimeline.push(newTimeline);

            const result = await this.service.updateByQuery(
                { offerNumber: offer.offerNumber },
                offer
            );

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE ,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } else {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: offer.message || ""
            }
        }
    };
};


export default startingApprovalCycle;