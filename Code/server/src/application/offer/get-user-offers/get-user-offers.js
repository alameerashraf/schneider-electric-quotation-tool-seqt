import methods from './methods';

class getUserOffers {
    constructor(offerServie){
        this.service = offerServie;
        methods.getOffersBasedOnQuery = methods.getOffersBasedOnQuery.bind(this);
    }

    async execute(user , query){
        let userOffers = [];
        let offersCount = 0;

        if(query.length == 0){ 
            userOffers = user.offers;
            offersCount = userOffers.length;
        } else if(query.length > 0 ){
            let arrayOfOffersIds = methods.getAllOffers(user.offers);
            let result = await methods.getOffersBasedOnQuery(arrayOfOffersIds , query , user.sesa);
            userOffers = result.data;
            offersCount = userOffers.length;
        }

        return {
            error: false,
            data: {
                offers: userOffers,
                count: offersCount
            }
        };

    }
};


export default getUserOffers;