function getAllOffers(userlistOfOffers){
    let arrayOfOffers =[];
    for (let index = 0; index < userlistOfOffers.length; index++) {
        const element = userlistOfOffers[index];
        arrayOfOffers.push(element._id)
    };

    return arrayOfOffers;
};



async function getOffersBasedOnQuery(userOfferIds , query , sesa){
    let dataQuery = {};
    let arrayOfConditions = [];
    for (let index = 0; index < query.length; index++) {
        const element = query[index];
        if(element != "ASSIGNED"){
            arrayOfConditions.push(
                { "offerTimeline" : { "$elemMatch" : { "isLatest" : true , "newStatus" : element } } }
            )
        }
    }
    
    if(query.includes("ASSIGNED")){
        arrayOfConditions.push(
            { "offerTimeline" : { "$elemMatch" : { "isLatest" : true , "newStatus" : "ASSIGNED" , "owner" : sesa }}}
        );
    } 

    dataQuery = { "$or" : arrayOfConditions };

    try{
        let result = await this.service.readAll(
            {
                $and : [
                    { _id : { $in : userOfferIds } },
                    dataQuery
                ]
            },
            {
                'offerNumber': 1,
                'seRefrence': 1,
                'currentStatus': 1,
                'createdBy': 1,
                'createdAt': 1,
                'currencyCode': 1,
                'opportunityName' : 1,
                'ownerName': 1, 
                'assignedTo': 1
            }
        );

        return{
            error : false,
            data : result.data
        }
    } catch(exception ){
        return {
            error: true,
            message: exception
        }
    }
}


export default { 
    getAllOffers,
    getOffersBasedOnQuery
}