async function getAllOffers(userlistOfOffers){
    let arrayOfOffers =[];
    for (let index = 0; index < userlistOfOffers.length; index++) {
        const element = userlistOfOffers[index];
        arrayOfOffers.push(element._id)
    };

    return arrayOfOffers;
};


async function getOffersCreatedByUser(userOfferIds , sesaSESA){
    try{
        let createdCount = await this.service.readAll(
            {
                $and : [
                    { _id : { $in : userOfferIds } },
                    { "createdBy" : sesaSESA },
                    { "offerTimeline" : {  "$elemMatch" : { "isLatest" : true  , "newStatus" : "CREATED"  }  }  }
                ]
            }
        );

        return{
            error : false,
            count : createdCount.data.length || 0
        }
    } catch(exception ){
        return {
            error: true,
            message: exception
        }
    }
};

async function getRejectedOfferCount(userOfferIds){
    try{
        let rejectedCount = await this.service.readAll(
            {
                $and : [
                    { _id : { $in : userOfferIds } },
                    { "offerTimeline" : {  "$elemMatch" : { "isLatest" : true  , "newStatus" : "REJECTED" }  }  }
                ]
            }
        );

        return{
            error : false,
            count : rejectedCount.data.length || 0
        }
    } catch(exception ){
        return {
            error: true,
            message: exception
        }
    }
};

async function getWaitingForApprovalOffers(userOfferIds){
    try{
        let waitingForApprovalCount = await this.service.readAll(
            {
                $and : [
                    { _id : { $in : userOfferIds } },
                    { "offerTimeline" : {  "$elemMatch" : { "isLatest" : true  , "newStatus" : "IN_APPROVAL_CYCLE" }  }  }
                ]
            }
        );

        return{
            error : false,
            count : waitingForApprovalCount.data.length || 0
        }
    } catch(exception ){
        return {
            error: true,
            message: exception
        }
    }
};

async function getApprovedOffers(userOfferIds){
    try{
        let waitingForApprovalCount = await this.service.readAll(
            {
                $and : [
                    { _id : { $in : userOfferIds } },
                    { "offerTimeline" : {  "$elemMatch" : { "isLatest" : true  , "newStatus" : "APPROVED" }  }  }
                ]
            }
        );

        return{
            error : false,
            count : waitingForApprovalCount.data.length || 0
        }
    } catch(exception ){
        return {
            error: true,
            message: exception
        }
    }
};

async function getOffersAssignedToUser(userlistOfOffers , sesaSESA){
    try{
        let assignedCount = await this.service.readAll(
            {
                $and : [
                    { _id : { $in : userlistOfOffers } },
                    { "offerTimeline" : {  "$elemMatch" : { "isLatest" : true  , "newStatus" : "ASSIGNED" , "owner" : sesaSESA }  }  }
                ]
            }
        );

        return{
            error : false,
            count : assignedCount.data.length || 0
        }
    } catch(exception ){
        return {
            error: true,
            message: exception
        }
    }
};


export default {
    getAllOffers,
    getApprovedOffers,
    getOffersCreatedByUser,
    getOffersAssignedToUser,
    getRejectedOfferCount,
    getWaitingForApprovalOffers
};
