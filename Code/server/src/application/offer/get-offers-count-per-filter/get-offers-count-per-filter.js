import methods from './methods';

class getOffersPerFilter {
    constructor(offerService){
        this.service = offerService;
        methods.getAllOffers = methods.getAllOffers.bind(this);
        methods.getApprovedOffers = methods.getApprovedOffers.bind(this);
        methods.getOffersCreatedByUser = methods.getOffersCreatedByUser.bind(this);
        methods.getOffersAssignedToUser = methods.getOffersAssignedToUser.bind(this);
        methods.getRejectedOfferCount = methods.getRejectedOfferCount.bind(this);
        methods.getWaitingForApprovalOffers = methods.getWaitingForApprovalOffers.bind(this);
    }

    async execute(listOfOffers , sesa){
        try {
            let arrayOfAllOffers = await methods.getAllOffers(listOfOffers);

            let rejectedCount = await methods.getRejectedOfferCount(arrayOfAllOffers);
            let waitingForApprovalCount = await methods.getWaitingForApprovalOffers(arrayOfAllOffers);
            let approvedCount = await methods.getApprovedOffers(arrayOfAllOffers);
            let createdByMeCount = await methods.getOffersCreatedByUser(arrayOfAllOffers , sesa);
            let assignedToMeCount = await methods.getOffersAssignedToUser(arrayOfAllOffers , sesa);


            const filters = {
                "CREATED_BY_ME_COUNT" : createdByMeCount.count,
                "APPROVED_COUNT" : approvedCount.count,
                "WAITING_FOR_APPROVAL_COUNT" : waitingForApprovalCount.count,
                "REJECTED_COUNT" : rejectedCount.count,
                "ASSIGNED_TO_ME_COUNT" : assignedToMeCount.count
            };
    
            return {
                error: false,
                data: filters
            }
    
        } catch (exception) {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: exception
            }
        }

    }
};

export default getOffersPerFilter;