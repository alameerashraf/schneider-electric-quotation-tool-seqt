import errors from '../../../common/constants/error-codes';

class deleteItemFromOffer{
    constructor(offerService){
        this.service = offerService;
    };

    async execute(offer, arrayOfDeletedItemNumbers){
        let itemsArray = offer.items;
        let newIndexNumber = 1;

        arrayOfDeletedItemNumbers.forEach((itemNo) => {
          let item = offer.items.find((x) => x.itemNo == itemNo);
          let deletedItemIndex = itemsArray.indexOf(item);
          itemsArray.splice(deletedItemIndex , 1);
        });


        for (let index = 0; index < itemsArray.length; index++) {
          itemsArray[index].itemNo = newIndexNumber;
          ++newIndexNumber;
        };
    

        try{
            let result = await this.service.updateByQuery({
                offerNumber: offer.offerNumber
            }, {
                $set: {
                    "items": itemsArray
                }
            });

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                }
            }
        } 
        catch(exception){
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                message: exception
            }
        }
    };
};

export default deleteItemFromOffer;