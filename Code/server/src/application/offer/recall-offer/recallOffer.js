import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class recallOffer{

    constructor(offerService, userService)
    {
        this.offerServiceInstance = offerService;
        this.userServiceInstance = userService;
    }

    async execute(offerNumber)
    {
        try {

            let offer = await this.offerServiceInstance.read({offerNumber : offerNumber});
            let offerTimeline = offer.data.offerTimeline;
            let offerId = offer.data._id;
            let index = offerTimeline.findIndex(o => o.newStatus == "SUBMITTED");

            let remainingOfferTimeline = offerTimeline.slice(index,offerTimeline.length);
            offerTimeline.splice(index,offerTimeline.length);
            offer.data.offerTimeline = offerTimeline;
            offer.data.currentStatus = "ASSIGNED";

            let result = await this.offerServiceInstance.updateByQuery(
                {offerNumber: offerNumber},
                offer.data
                );
            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                    message: result.message
                }
            } else 
            {
                for(var i = 0; i < remainingOfferTimeline.length; i++)
                {
                    let owner = remainingOfferTimeline[i].owner;
                    let user = await this.userServiceInstance.read({sesa : owner});
                    user = user.data;
    
                    if (user.offers.includes(offerId) == true)
                    {
                        let index = user.offers.indexOf(offerId);
                        user.offers.splice(index,1);
                    }
    
                    let manager = user.kpis.manager.toObject();
            
                    Object.keys(manager).forEach((key) => {
                        if(manager[key].find(x => x == offerId.toString()) != undefined)
                        {
                           let index = manager[key].indexOf(offerId);
                           manager[key].splice(index,1);
                           user.kpis.manager = manager;
                        }
                       
                    });
    
                    let updateUserInstance = await this.userServiceInstance.updateByQuery(
                        {sesa: owner},
                        user
                    );
    
                    if (updateUserInstance.error) {
                        return {
                            error: true,
                            code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                            message: updateUserInstance.message
                        }
                    }
                }
                
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                message: exception
            }
        }
    }
};

export default recallOffer;