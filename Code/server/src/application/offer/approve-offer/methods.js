import xlsx from 'xlsx'
import offerStatus from '../../../common/constants/offer-status';

const parsingXLSXsheet = (sheet) => {
    const approvalCycleJSON = JSON.parse(JSON.stringify(sheet)) 
    let approvalArray = []
    let temp = ''
    for (let [key, value] of Object.entries(approvalCycleJSON)){
        if (key[0] === '!') continue;
        if( key[0] === 'A')
            temp = approvalCycleJSON[key].v
        else if (key[0] === 'B'){
            approvalArray.push({
                amount: temp,
                order: approvalCycleJSON[key].v
            })
        }
    }
    return approvalArray
}

const isCompletelyApproved = (department, amount, latestMove) => {
    let order = 'n';
    let approvalArray
    try{
        const approvalCycleWorkbook = xlsx.readFile('approval-cycle.xlsx')
        const approvalCycleSheet =  approvalCycleWorkbook.Sheets[department]
        approvalArray = parsingXLSXsheet(approvalCycleSheet)
    }catch(error){
        return {
            error: true,
            message: "Error reading file"
        }
    }
    
    approvalArray.some( element => {
        if(amount > element.amount){
            order = element.order
        }
    })

    if(latestMove.lastApproval == order)
        return true
    return false
};

export default {
    isCompletelyApproved
}