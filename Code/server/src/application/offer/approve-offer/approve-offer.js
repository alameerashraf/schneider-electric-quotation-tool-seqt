import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import offerStatus from '../../../common/constants/offer-status';
import user from '../../user/user.app'
import methods from './methods'


let userApps = user()

class approveOffer{
    constructor(offerService){
        this.service = offerService;
    };

    async execute(offer){
        if(!offer.error){
            //making sure it wasn't approved before
            for (const move of offer.offerTimeline){
                if(move.newStatus == offerStatus.APPROVED || move.newStatus == offerStatus.REJECTED) 
                    return {
                        error: false,
                        data: null
                    }
            }
            const oldTimeline = offer.offerTimeline.find(element => element.isLatest)
            const oldOwner = await userApps.getUserBySesa.execute(oldTimeline.owner)
            // getting the tendering engineer to use his department
            const tenderingEngineer = await userApps.getUserBySesa.execute(offer.assignedTo)
            if (tenderingEngineer.error || oldOwner.error) {
                return {
                    error: true,
                    code: errors.ERROR_LOADING_DATA_FROM_DATABASE ,
                    message: tenderingEngineer.message || oldOwner.message
                }
            }

            let newOwner = oldTimeline.owner
            let newStatus = oldTimeline.newStatus 
            let oldStatus = oldTimeline.newStatus
            let lastApproval = 'n'

            const isCompletelyApprovedResult = methods.isCompletelyApproved(tenderingEngineer.data.department, offer.amount, oldTimeline)
            if(isCompletelyApprovedResult.error){
                return {
                    error: true,
                    code: errors.ERROR_READING_FILE ,
                    message: isCompletelyApprovedResult.message
                }
            }

            if(isCompletelyApprovedResult){
                newStatus = offerStatus.APPROVED
                oldStatus = oldTimeline.newStatus
                if(oldTimeline.lastApproval)
                    lastApproval = oldTimeline.lastApproval
                offer.currentStatus = offerStatus.APPROVED;
            } else {
                lastApproval = 'n+1'
                newOwner = oldOwner.data.manager.sesa
                if(oldTimeline.lastApproval == 'n+1')
                    lastApproval = 'n+2'
            }

            const newTimeline = {
                "changedAt": Date.now(),
                "changedBy": oldTimeline.owner,
                "owner": newOwner,
                "isLatest": true,
                "newStatus": newStatus,
                "oldStatus": oldStatus,
                "lastApproval": lastApproval
            };

            
            offer.offerTimeline.forEach(element => {
                element.isLatest = false;
            });

            offer.offerTimeline.push(newTimeline);

            const result = await this.service.updateByQuery(
                { offerNumber: offer.offerNumber },
                offer
            );

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE ,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: newTimeline
                }
            }
        } else {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: offer.message || ""
            }
        }
    };
};


export default approveOffer;