import status from '../../../common/constants/offer-actions-map';

class defineOfferActions{    
    //offerOwner: the one who creates the offer.
    //offerHandler: the tendering engineer that has been assigned to the offer.

    execute(offerStatus , requester , offerOwner , offerHandler , userActions){
        let userActionTypes = [];
        let validActionsPerUser = [];
        let availableActions = status[offerStatus];

        let isOwner = requester == offerOwner ? 'OWNER' : 'NOT_OWNER';
        userActionTypes.push(isOwner);

        // If the tendering manager assign the offer to him self, I need to grant the NOT_OWNER actions to him.
        if(offerOwner == offerHandler){
            userActionTypes.push("NOT_OWNER");
        };

        userActionTypes.forEach(type => {
            validActionsPerUser  = validActionsPerUser.concat(availableActions[type]);
        });


        let matches = [];
        userActions.forEach(function (e) {
            matches = matches.concat(
                validActionsPerUser.filter(function (c) {
                    return c === e;
                })
            );
        });

        return matches;
    }
}

export default defineOfferActions;