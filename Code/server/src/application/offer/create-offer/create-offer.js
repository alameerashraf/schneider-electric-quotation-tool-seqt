import logger from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class createOffer {
    constructor(offerService) {
        this.service = offerService;
    }

    /**
     * Run offer creation 
     */
    async execute(offer) {
        try {
            let result = await this.service.create(offer);
            if (result.error) {
                return {
                    error: true,
                    code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                    message: result.message 
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: exception
            }
        }
    }
}

export default createOffer;