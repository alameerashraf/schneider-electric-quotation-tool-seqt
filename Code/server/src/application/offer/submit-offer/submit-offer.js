import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import offerStatus from '../../../common/constants/offer-status';


class submitOffer{
    constructor(offerService){
        this.service = offerService;
    };

    async execute(offer , assignmentData){

        offer.offerTimeline.forEach(element => {
            element.isLatest = false;
        });


        // Set the owner to the current manager 
        assignmentData["owner"] = offer.createdBy;
        assignmentData["isLatest"] = true;

        // reset all timeline isRecent ==> false 
        // add new move to the time line 
        if(!offer.error){
            //making sure it wasn't submitted before
            for (const move of offer.offerTimeline){
                if(move.newStatus == offerStatus.SUBMITTED) 
                    return {
                        error: false,
                        data: null
                    }
            }
            // reset all timeline isRecent ==> false 
            // add new move to the time line 
            offer.offerTimeline.forEach(element => {
                element.isLatest = false;
            });

            offer.currentStatus = offerStatus.SUBMITTED;
            offer.offerTimeline.push(assignmentData);

            const result = await this.service.updateByQuery(
                { offerNumber: offer.offerNumber },
                offer
            );

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE ,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } else {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: offer.message || ""
            }
        }


        
    };
};


export default submitOffer;