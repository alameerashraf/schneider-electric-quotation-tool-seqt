import errors from '../../../common/constants/error-codes';

class getLatestCreatedOfferNumber{
    constructor(offerService){
        this.service = offerService;
    }


    async execute(seRefrence){
        try{
            let latestOffer = await this.service.getLatest({ "seRefrence" : seRefrence });
            if(!latestOffer.error){
                return {
                    data: latestOffer.data,
                    error: false
                }
            } else {
                return {
                    error: true,
                    error: latestOffer.error,
                    code: errors.ERROR_LOADING_DATA_FROM_DATABASE
                }
            }
        } catch(exception){
            return {
                error: true,
                error: exception,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE
            }
        }
    }
};


export default getLatestCreatedOfferNumber;