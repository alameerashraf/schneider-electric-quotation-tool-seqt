import path from 'path';
import fs from 'fs';
import errors from '../../../common/constants/error-codes';
class getCountries{
    constructor(){

    }

    async execute(){
        let countriesURL = path.join(__dirname, '../../../common/data/countries.json');
        return new Promise((resolve, reject) => {
            fs.readFile(countriesURL, 'utf8' , (err, data) => {
                if (!err) {
                    let actualData = {
                        data: JSON.parse(data),
                        error: false
                    };
                    resolve(actualData);
                } else {
                    resolve({
                        error: true,
                        code: errors.ERROR_LOADING_DATA_FROM_EXTERNAL_SOURCE,
                        exception: err
                    })
                }
            })
        });
    }
};


export default getCountries;