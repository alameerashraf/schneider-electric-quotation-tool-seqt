import path from "path";
import fs from "fs";
import xlsx from "node-xlsx";
import errors from "../../../common/constants/error-codes";

class getExpenses {
  constructor() {}

  execute() {
    try {
      let expensesFilePath = path.join(
        __dirname,
        "../../../../uploads/Expenses-categories/_last.xlsx"
      );
      const expensesFile = xlsx.parse(fs.readFileSync(expensesFilePath));

      let fileData = expensesFile[0].data;

      let expense = {};
      let expensesCategories = [];
      fileData.forEach(row => {
        if (row.length != 0) {

          //It's header group name of expenses!
          if (row[1] == null || typeof row[1] == "undefined") {
            if ( Object.keys(expense).length !== 0) {
              expensesCategories.push(expense);
              expense = {};
            }
            expense["groupName"] = row[0];
            expense["expensesList"] = [];
          } else {
            expense["expensesList"].push({
                name: row[0],
                valueType: row[2],
                itemsAvailability: row[3] == 'Yes' ? true : false,
                generalAvailability: row[4] == 'Yes' ? true : false,
                hourlyRate : row[5] !== null ? row[5] : ""
            });
          }
        }
      });


      return {
        data: expensesCategories,
        error: false
      };
    } catch (exception) {
      return {
        error: true,
        code: errors.ERROR_LOADING_DATA_FROM_EXTERNAL_SOURCE,
        exception: exception
      };
    }
  }
}

export default getExpenses;
