import roleService from '../../services/role.service';
import role from '../../models/user/role';

import getCountries from './get-countries/get-countries';
import getExpenses from './get-expenses/get-expenses';
import getCurrenciesRates from './get-currencies-rates/get-currencies-rates';
import getRoles from './get-roles/get-roles';

let service = new roleService(new role().constructModel());


export default () => {
    return Object.freeze({
        getCountries: new getCountries(),
        getExpenses: new getExpenses(),
        getCurrencies: new getCurrenciesRates(),
        getRoles: new getRoles(service)
    });
};
