

import notifyByEmail from './notify-by-email/notify-by-email';




export default () => {
    return Object.freeze({
        notifyByEmail: new notifyByEmail()
    });
};


