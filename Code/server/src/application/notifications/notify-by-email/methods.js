import fs from 'fs';
import nodemailer from 'nodemailer';

function emailTemplate (path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

function createTransport() {
  let smtp = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: "587",
    secure: false,
    auth: {
      user: "alamiir.ashraf@gmail.com",
      pass: "01060931989",
    },
  });

  return smtp;
};

function loadReplacements(body){
    let replacemnets = {
        title: body.title,
        receiverName: body.receiverInfo.name,
        message: body.message,
        btnMessage: body.btnMessage,
        btnLink: body.btnLink,
        optionalMessage: body.optionalMessage,
        senderName: body.senderInfo.name,
        senderEmail: body.senderInfo.email,
    };

    return replacemnets;
};

function loadOptions(from , senderEmail , to , subject, htmlToSend){
    let emailOptions = {
        from: [from , senderEmail],
        to: to,
        subject : subject,
        html : htmlToSend
    };
    
    return emailOptions;
}



const methods = {
    emailTemplate,
    createTransport,
    loadReplacements,
    loadOptions
};


export default methods;