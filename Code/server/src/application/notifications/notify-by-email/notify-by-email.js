import path from 'path';
import handlebars from 'handlebars';
import methods from './methods';
import errors from '../../../common/constants/error-codes';


class notifyByEmail{

    async execute(notificationBody){
        return new Promise((resolve , reject) => {
            let templatePath = path.join(__dirname , '../../../../templates/email/simple.html');
            methods.emailTemplate(templatePath , (err , htmlTemplate) =>{
                let template = handlebars.compile(htmlTemplate);
                let replacments = methods.loadReplacements(notificationBody);
                let finalHTML = template(replacments);

                let transports = methods.createTransport();
                let options = methods.loadOptions("" , notificationBody.senderInfo.email , notificationBody.receiverInfo.email , 
                    notificationBody.subject , finalHTML);
                transports.sendMail(options , (err , res) =>{
                    if (!err) {
                        resolve({error: false});
                    } else {
                      resolve({
                        error: true,
                        code: errors.FAILED_TO_SEND_NOTIFICATION,
                        exception: err,
                      });
                    }
                });
            });
        })
    }
};


export default notifyByEmail;