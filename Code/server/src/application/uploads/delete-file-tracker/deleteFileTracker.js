import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class deleteFileTracker {

    constructor(fileTrackerService) {
        this.service = fileTrackerService;
    }

    async execute(offerNumber) {
        try {
            let result = await this.service.deleteByQuery(
                { offerNumber: offerNumber }
            );
            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                }
            }
        }
         catch (error) {
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                message: exception
            }
        }
    }
};

export default deleteFileTracker;