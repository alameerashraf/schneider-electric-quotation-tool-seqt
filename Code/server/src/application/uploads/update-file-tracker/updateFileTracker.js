import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';


class updateFileTracker{

    constructor(fileTrackerService)
    {
        this.service = fileTrackerService;
    }

    async execute(offerNumber, uploadedFile)
    {
        try {
            let result = await this.service.updateByQuery(
                {offerNumber: offerNumber},
                uploadedFile
                );
            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                message: exception
            }
        }
    }
};

export default updateFileTracker;