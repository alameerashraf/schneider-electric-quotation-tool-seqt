import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import path from 'path'
import fs from 'fs'
class uploadExpensesFile {


    async execute(file) {

        try {
            let today = new Date();
            let getDate = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() + '-' + today.getHours() + '-' + today.getMinutes();
            let filePath = path.join(__dirname + '../../../../../uploads/Expenses-categories/_last.xlsx');
            let datedFilePath = path.join(__dirname +  '../../../../../uploads/Expenses-categories/' + getDate + '.xlsx');
            fs.rename(filePath, datedFilePath,function(err){
                if (err)
                    return {
                        error: true,
                        code: errors.ENABLE_TO_LOAD_FILE,
                        message: err
                    }
            });

            
            let expensesFile = file;
            let result = expensesFile.mv(filePath);
            if (!result) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPLOAD_FILE_TO_SERVER,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                }
            }
        } catch (error) {
            return {
                error: true,
                code: errors.PROCESS_TERMINATED,
                message: error
            }
        }

        
    }
};


export default uploadExpensesFile;