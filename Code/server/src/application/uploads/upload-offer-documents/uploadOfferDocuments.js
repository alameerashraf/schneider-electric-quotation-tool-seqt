import fs from 'fs';
import path from 'path';
import files from '../../../common/constants/file-types';

class uploadOfferDocuments {
    constructor() {

    }

    execute(file, offerNumber, fileType) {
        let folderName = files["offer"][fileType] == undefined ? "other" : files["offer"][fileType];
        let dir = path.join(__dirname, '../../../../uploads', '/Offers/', offerNumber, folderName);


        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, {
                recursive: true
            });
        }

        return new Promise((resolve, reject) => {
            file.mv(`${dir}/${file.name}`, (err) => {
                if (!err)
                    resolve({
                        error: false,
                    })
                else
                    resolve({
                        error: false,
                        message: err
                    });
            })
        })
    }
}

export default uploadOfferDocuments;