import uploadExpensesFile from './upload-expenses-file/uploadExpensesFile';
import uploadFamilyMasterFile from './upload-family-master-file/uploadFamilyMasterFile';
import uploadExchangeRateFile from './upload-exchange-rate-file/uploadExchangeRateFile';
import logUploadedFile from './log-uploaded-file/logUploadedFile'
import createFileTracker from './create-file-tracker/createFileTracker'
import updateFileTracker from './update-file-tracker/updateFileTracker'
import listFiles from './list-files/listFiles'
import deleteFileTracker from './delete-file-tracker/deleteFileTracker'
import uploadOfferDocuments from './upload-offer-documents/uploadOfferDocuments';

import loggerService from '../../services/logger.service';
import logger from '../../models/logger/logger';
import fileTrackerService from '../../services/fileTrackerService';
import fileTracker from '../../models/file-tracker/fileTracker';

const loggerServiceInstance = new loggerService(new logger().constructModel());
const fileTrackerServiceInstance = new fileTrackerService(new fileTracker().constructModel());

export default () => {
    return Object.freeze({
        uploadExpensesFile: new uploadExpensesFile(),
        uploadFamilyMasterFile: new uploadFamilyMasterFile(),
        uploadExchangeRateFile: new uploadExchangeRateFile(),
        logUploadedFile: new logUploadedFile(loggerServiceInstance),
        createFileTracker: new createFileTracker(fileTrackerServiceInstance),
        updateFileTracker: new updateFileTracker(fileTrackerServiceInstance),
        listFiles: new listFiles(fileTrackerServiceInstance),
        deleteFileTracker: new deleteFileTracker(fileTrackerServiceInstance),
        uploadOfferDocuments: new uploadOfferDocuments()
    })
};
