import errors from '../../../common/constants/error-codes';
import admin from '../../../application/admin/admin.app';
let adminApps = admin();
import path from 'path'
import fs from 'fs';

class uploadFamilyMasterFile{


    async execute(file){
        try{
            let today = new Date();
            let todaysDate = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() + '-' + today.getHours() + '-' + today.getMinutes();
            let oldFilePath = path.join(__dirname + '../../../../../uploads/Families/_last.xlsx');
            let newlyFilePathToBeRenamed = path.join(__dirname +  '../../../../../uploads/Families/' + todaysDate + '.xlsx');
            fs.rename(oldFilePath, newlyFilePathToBeRenamed, function(err){
                if (err)
                    return {
                        error: true,
                        code: errors.ENABLE_TO_LOAD_FILE,
                        message: err
                    }
            });

            let uploadedFile = file;
            let result = uploadedFile.mv(oldFilePath , async (err) => {
                if(!err){
                    await adminApps.bulkCreateFamilies.execute();
                }
            });

            return {
                error: false
            }


        } catch(error){
            return {
                error: true,
                code: errors.PROCESS_TERMINATED,
                message: error
            }
        }
    }
};


export default uploadFamilyMasterFile;