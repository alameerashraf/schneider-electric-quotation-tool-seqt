import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class logUploadedFile {

    constructor(loggerService) {
        this.service = loggerService;
    }

    async execute(data) {

        try 
        {
            let result = await this.service.create(data);
            
            if (result.error) 
            {
                
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            }
             else 
             {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) 
        {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};

export default logUploadedFile;