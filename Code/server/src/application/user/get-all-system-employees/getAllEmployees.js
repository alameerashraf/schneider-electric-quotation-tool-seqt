import errors from '../../../common/constants/error-codes';


class getAllEmployees {
    constructor(userService) {
        this.service = userService;
    }

    async execute(query) {
        try {
            
            let result = await this.service.loadAllUsers(query);

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                const engineers = result.data.filter(user => {
                    return user.roles.find(x => x.shortcut == "TE") != undefined || user.roles.find(x => x.shortcut == "SE") != undefined
                });
                return {
                    error: false,
                    data: engineers
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};

export default getAllEmployees;