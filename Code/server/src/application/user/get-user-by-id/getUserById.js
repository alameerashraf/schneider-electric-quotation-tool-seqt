class getUserById{
    constructor(userService){
        this.service = userService;
    }

    async execute(id) {
        try {
            let result = await this.service.read({
                _id: id
            });

            if (result.error) {
                return {
                    error: true,
                    code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                    message: result.exception 
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: exception
            }
        }
    };
};


export default getUserById;