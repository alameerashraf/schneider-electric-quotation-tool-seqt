import kpiStatusMap from '../../../common/constants/kpi-status-map';
import loggers from '../../../loaders/logger';


function getKPIs(creator, isCreatorManager, owner, isOwnerManager, status , offerId){

    let userKPIs = [];
    let kpi = {};
    let role = "";

    // Load the creator KPI
    // if the creator at any time is not a manager!
    kpi = isCreatorManager ? kpiStatusMap.manager[status] : kpiStatusMap.employee[status];
    role = isCreatorManager ? "manager" : "employee" ;
    
    if (creator.sesa == owner.sesa) {
        userKPIs.push({
            user: creator.sesa,
            offer : offerId,
            kpi: kpi,
            role: role
        });
    } else {
        userKPIs.push({
            user: creator.sesa,
            offer : offerId,
            kpi: kpi,
            role: role
        });

        userKPIs.push({
            user: owner.sesa,
            offer : offerId,
            kpi: isOwnerManager ? kpiStatusMap.manager[status] : kpiStatusMap.employee[status],
            role: isOwnerManager ? "manager" : "employee"
        })
    }
    return userKPIs;
};

/**
 * erasing offer id from the old kpi
 * @param {user whose kpis will be updated} user 
 * @param {entry from the userKPIs array} userKPI 
 */
function eraseOfferFromTheOldKPI(user, userKPI){
    const keys = Object.keys(user.kpis[userKPI.role])
    keys.forEach(kpi => {
        if(kpi != "$init"){
            let filteredKPIS = user.kpis[userKPI.role][kpi].filter(offerID => offerID.toString() != userKPI.offer.toString())
            user.kpis[userKPI.role][kpi] = filteredKPIS
        }
    })
}

async function addOfferToUserKPI(userKPIs){
    let result = true;
    for (let index = 0; index < userKPIs.length; index++) {
        const element = userKPIs[index];
        let user = await this.service.read({ sesa: element.user });
        if(!user.error){
            eraseOfferFromTheOldKPI(user.data, element)
            let arrayOfKPI = user.data.kpis[element.role][element.kpi];
            if(!arrayOfKPI.includes(element.offer)){
                user.data.kpis[element.role][element.kpi] = arrayOfKPI.push(element.offer);
                let updateResult = await this.service.updateByQuery({ sesa: element.user } , user.data);
                if(!updateResult.error){
                    result = true;
                } else {
                    loggers.error(`error: ${updateResult.message}     ###LOCATION update-user-kpis.addOfferToUserKPI`);
                    result = false;
                }
            }
        } else {
            loggers.error(`error: ${error.message}     ###LOCATION update-user-kpis.addOfferToUserKPI`);
            result = false;
        }
    }
    return result;
};




export default {
    getKPIs,
    addOfferToUserKPI
};