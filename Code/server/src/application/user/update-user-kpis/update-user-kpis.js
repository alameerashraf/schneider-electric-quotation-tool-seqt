import methods from './methods';
import errors from '../../../common/constants/error-codes';

class updateUserKpis {

    constructor(userService) {
        this.service = userService;
        methods.addOfferToUserKPI = methods.addOfferToUserKPI.bind(this);
    }

    async execute(offer) {
        const latestMove = offer.offerTimeline.find(x => x.isLatest == true);
        let creator = await this.service.loadAll_UserDataBySESA({ sesa: offer.createdBy})
        const isCreatorManager = creator.data?.roles.find(x => x.shortcut == 'TM') == undefined ? false : true;

        let owner = await this.service.loadAll_UserDataBySESA({ sesa: latestMove.owner})
        const isOwnerManager = owner.data?.roles.find(x => x.shortcut == 'TM') == undefined ? false : true;

        const status = latestMove.newStatus;
        
        let kpis = methods.getKPIs(creator.data, isCreatorManager, owner.data, isOwnerManager, status , offer._id);
        let result = await methods.addOfferToUserKPI(kpis);
        if (!result) {
          return {
            error: true,
            code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
            message: result.message
          };
        } else {
          return {
            error: false,
            data: result.data
          };
        }
    }
}

export default updateUserKpis;