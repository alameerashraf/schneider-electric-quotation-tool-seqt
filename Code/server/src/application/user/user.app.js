import userService from '../../services/user.service';
import user from '../../models/user/user';

import createUser from './create-user/create-user';
import getUserBySesa from './get-user-by-sesa/get-user-by-sesa';
import updateUserOffers from './update-user-offers/update-user-offers';
import updateUserKPIs from './update-user-kpis/update-user-kpis';
import getUserById from './get-user-by-id/getUserById';
import getAllTenderingEngineer from './get-all-tendering-engineers/get-all-tendering-engineers';
import getAllManagers from './get-all-system-managers/getAllManagers';
import getAllEmployees from './get-all-system-employees/getAllEmployees';

const service = new userService(new user().constructModel());


export default () => {
    return Object.freeze({
        createUser: new createUser(service),
        getUserBySesa: new getUserBySesa(service),
        updateUserOffers: new updateUserOffers(service),
        updateUserKPIs: new updateUserKPIs(service),
        getUserById: new getUserById(service),
        getAllTenderingEngineer: new getAllTenderingEngineer(service),
        getAllManagers: new getAllManagers(service),
        getAllEmployees: new getAllEmployees(service)
    })
};


