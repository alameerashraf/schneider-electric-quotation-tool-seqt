import errors from '../../../common/constants/error-codes';


class getUserBySesa {
    constructor(userService) {
        this.service = userService;
    }

    async execute(sesa) {
        try {
            let result = await this.service.loadAll_UserDataBySESA({
                sesa: sesa
            });

            if (result.error) {
                return {
                    error: true,
                    code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                    message: result.exception 
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: exception
            }
        }
    }
};

export default getUserBySesa;