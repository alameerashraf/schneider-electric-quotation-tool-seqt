import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';


class getAllManagers {
    constructor(userService) {
        this.service = userService;
    }

    async execute(query) {
        try {
            
            let result = await this.service.loadAllUsers(query);

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                const managers = result.data.filter(user => {
                    return user.roles.find(x => x.shortcut == "TM") != undefined || user.roles.find(x => x.shortcut == "SM") != undefined
                });
                return {
                    error: false,
                    data: managers
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};

export default getAllManagers;