import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import mongoose from 'mongoose'


class updateUserOffers {
    constructor(userService) {
        this.service = userService;
    }

    async execute(userSESA, offer, mode) {
        try {
            let operator = { $push: { "offers": offer } };

            if(mode == "remove"){
                operator = { $pull: { "offers" : mongoose.Types.ObjectId(offer) } };
            }


            let result = await this.service.updateByQuery({
                sesa: userSESA
            }, operator , { multi: true } );


            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};

export default updateUserOffers;