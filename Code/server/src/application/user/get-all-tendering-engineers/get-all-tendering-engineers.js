import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';


class getAllTenderingEngineer {
    constructor(userService) {
        this.service = userService;
    }

    async execute(query) {
        try {
            
            let result = await this.service.loadAllUsers(query);

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                const engineers = result.data.filter(user => {
                    return user.roles.length == 1 && user.roles[0].name == "TenderingEngineer"
                });
                return {
                    error: false,
                    data: engineers
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};

export default getAllTenderingEngineer;