import offerActions from './offer/offer.app';

export default () => {
    return Object.freeze({
       offerActions: offerActions 
    });
}