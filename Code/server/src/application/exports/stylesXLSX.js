const headerStyle = {
    alignment: { 
        horizontal: 'center',
        vertical: 'center',
        justifyLastLine: true,
        readingOrder: 'leftToRight', 
        wrapText: true
    },
    font: {
        bold: true,
        color: 'white',
        condense: false,
        extend: false,
        family: 'Roman',
        outline: true,
        scheme: 'major',
        shadow: true,
        size: 10,
    },
    border: { 
        left: {
            style: 'thick',
            color: '#444444'
        },
        right: {
            style: 'thick',
            color: '#444444'
        },
        top: {
            style: 'thick',
            color: '#444444'
        },
        bottom: {
            style: 'thick',
            color: '#444444'
        },
        diagonal: {
            style: 'thick',
            color: '#444444'
        },
        diagonalDown: true,
        outline: true
    },
    fill: {
        type: 'pattern',
        patternType: 'solid',
        fgColor:'26a612'
    },
}

const footerStyle = {
    alignment: { 
        horizontal: 'center',
        vertical: 'center',
        justifyLastLine: true,
        readingOrder: 'leftToRight', 
        wrapText: true
    },
    font: {
        bold: true,
        color: 'white',
        condense: false,
        extend: false,
        family: 'Roman',
        outline: true,
        scheme: 'major',
        shadow: true,
        size: 10,
    },
    border: { 
        top: {
            style: 'thick',
            color: '#444444'
        },
        bottom: {
            style: 'thick',
            color: '#444444'
        },
        diagonal: {
            style: 'thick',
            color: '#444444'
        },
        diagonalDown: true,
        outline: true
    },
    fill: {
        type: 'pattern',
        patternType: 'solid',
        fgColor:'26a612'
    },
}


const cellStyle = {
    alignment: { 
        horizontal: 'center',
        vertical: 'center',
        justifyLastLine: true,
        readingOrder: 'leftToRight', 
        wrapText: true
    },
    border: { 
        left: {
            style: 'thin',
            color: '#444444'
        },
        right: {
            style: 'thin',
            color: '#444444'
        },
        top: {
            style: 'thin',
            color: '#444444'
        },
        bottom: {
            style: 'thin',
            color: '#444444'
        },
        diagonal: {
            style: 'thin',
            color: '#444444'
        },
        diagonalDown: true,
        outline: true
    },
}

const fullBoarder = {
    border: { 
        left: {
            style: 'thick',
            color: '#444444'
        },
        right: {
            style: 'thick',
            color: '#444444'
        },
        top: {
            style: 'thick',
            color: '#444444'
        },
        bottom: {
            style: 'thick',
            color: '#444444'
        },
        diagonal: {
            style: 'thick',
            color: '#444444'
        },
    }
}

export default {
    headerStyle,
    footerStyle,
    cellStyle,
    fullBoarder
}