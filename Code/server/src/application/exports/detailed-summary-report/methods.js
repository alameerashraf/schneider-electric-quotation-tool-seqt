import fs from 'fs'
import errors from '../../../common/constants/error-codes'
import headers from '../../../common/constants/reports-headers'
import styles from '../stylesXLSX'
import xlsx from 'excel4node'

function makeBaseDir (userSESA) {
    const date = new Date()
    const today = date.getDate() + '-' + (date.getMonth()+1) +'-' + date.getFullYear()
    const baseDir = 'exports/' + today +'/'+ userSESA
    if(!fs.existsSync(baseDir)){
        fs.mkdirSync(baseDir, {recursive: true})
    }
    return baseDir
}

function constructReportData(items) {
    let jsonItems = []
    items.forEach( item => {
        jsonItems.push(item._doc)
    })
    return jsonItems
}

function makeXLSXReport(reportName, baseDir, data, projectName){
    try{
        const workbook = new xlsx.Workbook()
        const worksheet = workbook.addWorksheet(reportName);
        // adding the Project name at top
        worksheet.cell(1, 1, 1, 17, true)
            .style(styles.headerStyle)
            .string(projectName)
        //fill the sheet with data
        fillSheet(headers.detailedSummaryHeaders, data, worksheet)
        workbook.write(baseDir + '/' + reportName + '.xlsx');
        return {
            error: false,
            data:(baseDir + '/' + reportName + '.xlsx').replace("exports/" , '/')
        }
    }catch(error){
        return {
            error: true,
            code: errors.ERROR_MAKING_FILE,
            message: error
        }
    }
    
}

//fill excel sheet with headers and data
const fillSheet = (headers, data, sheet) => {
    // filling in data through the sheet 
    const numOfColumns = (Object.keys(headers)).length
    const headerRow = 3
    const footerRow = headerRow + data.length + 1
    let i = 1
    for (let key of Object.keys(headers)){
        sheet.cell(headerRow,i).string(headers[key].columnName)
        i++
    }
    for (i=0; i < data.length; i++){
        let j = 1
        for(let [key, value] of Object.entries(headers)){
            if(data[i][key])
                sheet.cell(i + headerRow + 1, j)[value.type](data[i][key])
            j++
        }
    }
    //adding formulas
    sheet.cell(footerRow, 1, footerRow, 6, true)
        .string('de formula ana m4 fahmha el 72e2a')
        .style(styles.headerStyle)
    sheet.cell(footerRow, 7).formula(`SUM((G${headerRow + 1}):G${footerRow - 1})`)
    sheet.cell(footerRow, 10).formula(`SUM((J${headerRow + 1}):J${footerRow - 1})`)
    sheet.cell(footerRow, 12).formula(`SUM((L${headerRow + 1}):L${footerRow - 1})`)
    sheet.cell(footerRow, 13).formula(`SUM((M${headerRow + 1}):M${footerRow - 1})`)
    sheet.cell(footerRow, 14).formula(`(Q${footerRow} - (J${footerRow} - L${footerRow}))/Q${footerRow}`)
    sheet.cell(footerRow, 17).formula(`SUM((Q${headerRow + 1}):Q${footerRow - 1})`)
    // finishing styling
    sheet.cell(headerRow, 1, headerRow, numOfColumns).style(styles.headerStyle)
    sheet.cell(headerRow+1, 1, footerRow - 1, numOfColumns).style(styles.cellStyle)
    sheet.cell(footerRow, 1, footerRow, numOfColumns).style(styles.footerStyle)
}

export default {
    makeBaseDir,
    constructReportData,
    makeXLSXReport,
}