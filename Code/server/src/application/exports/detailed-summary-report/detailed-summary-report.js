import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import methods from './methods'



class detailedSummaryReport {

    constructor(offerService){
        this.service = offerService;
    };

    async execute(reportMetaData, user) {
        if(user && reportMetaData.entity.offerNumber){
            const baseDir = methods.makeBaseDir(user)
            let offer = (await this.service.read({
                offerNumber: reportMetaData.offerNumber
            }))

            const reportData = methods.constructReportData(offer.data.items)
            const result = methods.makeXLSXReport(reportMetaData.reportName, baseDir, reportData, offer.data.opportunityName)
            if (result.error) {
                return {
                    error: true,
                    code: result.code ,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: [result.data]
                }
            }
        }else 
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: user.message || ""
            }
    }
};

export default detailedSummaryReport;