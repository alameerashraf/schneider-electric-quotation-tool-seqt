import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import methods from './methods'



class generalExpensesSummary {

    constructor(offerService){
        this.service = offerService;
    };

    async execute(reportMetaData, user) {
        if(user && reportMetaData.entity.offerNumber){
            const baseDir = methods.makeBaseDir(user)
            let offer = (await this.service.read({
                offerNumber: reportMetaData.offerNumber
            }))

            const reportData = methods.constructReportData(offer.data.expenses)
            const result = methods.makeXLSXReport(reportMetaData.reportName, baseDir, reportData)

            if (result.error) {
                return {
                    error: true,
                    code: result.code ,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: [result.data]
                }
            }
        }else 
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: user.message || ""
            }
    }
};

export default generalExpensesSummary;