import fs from 'fs'
import errors from '../../../common/constants/error-codes'
import headers from '../../../common/constants/reports-headers'
import styles from '../stylesXLSX'
import xlsx from 'excel4node'

function makeBaseDir (userSESA) {
    const date = new Date()
    const today = date.getDate() + '-' + (date.getMonth()+1) +'-' + date.getFullYear()
    const baseDir = 'exports/' + today +'/'+ userSESA
    if(!fs.existsSync(baseDir)){
        fs.mkdirSync(baseDir, {recursive: true})
    }
    return baseDir
}

function constructReportData(expenses) {
    let jsonItems = []
    expenses.forEach( expense => {
        jsonItems.push(expense._doc)
    })
    return jsonItems
}

function makeXLSXReport(reportName, baseDir, data){

    try{
        const workbook = new xlsx.Workbook()
        const worksheet = workbook.addWorksheet(reportName);
        //fill the sheet with data
        fillSheet(headers.generalExpensesSummary, data, worksheet)
        workbook.write(baseDir + '/' + reportName + '.xlsx');
        return {
            error: false,
            data:(baseDir + '/' + reportName + '.xlsx').replace("exports/" , '/')
        }
    }catch(error){
        return {
            error: true,
            code: errors.ERROR_MAKING_FILE,
            message: error.toString()
        }
    }
    
}

//fill excel sheet with headers and data
const fillSheet = (headers, data, sheet) => {
    const numOfColumns = (Object.keys(headers)).length
    const headerRow = 3
    const footerRow = headerRow + data.length + 1

    // adding the Project name at top
    sheet.cell(1, 1, 1, numOfColumns, true)
        .style(styles.headerStyle)
        .string("General Expenses")
    // filling in data through the sheet 
    let i = 1
    for (let key of Object.keys(headers)){
        sheet.cell(headerRow,i).string(headers[key].columnName)
        i++
    }
    for (i=0; i < data.length; i++){
        let j = 1
        let currentRow = i + headerRow + 1
        for(let [key, value] of Object.entries(headers)){
            if(data[i][key])
                sheet.cell(currentRow, j)[value.type](data[i][key])
            else if (key == 'netValue')
                sheet.cell(currentRow, j).formula(`B${currentRow}*C${currentRow}`)
            j++
        }
    }
    //adding formulas
    sheet.cell(footerRow, 1, footerRow, 3, true)
        .string('TOTAL EGP')
        .style(styles.headerStyle)
    sheet.cell(footerRow, 4).formula(`SUM((D${headerRow + 1}):D${footerRow - 1})`)
    // finishing styling
    sheet.cell(headerRow, 1, headerRow, numOfColumns).style(styles.headerStyle)
    sheet.cell(headerRow+1, 1, footerRow - 1, numOfColumns).style(styles.cellStyle)
    sheet.cell(footerRow, 1, footerRow, numOfColumns).style(styles.footerStyle)
}

export default {
    makeBaseDir,
    constructReportData,
    makeXLSXReport,
}