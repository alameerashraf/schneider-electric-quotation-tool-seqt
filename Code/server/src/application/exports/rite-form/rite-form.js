import methods from './methods';
import errors from '../../../common/constants/error-codes';


class riteForm {
    constructor(offerService) {
        this.service = offerService;
    }

    async execute(requestedReportInfo, user) {
        let offerNumber;

        if (requestedReportInfo.entity.offerNumber) {
            offerNumber = requestedReportInfo["entity"]["offerNumber"].replace("*", "$");
        } else {
            return {
                error: true,
                message: "Offer has no technical documents",
                code: errors.ENABLE_TO_LOAD_FILE
            }
        }

        let exportPath = methods.createExportPath(user);
        let offerData = await this.service.read({ offerNumber: requestedReportInfo["entity"]["offerNumber"] });
        let offerItems = offerData.data.items;
        let offerExpenses = offerData.data.expenses;

        let rit_form = methods.calculateRITEForm(offerItems , offerExpenses);
        let changedCells = await methods.changeCells(exportPath , rit_form);

        if(!changedCells.error){
            return {
                error: false,
                data: [changedCells.url]
            }
        }
    }
};

export default riteForm;