import calculations from './calculations';
import fs from 'fs';
import extra from 'fs-extra';
import path from 'path';
import excelJs from 'exceljs';

let riteFormCells = {
    salesAmount: { row: 7, col: 3},
    COGS_IG: { row: 8, col: 3},
    COGS_OG: { row: 9, col: 3},
    design_engineering_prmgmnt_IG: { row: 11, col: 3},
    design_engineering_prmgmnt_OG: { row: 12, col: 3},
    install_commission_service_IG: { row: 14, col: 3},
    install_commission_service_OG: { row: 15, col: 3},
    other_COGS_IG: { row: 17, col: 3},
    other_COGS_OG: { row: 18, col: 3},
    warranty: { row: 20, col: 3},
    risk: { row: 21, col: 3},
    // TODO: to implement~
    SFC_FO: { row: 24, col: 3},
    SFC_UPS: { row: 25, col: 3},
    Upstream_Margin:{ row: 28, col: 3},
    CCO: { row: 27, col: 3},
    statutory: { row: 29, col: 3},
}

function createExportPath (userSESA) {
    const date = new Date()
    const today = date.getDate() + '-' + (date.getMonth()+1) +'-' + date.getFullYear()
    const baseDir = 'exports/' + today +'/'+ userSESA + '/' + 'rite_form'
    if(!fs.existsSync(baseDir)){
        fs.mkdirSync(baseDir, {recursive: true})
    }
    return baseDir
};


function calculateRITEForm(offerItems , offerExpenses){
    let rite_form = {
        salesAmount: 0,
        COGS_IG: 0,
        COGS_OG: 0 ,
        design_engineering_prmgmnt_IG:0,
        design_engineering_prmgmnt_OG:0,
        install_commission_service_IG:0,
        install_commission_service_OG:0,
        other_COGS_IG:0,
        other_COGS_OG:0,
        warranty:0,
        risk:0,
        // TODO: to implement~
        SFC_FO:0,
        SFC_UPS:0,
        Upstream_Margin:0,
        CCO:0,
        statutory:0

    };

    rite_form.salesAmount = calculations.totalSP2(offerItems);
    rite_form.COGS_IG = calculations.COGS_IG(offerItems);
    rite_form.COGS_OG = calculations.COGS_OG(offerItems);
    rite_form.design_engineering_prmgmnt_IG = calculations.design_engineering_prmgmnt_IG(offerItems);
    rite_form.design_engineering_prmgmnt_OG = calculations.design_engineering_prmgmnt_OG(offerItems);
    rite_form.install_commission_service_IG = calculations.install_commission_service_IG(offerItems);
    rite_form.install_commission_service_OG = calculations.install_commission_service_OG(offerItems);
    rite_form.other_COGS_IG = calculations.other_COGS_IG(offerItems);
    rite_form.other_COGS_OG = calculations.other_COGS_OG(offerItems);
    rite_form.warranty = calculations.warranty(offerItems);
    rite_form.risk = calculations.risk(offerItems);

    return rite_form;
};


function changeCells(exportPath , ritFrom){
    let targetDir = path.join(__dirname , '../../../../templates/' , 'RITE form');

    var workbook = new excelJs.Workbook();

    return new Promise((resolve , reject) =>{
        workbook.xlsx.readFile(`${targetDir}\\RITE Form.xlsx`)
        .then(function() {
            var worksheet = workbook.getWorksheet(1);

            Object.keys(ritFrom).forEach((k) =>{
                changeCell(worksheet , riteFormCells[k]["row"] , riteFormCells[k]["col"] , ritFrom[k] );
            });
            workbook.xlsx.writeFile(`${exportPath}\\RITE Form.xlsx`).then(() => {
                resolve({
                    error: false,
                    url: `${exportPath}\\RITE Form.xlsx`.replace("exports/" , '/')
                })
            })
        })
    })
};

function changeCell(worksheet , rowNumber , colNumber , newValue){
    var row = worksheet.getRow(rowNumber);
    row.getCell(colNumber).value = newValue;
    row.commit();
};

export default {
    createExportPath,
    calculateRITEForm,
    changeCells
}