function getOG(offerItems){
    return offerItems.filter((item) => {
        return item.IG_OG == "OG";
    })
};

function getIG(offerItems){
    return offerItems.filter((item) => {
        return item.IG_OG == "IG";
    })
};

function totalSP2(offerItems){
    let totalSP2 = 0;
    offerItems.forEach(element => {
        totalSP2 = totalSP2 + element.totalSP2;
    });

    return totalSP2.toFixed(2);
};

function COGS_IG(offerItems){
    let igItems = getIG(offerItems);
    let totalCostExcludingExpenses = 0;
    let totalUpstreamForIG = 0;

    offerItems.forEach(anItem => {
        totalCostExcludingExpenses = totalCostExcludingExpenses + anItem.totalCostExcludingExpenses;
    });

    igItems.forEach((anItem) => {
        totalUpstreamForIG = totalUpstreamForIG + anItem.Upstream_amount;
    });

    return (totalCostExcludingExpenses - totalUpstreamForIG).toFixed(2);
};

function COGS_OG(offerItems){
    let totalCostExcludingExpenses = 0;
    let ogItems = getOG(offerItems);

    ogItems.forEach(anItem => {
        totalCostExcludingExpenses = totalCostExcludingExpenses + anItem.totalCostExcludingExpenses;
    });

    return totalCostExcludingExpenses.toFixed(2);
};

function design_engineering_prmgmnt_IG(offerItems){
    let igItems = getIG(offerItems);
    let hoursValuesOnIg = 0;

    igItems.forEach((anItem) => {
        anItem.expenses.forEach((anExpense) => {
            if(anExpense.valueType == "Hours"){
                hoursValuesOnIg = hoursValuesOnIg + anExpense.value
            }
        })
    });

    return hoursValuesOnIg.toFixed(2);
};

function design_engineering_prmgmnt_OG(offerItems){
    let ogItems = getOG(offerItems);
    let hoursValuesOnOg = 0;

    ogItems.forEach((anItem) => {
        anItem.expenses.forEach((anExpense) => {
            if(anExpense.valueType == "Hours"){
                hoursValuesOnOg = hoursValuesOnOg + anExpense.value
            }
        })
    })

    return hoursValuesOnOg.toFixed(2);
};

function install_commission_service_IG(offerItems){
    let igItems = getIG(offerItems);
    let value = 0;

    let namesCompare = ["Site Testing Hours" , "Site Testing Expenses" , "Site Commissioning Hours" , "Site Commissioning Expenses"];
    igItems.forEach((anItem) => {
        anItem.expenses.forEach((anExpense) => {
            if(namesCompare.includes(anExpense.name)){
                value = value + anExpense.value;
            }
        })
    });

    return value.toFixed(2);
};

function install_commission_service_OG(offerItems) {
    let ogItems = getOG(offerItems);
    let value = 0;

    let namesCompare = ["Site Testing Hours", "Site Testing Expenses", "Site Commissioning Hours", "Site Commissioning Expenses"];
    ogItems.forEach((anItem) => {
        anItem.expenses.forEach((anExpense) => {
            if (namesCompare.includes(anExpense.name)) {
                value = value + anExpense.value;
            }
        })
    });

    return value.toFixed(2);
};

function other_COGS_IG(offerItems){
    let igItem = getIG(offerItems);
    let namesCompare = ["Site Testing Hours", "Site Testing Expenses", "Site Commissioning Hours", "Site Commissioning Expenses"];

    let value = 0;

    igItem.forEach((anItem) => {
        anItem.expenses.forEach((anExpense) => {
            if(!namesCompare.includes(anExpense) && anExpense.valueType != "Hours" ){
                value = value + anExpense.value;
            }
        })
    });

    return value.toFixed(2);
};

function other_COGS_OG(offerItems){
    let ogItem = getOG(offerItems);
    let namesCompare = ["Site Testing Hours", "Site Testing Expenses", "Site Commissioning Hours", "Site Commissioning Expenses"];

    let value = 0;

    ogItem.forEach((anItem) => {
        anItem.expenses.forEach((anExpense) => {
            if(!namesCompare.includes(anExpense) && anExpense.valueType != "Hours" ){
                value = value + anExpense.value;
            }
        })
    });

    return value.toFixed(2);
};

function warranty(offerItems){
    let warranty = 0;

    offerItems.forEach((anItem) => {
        anItem.expenses.forEach((anExpense) => {
            if(anExpense.name == "Warranty provision"){
                warranty = warranty + anExpense.value
            }
        })
    })

    return warranty.toFixed(2);
};

function risk(offerItems){
    let risk = 0;

    offerItems.forEach((anItem) => {
        anItem.expenses.forEach((anExpense) => {
            if(anExpense.name == "RAI (Risk Analysis Impact)" || anExpense.name == "Risk"){
                risk = risk + anExpense.value
            }
        })
    })

    return risk.toFixed(2);
};


export default {
    totalSP2,
    COGS_IG,
    COGS_OG,
    design_engineering_prmgmnt_IG,
    design_engineering_prmgmnt_OG,
    install_commission_service_IG,
    install_commission_service_OG,
    other_COGS_IG,
    other_COGS_OG,
    warranty,
    risk
}