import offerService from '../../services/offer.service';
import offer from '../../models/offer/offer';

import allExpensesReport from './all-expenses-report/all-expenses-report';
import detailedSummaryReport from './detailed-summary-report/detailed-summary-report'
import generalExpensesSummary from './general-expenses-summary/general-expenses-summary'
import familySummary from './family-summary/family-summary'


//const userServiceInstance = new userService(new user().constructModel());
const offerServiceInstance = new offerService(new offer().constructModel());


export default () => {
    return Object.freeze({
        allExpensesReport: new allExpensesReport(),
        detailedSummaryReport: new detailedSummaryReport(offerServiceInstance),
        generalExpensesSummary: new generalExpensesSummary(offerServiceInstance),
        familySummary: new familySummary(),
    })
};
