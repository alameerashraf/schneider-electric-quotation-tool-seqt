import fs from 'fs'
import PDFDocument from 'pdfkit'
import errors from '../../../common/constants/error-codes'
import templates from '../../../common/constants/report-templates'

function makeBaseDir (userSESA) {
    const date = new Date()
    const today = date.getDate() + '-' + (date.getMonth()+1) +'-' + date.getFullYear()
    const baseDir = 'exports/' + today +'/'+ userSESA
    if(!fs.existsSync(baseDir)){
        fs.mkdirSync(baseDir, {recursive: true})
    }
    return baseDir
}

function makeReport(reportMetaData, baseDir) {
    
    switch(reportMetaData.fileType){
        case("PDF"):
            return makePDF(reportMetaData, baseDir)
        case("excel"):
            break
        case("word"):
            break
        default: 
            return {
                error: true,
                code: errors.NOT_SUPPORTED_FILE_TYPE,
                message: "not supported file type"
            }
    }
    
}

function makePDF({reportName, entity, template}, baseDir){
    let doc = new PDFDocument({ margin: 50 });
    templates.allExpensesReportPDF({reportName: reportName}, doc)
    doc.pipe(fs.createWriteStream(baseDir +'/' + reportName + '.pdf'));
    doc.end()
    return baseDir +'/' + reportName + '.pdf'
}

export default {
    makeBaseDir,
    makeReport
}