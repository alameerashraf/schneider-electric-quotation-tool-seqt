import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import methods from './methods'
import fs from 'fs'


class allExpensesReport {

    async execute(reportMetaData, user) {
        const baseDir = methods.makeBaseDir(user.sesa)
        //TODO: construct report /using mongoose service and models \OR\ using other endpoint apps
        const reportLink = methods.makeReport(reportMetaData, baseDir)

        return {
            error: false,
            data: [reportLink]
        }
       
    }
};

export default allExpensesReport;