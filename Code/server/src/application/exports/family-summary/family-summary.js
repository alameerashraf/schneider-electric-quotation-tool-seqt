import loggers from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import methods from './methods'



class familySummary {

    // constructor(offerService){
    //     this.service = offerService;
    // };

    async execute(reportMetaData, user) {
        if(user){
            const baseDir = methods.makeBaseDir(user)
            const reportData = methods.constructReportData()
            const result = methods.makeXLSXReport(reportMetaData.reportName, baseDir, reportData)
            
            if (result.error) {
                return {
                    error: true,
                    code: result.code ,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: [result.data]
                }
            }
        }else 
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: user.message || ""
            }
    }
};

export default familySummary;