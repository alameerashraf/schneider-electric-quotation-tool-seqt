import fs from 'fs'
import errors from '../../../common/constants/error-codes'
import headers from '../../../common/constants/reports-headers'
import styles from '../stylesXLSX'
import xlsx from 'excel4node'

function makeBaseDir (userSESA) {
    const date = new Date()
    const today = date.getDate() + '-' + (date.getMonth()+1) +'-' + date.getFullYear()
    const baseDir = 'exports/' + today +'/'+ userSESA
    if(!fs.existsSync(baseDir)){
        fs.mkdirSync(baseDir, {recursive: true})
    }
    return baseDir
}

// return array of json Objects (rows)
function constructReportData() {
    let jsonRows = [
        {
            family:"family 1",
            salesFamily: "sales family 1",
            totalCostExcludingExpenses: 100 ,
            totalCostIncludingExpenses: 1000,
            totalSP: 10,
            localMargin: 10,
            SP_CCO_GM: 10,
            thresholdCCO_GM: 10, 
        },
        {
            family:"family 2",
            salesFamily: "sales family 2",
            totalCostExcludingExpenses: 200 ,
            totalCostIncludingExpenses: 2000,
            totalSP: 20,
            localMargin: 20,
            SP_CCO_GM: 20,
            thresholdCCO_GM: 20, 
        },
        {
            family:"family 3",
            salesFamily: "sales family 3",
            totalCostExcludingExpenses: 300 ,
            totalCostIncludingExpenses: 3000,
            totalSP: 30,
            localMargin: 30,
            SP_CCO_GM: 30,
            thresholdCCO_GM: 30, 
        },
        {
            family:"family 4",
            salesFamily: "sales family 4",
            totalCostExcludingExpenses: 400 ,
            totalCostIncludingExpenses: 4000,
            totalSP: 40,
            localMargin: 40,
            SP_CCO_GM: 40,
            thresholdCCO_GM: 40, 
        },
    ]
    return jsonRows
}

function makeXLSXReport(reportName, baseDir, data){

    try{
        const workbook = new xlsx.Workbook()
        const worksheet = workbook.addWorksheet(reportName);
        //fill the sheet with data
        fillSheet(headers.familySummary, data, worksheet)
        workbook.write(baseDir + '/' + reportName + '.xlsx');
        return {
            error: false,
            data:(baseDir + '/' + reportName + '.xlsx').replace("exports/" , '/')
        }
    }catch(error){
        return {
            error: true,
            code: errors.ERROR_MAKING_FILE,
            message: error.toString()
        }
    }
    
}

//fill excel sheet with headers and data
const fillSheet = (headers, data, sheet) => {
    // // adding the Project name at top
    // sheet.cell(1, 1, 1, 8, true)
    //     .style(styles.headerStyle)
    //     .string("General Expenses")
    // filling in data through the sheet 
    const headerRow = 2
    const footerRow = headerRow + data.length + 1
    const numOfColumns = (Object.keys(headers)).length
    let i = 1
    for (let key of Object.keys(headers)){
        sheet.cell(headerRow,i).string(headers[key].columnName)
        i++
    }
    for (i=0; i < data.length; i++){
        let j = 1
        let currentRow = i + headerRow + 1
        for(let [key, value] of Object.entries(headers)){
            if(data[i][key])
                sheet.cell(currentRow, j)[value.type](data[i][key])
            j++
        }
    }
    //adding formulas
    sheet.cell(footerRow, 1, footerRow, 2, true)
        .string('Grand Total: ')
        .style(styles.headerStyle)
    sheet.cell(footerRow, 3).formula(`SUM((C${headerRow + 1}):C${footerRow - 1})`)
    sheet.cell(footerRow, 4).formula(`SUM((D${headerRow + 1}):D${footerRow - 1})`)
    sheet.cell(footerRow, 5).formula(`SUM((E${headerRow + 1}):E${footerRow - 1})`)
    
    // finishing styling
    sheet.cell(headerRow, 1, headerRow, numOfColumns).style(styles.headerStyle)
    sheet.cell(headerRow+1, 1, footerRow - 1, numOfColumns).style(styles.cellStyle)
    sheet.cell(footerRow, 1, footerRow, numOfColumns).style(styles.footerStyle)
}

export default {
    makeBaseDir,
    constructReportData,
    makeXLSXReport,
}