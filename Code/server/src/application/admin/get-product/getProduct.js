import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class getProduct{

    constructor(familyService) {
        this.service = familyService;
    }

    async execute(family) { 
        try {
            let result = await this.service.read(family);
            if (result.error) {
                return {
                    error: true,
                    code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                    message: result.message 

                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (error) {
            return {
                error: true,
                code: errors.ERROR_LOADING_DATA_FROM_DATABASE,
                message: exception
            }
        }

    }
};

export default getProduct;