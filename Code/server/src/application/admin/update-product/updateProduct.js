import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import deleteProduct from '../delete-product/deleteProduct';

class updateProduct{

    constructor(familyService) {
        this.service = familyService;
    }

    async execute(familyName, productId, product) {
        try {
            let productInstance = await this.service.read({family: familyName});
            productInstance = productInstance.data.products;
            let index = productInstance.findIndex(p => p._id == productId);
            productInstance[index] = product;

            let result = await this.service.updateByQuery(
                {family: familyName},
                {products : productInstance}
            )
            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }

    }
};

export default updateProduct;