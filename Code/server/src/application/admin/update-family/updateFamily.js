import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class updateFamily {

    constructor(familyService) {
        this.service = familyService;
    }

    async execute(familyName, family) {
       
        try {
            let result = await this.service.updateByQuery(
                {
                  family: familyName  
                }, family
                );

            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                message: exception
            }
        }

    }
};

export default updateFamily;