import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class createProduct { 
    constructor(familyService){
        this.service = familyService;
    }

    async execute(familyName,product) {
        try {
            let result = await this.service.updateByQuery({
                family: familyName, }, {
                $push: { products: product },
              }
            );
            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message 
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};


export default createProduct;