import path from "path";
import fs from "fs";
import xlsx from "node-xlsx";
import errors from "../../../common/constants/error-codes";


class bulkCreateFamilies {
    constructor(familyService) {
        this.service = familyService;
    }

    async execute() {
        try{
            let familiesPath = path.join(
                __dirname,
                "../../../../uploads/Families/_last.xlsx");
            
            const familiesFile = xlsx.parse(fs.readFileSync(familiesPath));
            let familiesData = [];
            let arrayOfFamilies = familiesFile[0].data;
    
            let singleFamily = {};
    
            for(let index = 2; index < arrayOfFamilies.length; index++) {
                const element = arrayOfFamilies[index];
                if(element[0] != undefined){
                    if (Object.keys(singleFamily).length !== 0) {
                        familiesData.push(singleFamily);
                        singleFamily = {};
                    }
    
                    singleFamily["family"] = element[0];
                    singleFamily["products"] = [];
    
    
                    singleFamily["products"].push({
                        description: element[1],
                        name: element[2],
                        cco_gm: element[3],
                        upstream: element[4]
                    });
    
                } else {
                    singleFamily["products"].push({
                        description: element[1],
                        name: element[2],
                        cco_gm: element[3],
                        upstream: element[4]
                    });     
                }
            };

            let deletionResult = await this.service.clear();

            if(!deletionResult.error){
                let result = await this.service.create(familiesData);

                if (result.error) {
                    return {
                        error: true,
                        code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                        message: result.message
    
                    }
                } else {
                    return {
                        error: false,
                        data: result.data
                    }
                }
            }

        } catch(exception){
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: error
            }
        }
    }
};

export default bulkCreateFamilies;