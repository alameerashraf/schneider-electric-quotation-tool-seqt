import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class deleteProduct{

    constructor (familyService)
    {
        this.service = familyService;
    }

    async execute(familyName, productId)
    {
        try {
            let result = await this.service.updateByQuery(
                {family : familyName},
                {$pull: {"products": {"_id": productId}}}
            );
            if (!result) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                    message: result.message

                }
            } else {
                return {
                    error: false,
                }
            }
        } catch (error) {
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                message: exception
            }
        }
    
    }
};

export default deleteProduct;