import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class updateUser{

    constructor(userService)
    {
        this.service = userService;
    }

    async execute(userSesa, user)
    {

        try {
            let result = await this.service.updateByQuery(
                { sesa: userSesa }, 
                user
            );


            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }  
};

export default updateUser;