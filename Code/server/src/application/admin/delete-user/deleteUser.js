import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import errorCodes from '../../../common/constants/error-codes';
import mongoose from 'mongoose'

class deleteUser {

    constructor(userService) {
        this.service = userService;
    }

    async execute(usersSesas) {

        try {
            for (var i = 0; i < usersSesas.length; i++) 
            {

                let userInstance = await this.service.read({ sesa: usersSesas[i] });
                userInstance = userInstance.data.toObject();
                let userId = userInstance._id;
                let subOrdinates = userInstance.subOrdinates;
                let manager = userInstance.manager;

                let deleteInstance = await this.service.deleteByQuery(
                    { sesa: usersSesas[i] }
                );
                if (deleteInstance.error) {
                    return {
                        error: true,
                        code: errors.DATA_NOT_FOUND,
                        message: deleteInstance.message

                    }
                } else {

                    if (userInstance.hasOwnProperty('manager') == true) {

                        let updateManagerInstance = await this.service.updateByQuery(
                            { _id: manager },
                            { $pull: { subOrdinates: userId } }
                        );

                        if (updateManagerInstance.error) {
                            return {
                                error: true,
                                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                                message: updateManagerInstance.message
                            }
                        }

                    }

                    if (subOrdinates.length != 0) {

                        for (var j = 0; j < subOrdinates.length; j++) {
                            let updatesubOrdinatesInstance = await this.service.updateByQuery(
                                { _id: subOrdinates[j] },
                                { $unset: { manager: "" } }
                            );

                            if (updatesubOrdinatesInstance.error) {
                                return {
                                    error: true,
                                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                                    message: updatesubOrdinatesInstance.message
                                }
                            }
                        }
                    }

                }
            }

            return {
                error: false
            }

        } catch (error) {
            return {
                error: true,
                code: errors.DATA_NOT_FOUND,
                message: error
            }
        }
    }

};

export default deleteUser;