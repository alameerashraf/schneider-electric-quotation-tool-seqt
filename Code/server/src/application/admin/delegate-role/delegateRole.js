import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';


class delegateRole {

    constructor(userService) {
        this.service = userService;
    }


    async execute(fromUserSesa, toUserSesa, date) {
        try {
            let fromUserRoles = await this.service.loadUserRoles({ sesa: fromUserSesa });
            fromUserRoles = fromUserRoles.data;

            let toUserRoles = await this.service.loadUserRoles({ sesa: toUserSesa });
            toUserRoles = toUserRoles.data;
            
            let UpdatedRoles = toUserRoles;

            let checkRole = false;
            for (var i = 0; i < fromUserRoles.length; i++) {
                checkRole = toUserRoles.includes(fromUserRoles[i]);

                if (checkRole == false) {
                    UpdatedRoles.push(fromUserRoles[i]);
                }
            }

            let fromUserObject = { "isActive": false }
            let fromUserResult = await this.service.updateByQuery({
                sesa: fromUserSesa
            },
                fromUserObject
            );

            let toUserObject = {
                "onBehalfOf": fromUserSesa,
                "roles": UpdatedRoles
            }
            let toUserResult = await this.service.updateByQuery({
                sesa: toUserSesa
            },
                toUserObject
            );

            if (toUserObject.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: toUserObject.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }

};

export default delegateRole;