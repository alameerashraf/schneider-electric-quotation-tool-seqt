import userService from '../../services/user.service';
import user from '../../models/user/user';

import familyService from '../../services/family.service';
import family from '../../models/family/family';

import loggerService from '../../services/logger.service';
import logger from '../../models/logger/logger';

import getUsersData from './get-users-data/getUsersData';
import updateUser from './update-user/updateUser';
import deleteUser from './delete-user/deleteUser';
import delegateRole from './delegate-role/delegateRole';
import bulkCreateFamilies from './bulk-create-families/bulkCreateFamilies';
import createFamily from './create-family/createFamily'
import listFamilies from './list-families/listFamilies';
import updateFamily from './update-family/updateFamily'
import deleteFamily from './delete-family/deleteFamily'
import createProduct from './create-product/createProduct'
import getProduct from './get-product/getProduct'
import deleteProduct from './delete-product/deleteProduct'
import updateProduct from './update-product/updateProduct'
import revokeRole from './revoke-role/revokeRole'
import assignRole from './assign-role/assignRole'
import logAction from './log-action/logAction';
import listLogs from './list-logs/list-logs';

const userServiceInstance = new userService(new user().constructModel());
const familyServiceInstance = new familyService(new family().constructModel());
const loggerServiceInstance = new loggerService(new logger().constructModel());

export default () => {
    return Object.freeze({
        getUsersData: new getUsersData(userServiceInstance),
        updateUser: new updateUser(userServiceInstance),
        deleteUser: new deleteUser(userServiceInstance),
        delegateRole: new delegateRole(userServiceInstance),
        bulkCreateFamilies: new bulkCreateFamilies(familyServiceInstance),
        createFamily: new createFamily(familyServiceInstance),
        listFamilies: new listFamilies(familyServiceInstance),
        updateFamily: new updateFamily(familyServiceInstance),
        deleteFamily: new deleteFamily(familyServiceInstance),
        createProduct: new createProduct(familyServiceInstance),
        getProduct: new getProduct(familyServiceInstance),
        deleteProduct: new deleteProduct(familyServiceInstance),
        updateProduct: new updateProduct(familyServiceInstance),
        revokeRole: new revokeRole(userServiceInstance),
        assignRole: new assignRole(userServiceInstance),
        logAction: new logAction(loggerServiceInstance),
        listLogs: new listLogs(loggerServiceInstance)
    })
};
