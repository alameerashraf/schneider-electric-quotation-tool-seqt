import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class createFamily { 
    constructor(familyService){
        this.service = familyService;
    }

    async execute(family) {
        try {
            let result = await this.service.create(family);
            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                    message: result.message 
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: exception
            }
        }
    }
};


export default createFamily;