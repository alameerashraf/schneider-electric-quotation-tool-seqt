import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';
import user from '../../../models/user/user';


class revokeRole {

    constructor(userService) {
        this.service = userService;
    }

    async execute(userSesa, roleId) {
        try {
            let result = this.service.updateByQuery(
                { sesa: userSesa },
                { $pull: { roles: roleId } }
            );
            if (!result) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                    message: result.message

                }
            } else {
                return {
                    error: false,
                }
            }
        } catch (error) {
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                message: exception
            }
        }
    }
};

export default revokeRole;