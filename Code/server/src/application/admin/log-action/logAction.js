import { logger } from "handlebars";
import errors from '../../../common/constants/error-codes';

class logAction {
  constructor(loggerService) {
    this.service = loggerService;
  }

  async execute(logger) {
    try {
        let result = await this.service.create(logger);
        if (result.error) {
            return {
                error: true,
                code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
                message: result.message 
            }
        } else {
            return {
                error: false,
            }
        }
    } catch (exception) {
        return {
            error: true,
            code: errors.ENABLE_TO_SAVE_RECORD_TO_DATA_BASE,
            message: exception
        }
    }
  }
}

export default logAction;
