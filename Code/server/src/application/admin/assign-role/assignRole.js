import { loggers } from '../../../loaders/logger';
import errors from '../../../common/constants/error-codes';

class assignRole{

    constructor(userService)
    {
        this.service = userService;
    }

    async execute(userSesa, roleId)
    {
        try {
            let result = await this.service.updateByQuery(
                {sesa: userSesa},
                {$push : {roles : roleId}}
            );
            if (result.error) {
                return {
                    error: true,
                    code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                    message: result.message
                }
            } else {
                return {
                    error: false,
                    data: result.data
                }
            }
        } catch (exception) {
            return {
                error: true,
                code: errors.ENABLE_TO_UPDATE_RECORD_IN_DATA_BASE,
                message: exception
            }
        }
    }
};

export default assignRole;