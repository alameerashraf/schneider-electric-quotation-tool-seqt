import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import globalTunnel from 'global-tunnel-ng';
import fileUpload from 'express-fileupload';
import middlewares from './middleware';

// API routes
import * as routes from '../api';


// const app = express(); // for itelisense only.. 

export default ({ app })=> {
    var env = process.env.NODE_ENV;
    /**
     * Enable cors on all actions
     */
    app.use(cors());

    /** 
     * Authorization request 
     */

    app.use((req, res, next) => {
        middlewares.userAuthorization(req, res, next)
    });

    /**
     * Enable file upload
     */
    app.use(fileUpload({
        createParentPath: true
    }));
    

    /**
     * Transform string to JSON.
     */
    app.use(bodyParser.json({limit: '50mb'}));

    /**
     * If we behind a reverse proxy
     */
    app.enable('trust proxy');

    /**
     * SERVERS 
     */

    app.use(process.env.ROUTING_PREFIX , routes.default);

    /**
     * Check API health.
     */
    app.get(`${process.env.ROUTING_PREFIX}status`, (req, res) => {
        res.status(200).send("SEQT IS UP AND RUNNING!");
    });

    /**
     * Get API ENV.
     */
    app.get(`${process.env.ROUTING_PREFIX}env`, (req, res) => {
        res.status(200).json({
            env: process.env.NAME,
            version: '1.0'
        });
    });

    // /**
    //  * Proxy information handler 
    //  */
    // if (env == "dev") {
    //   globalTunnel.initialize({
    //     host: process.env.PROXY,
    //     port: 80
    //   });
    // }

    /**
     * Serving static files 
     */

    // app.use(express.static(__dirname + 'src/common'));

    /**
     * Catch 404 and forward to error handle.
     */
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err['status'] = 404;
        next(err);
    });

    /**
     * Global error catcher.
     */
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.json({
            errors:{
                message: err.message
            }
        });
    });

};


