import userApp from '../application/user/user.app';
import { ObjectId } from 'mongodb';

const {
    MongoClient
} = require('mongodb');

//#region UserAuthorizationMiddleware
function userAuthorization(req, res, next) {
    let access_token = req.headers.access_token;
    if (access_token != "") {
        MongoClient.connect('mongodb://localhost:27017', function (err, db) {
            if (err) res.status(500);
            var seqtdb = db.db("seqtdb");

            seqtdb.collection("AccessTokens").findOne({ Token: access_token}, (err , token) => {
                validateToken(err , token , res)
            });
        });

        next();
    } else {
        res.status(403).send("Unauthorized user")
    };

};

function validateToken(err, token, res) {
    if (err)
        res.status(500);
    else {
        if (token == null) {
            res.status(403);
        } else {
            if (new Date() > new Date(token.ExpiresAt)) {
                res.status(403);
            }
        }
    }
};

//#endregion

async function validateRole (seqtdb, token, res, roleName, next){
    if (token == null) {
        res.status(403);
    } else {
        //check the expiry of the access token
        if (new Date() > new Date(token.ExpiresAt)) {
            return res.status(403);
        }
        //check if the role given is in the roles array of the user
        const user = await seqtdb.collection("users").findOne({sesa: token.SESA})
        const role = await seqtdb.collection("roles").findOne({name: roleName})
        
        for(const roleID of user.roles){
            if (roleID.toString() == role._id.toString()){
                next();
            }   
        }
        return res.status(403);
    }
    
}
async function isTenderingEngineer(req, res, next) {
    let access_token = req.headers.access_token;
    if (access_token != "") {
        try{
            const client = await MongoClient.connect('mongodb://localhost:27017');
            var seqtdb = client.db("seqtdb");
            const token = await seqtdb.collection("AccessTokens").findOne({ Token: access_token})
            validateRole(seqtdb, token, res, "TenderingManager", next)
        }catch(err){
            res.status(500);
        }
    } else {
        res.status(403).send("Unauthorized user")
    };
};
async function isTenderingManager(req, res, next) {
    let access_token = req.headers.access_token;
    if (access_token != "") {
        try{
            const client = await MongoClient.connect('mongodb://localhost:27017');
            var seqtdb = client.db("seqtdb");
            const token = await seqtdb.collection("AccessTokens").findOne({ Token: access_token})
            validateRole(seqtdb, token, res, "TenderingManager", next)
        }catch(err){
            res.status(500);
        }
       
    } else {
        res.status(403).send("Unauthorized user")
    };
};

const middlewares = {
    userAuthorization,
    isTenderingEngineer,
    isTenderingManager,
};

export default middlewares;