import Service from './Service';
import loggers from '../loaders/logger';

class familyService extends Service {
    constructor(model) {
        super(model);
    };

    // Romves all data from collection 
    async clear() {
        try {
            let item = await this.model
            .remove()

            return {
                error: false,
                statusCode: 200,
                data: item,
            }
        } catch (error) {
            return {
                error: exception,
                statusCode: 500,
                exception
            }
        }
    };

    async loadAllFamilies()
    {
        try {
            let items = await this.model.find({});     
            return {
                error: false,
                statusCode: 200,
                data: items
            }
        } catch (error) {
            return{
                error: true,
                statusCode: 500,
                message: error
            }
        }
    };
}

export default familyService;