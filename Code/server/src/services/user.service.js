import Service from "./Service";

class userService extends Service {
    constructor(model){
        super(model);
    }


    // special functions 
    async loadAll_UserDataBySESA(query){
        let {
            skip,
            limit
        } = query;

        // delete from DB query!
        skip = skip ? Number(skip) : 0;
        limit = limit ? Number(limit) : 10;

        delete query.skip;
        delete query.limit;

        try {
            let items = await this.model
                .findOne(query)
                .skip(skip)
                .limit(limit)
                .populate({ path: "manager" , select : ["name" , "email" , "sesa" , "_id"] })
                .populate({ path: "subOrdinates" , select : ["name" , "email" , "sesa" , "_id"] })
                .populate({ path: 'offers' , select : [
                    'offerNumber',
                    'seRefrence',
                    'currentStatus',
                    'createdBy',
                    'assignedTo',
                    'createdAt',
                    'currencyCode',
                    'opportunityName',
                    'ownerName'
                ]})
                .populate({
                    path: 'roles',
                    model: 'role',
                    populate: {
                        path: 'subjects',
                        model: 'subject'           
                    }
                })
            return {
                error: false,
                statusCode: 200,
                data: items,
            }
        } catch (exception) {
            return {
                error: exception,
                statusCode: 500,
                exception
            }
        }
    };

    async loadAllUsers(query){
        try {
            let items = await this.model
                .find(query).select({ "name": 1, "roles" : 1, "sesa" : 1 , "email" : 1 , "_id": 1})
                .populate({
                    path: 'roles',
                    model: 'role'
                })

            return {
                error: false,
                statusCode: 200,
                data: items,
            }
        } catch (exception) {
            return {
                error: exception,
                statusCode: 500,
                message: exception
            }
        }
    };

    async loadUsersData(query)
    {
        try {
            let items = await this.model
            .find(query, { _id: 0, name: 1, sesa : 1, department : 1, email : 1, isActive : 1, manager:1, onBehalfOf:1})
            .populate({ path: "manager" , select : ["name" , "email" , "sesa"] })
            .populate({ path: 'offers' , select : [
                'offerNumber',
                'seRefrence',
                'currentStatus',
                'createdBy',
                'assignedTo',
                'createdAt',
                'currencyCode',
                'opportunityName',
                'ownerName'
            ]})
            .populate({
                path: 'roles',
                model: 'role',
                populate: {
                    path: 'subjects',
                    model: 'subject'           
                }
            })
            return {
                error: false,
                statusCode: 200,
                data: items
            }
        } catch (error) {
            return{
                error: true,
                statusCode: 500,
                message: error
            }
        }
    };

    async loadUserRoles(query)
    {
        try {
            let user = await this.model
                .findOne(query, { _id: 0, roles: 1});


            
            return {
                error: false,
                statusCode: 200,
                data: user.roles
            }
        } catch (error) {
            return{
                error: true,
                statusCode: 500,
                message: error
            }
        }
    };
}

export default userService;