import Service from './Service';
import loggers from '../loaders/logger';

class fileTrackerService extends Service
{
    constructor(model)
    {
        super(model);
    }

    async loadAllFiles()
    {
        try {
            let items = await this.model.find({});     
            return {
                error: false,
                statusCode: 200,
                data: items
            }
        } catch (exception) {
            return{
                error: true,
                statusCode: 500,
                message: exception
            }
        }
    };

};

export default fileTrackerService;
