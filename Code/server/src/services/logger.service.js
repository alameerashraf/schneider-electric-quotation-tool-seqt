import Service from './Service';
import loggers from '../loaders/logger';

class loggerService extends Service {
    constructor(model) {
        super(model);
    };

    async loadAllLogs(query)
    {
        try {
            let items = await this.model.find(query);     
            return {
                error: false,
                statusCode: 200,
                data: items
            }
        } catch (error) {
            return{
                error: true,
                statusCode: 500,
                message: error
            }
        }
    };
}

export default loggerService;