import Service from './Service';
import loggers from '../loaders/logger';

class roleService extends Service {
    constructor(model) {
        super(model);
    };


    async loadAllRoles()
    {
        try {
            let items = await this.model.find({});     
            return {
                error: false,
                statusCode: 200,
                data: items
            }
        } catch (error) {
            return{
                error: true,
                statusCode: 500,
                message: error
            }
        }
    };
}

export default roleService;