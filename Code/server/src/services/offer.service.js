import Service from './Service';
import loggers from '../loaders/logger';

class offerService extends Service {
    constructor(model) {
        super(model);
    };

    // Implement any special functions here..!
    async getLatest(query) {
        try {
            let item = await this.model
                .find(query)
                .limit(1)
                .sort({
                    $natural: -1
                });

            return {
                error: false,
                statusCode: 200,
                data: item,
            }
        } catch (error) {
            return {
                error: exception,
                statusCode: 500,
                exception
            }
        }
    };
}

export default offerService;