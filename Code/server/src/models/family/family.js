import mongoose, { Schema } from 'mongoose';

import product from './product';


class family {
    constructor(){
    }
    
    initSchema(){
        const schema = new Schema({
            family: { type: String , required: true },
            products : {
                type: [new product().initSchema()]
            }
        }, { timestamps: true });

        return mongoose.models.family || mongoose.model('family' , schema); 
    }

    constructModel(){
        return this.initSchema();
    }

}


export default family;