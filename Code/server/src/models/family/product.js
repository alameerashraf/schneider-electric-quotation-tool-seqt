import mongoose, { Schema } from "mongoose";

class product {
    constructor(){

    }

    initSchema(){
        var schema = new Schema({
            name: { type: String , required: true },
            description: { type: String , required: true },
            cco_gm: { type: Number , required: true },
            upstream: { type: Number , required: true }
        });

        return schema;
    }

    constructModel(){
        this.initSchema();
    }
};


export default product;