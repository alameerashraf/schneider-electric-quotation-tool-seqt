import mongoose, { Schema } from 'mongoose';
import validators from './validators/user-validators';

import role from './role';


class user{
    constructor(){
        this._validators = new validators();
        new role().constructModel();
    }
    
    initSchema(){
        const schema = new Schema({
            name: { type: String },
            sesa: { type: String , required: true,
                validate: {
                    validator: function(v){
                        new validators().userSesa(v)
                    },
                    message: "User Sesa is not valid"
                }
            },
            email: { type: String, required: true,
                validate: {
                    validator: function(v){
                        new validators().userEmail(v)
                    },
                    message: "User Email is not valid"
                } },
            department: { type: String, required: false },
            roles: {  
                type: [{ type: mongoose.Schema.Types.ObjectId , ref : 'role' }] , 
                required : false 
            },
            isActive: { type : Boolean , default: true },
            subOrdinates : {
                type: [{ type: mongoose.Schema.Types.ObjectId , ref : 'user' }],
                required : false
            },
            manager: { type: mongoose.Schema.Types.ObjectId , ref : 'user' ,  required : false },
            offers: { 
                type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] , 
                validate : {
                    validator : function(v){
                        new validators().assignedOfferId(v);
                    },
                    message: "Offer assigned to user is not found!"
                }
            },
            onBehalfOf:{
                type : String, required:false
            },
            orders: { type : [{  
                order : { type: mongoose.Schema.Types.ObjectId , ref : 'order'} ,
                status : { type: String }
            }]},
            settings: {
                avatar: { type: String , required : false }
            },
            kpis : {
                employee : {
                    IN_PROGRESS : { type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}]},
                    WAITING_FOR_APPROVAL : { type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                    APPROVED : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                    WON : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                    CANCELED : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                }, 
                manager: {
                    IN_PROGRESS : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                    PENDING_APPROVAL : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}]},
                    APPROVED : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                    WON : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                    CANCELED : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                    DRAFT : {  type : [{ type: mongoose.Schema.Types.ObjectId , ref : 'offer'}] },
                }
            }
        });

        return mongoose.models.user || mongoose.model('user' , schema); 
    }

    constructModel(){
        return this.initSchema();
    }

}


export default user;