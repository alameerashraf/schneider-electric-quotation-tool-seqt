function isAssignedOfferIdNull(offerId){
    if(offerId == null){
        return false;
    }
};

function isUserSesaNull(userSesa)
{
    if(userSesa == null)
    {
        return false;
    }
}

function checkUserSesaFormat(userSesa)
{
    if (/^(sesa||SESA||Sesa)+([0-9]+)$/.test(userSesa) == false)
    {
        return false;
    }
}

function checkUserEmailFormat(email)
{
    if(/^[a-z,A-Z]+([-.][a-z,A-Z]+)*@?(se.com|non.se.com|schneider-electric.com|non.schneider-electric.com)$/
    .test(email.toLowerCase()) == false)
    {
        return false;
    }
}

const validators = {
    isAssignedOfferIdNull,
    isUserSesaNull,
    checkUserSesaFormat,
    checkUserEmailFormat
};

export default validators;