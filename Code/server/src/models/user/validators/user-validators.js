import validators from './validators';

class userValidators {
    assignedOfferId(offerId){
        let isAssignedOfferIdNull = validators.isAssignedOfferIdNull(offerId);

        return isAssignedOfferIdNull;
    }


    userSesa(userSesa)
    {
        let isUserSesaNull = validators.isUserSesaNull(userSesa);
        let checkUserSesaFormat = validators.checkUserSesaFormat(userSesa);


        return isUserSesaNull && checkUserSesaFormat;

    }

    userEmail(email)
    {
        let checkUserEmailFormat = validators.checkUserEmailFormat(email);

        return checkUserEmailFormat;
    }

};



export default userValidators;