import mongoose, { Schema } from 'mongoose';
import subject from './subject';

class role{
    constructor(){
        new subject().constructModel();
    }
    
    initSchema(){
        const schema = new Schema({
            name : { type: String , required: true },
            shortcut : { type : String },
            displayName: { type: String },
            priority : { type: Number , default : 0 },
            subjects : { type : [{ type: mongoose.Schema.Types.ObjectId , ref: 'subject' }] }
        });

        return mongoose.models.role || mongoose.model('role' , schema); 
    }

    constructModel(){
        return this.initSchema();
    }

}

export default role;