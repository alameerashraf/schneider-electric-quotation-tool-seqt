import mongoose, { Schema } from 'mongoose';

class subject{

    initSchema(){
        const schema = new Schema({
            name : { type: String , required: true },
            actions : { type : [String] }
        });

        return mongoose.models.subject || mongoose.model('subject' , schema); 
    }

    constructModel(){
        return this.initSchema();
    }

};

export default subject;