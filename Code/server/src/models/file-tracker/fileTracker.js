import mongoose, { Schema } from 'mongoose';


class fileTracker {
    constructor(){
    }
    
    initSchema(){
        const schema = new Schema({
            offerNumber: {type: String , required: true},
            name: { type: String , required: true },
            path: { type: String , required: true },
            type: { type: String , enum: ["COST" , "TECHNICAL" , "RISK&OPPORTUNITY"] , required: true },
            uploadedBy: { type: String },
        }, { timestamps: true });

        return mongoose.models.fileTracker || mongoose.model('fileTracker' , schema); 
    }

    constructModel(){
        return this.initSchema();
    }

}

export default fileTracker;