import mongoose, { Schema } from 'mongoose';


class logger {
    constructor(){
    }
    
    initSchema(){
        const schema = new Schema({
            action: { type: String , enum: ['REVOKE' , 'GRANT' , 'CREATE' , 'UPDATE' , 'DELETE' , 'DELEGATE'] , required: true },
            entity: { type: String , enum: [ 'ROLE' , 'USER' , 'OFFER' , 'ITEM' , 'FAMILY' ], required: true },
            takenBy: { type: String },
            affectedUser: { type: String },
            actionText: { type: String },
            additionalInfo: { type: String , default: ""},
            additionalText: { type: String , default: ""},
        }, { timestamps: true });

        schema.pre("save", function (next) {
            let tempActionName = this.action;
            if(this.action.charAt(this.action.length - 1) == "E")
                tempActionName = this.action.slice(0, -1); 
            this.actionText = `${this.entity.toLocaleLowerCase()} ${this.additionalInfo} has been ${tempActionName.toLocaleLowerCase()}ed ${this.additionalText}`;
          next();
        });

        return mongoose.models.logger || mongoose.model('logger' , schema); 
    }

    constructModel(){
        return this.initSchema();
    }

}

export default logger;