import mongoose, { Schema } from "mongoose";

class expense {
    constructor(){

    }

    initSchema(){
        var schema = new Schema({
            type: { type: String , enum: ['GENERAL' , 'ITEM'] , required: true , default: 'GENERAL'},
            name: { type: String , required: true },
            valueType: { type: String , required: true },
            value: { type: Number, required: true }
        });

        return schema;
    }

    constructModel(){
        this.initSchema();
    }
};


export default expense;