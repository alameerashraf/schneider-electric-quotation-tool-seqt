function isOfferNumberStructureCorrect(offerNumber){
    if(offerNumber != ""){
        let currentOfferNumber = offerNumber.split('*')[1];
        if(isNaN(parseInt(currentOfferNumber))){
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
};

function isOfferNumberNull(offerNumber){
    if(offerNumber == null || typeof offerNumber == 'undefined'){
        return false;
    }
};

function isOfferTimeLineNull(offerTimeLine){
    return true;
};

let validators = {
    isOfferNumberStructureCorrect,
    isOfferNumberNull,
    isOfferTimeLineNull
};

export default validators;