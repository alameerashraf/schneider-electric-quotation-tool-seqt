import validators from './validators';


class offerValidators {
    offerNumber(offerNumber){
        let isOfferNumberNull = validators.isOfferNumberNull(offerNumber);
        let isOfferNumberStructureCorrect = validators.isOfferNumberStructureCorrect(offerNumber);
        return isOfferNumberNull && isOfferNumberStructureCorrect;
    };

    offerTimeLine(offerTimeLine){
        let isOfferTimeLineNull = validators.isOfferTimeLineNull(offerTimeLine);

        return isOfferTimeLineNull;
    };
};


export default offerValidators;