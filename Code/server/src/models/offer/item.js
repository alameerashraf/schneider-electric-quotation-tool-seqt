import mongoose, { Schema } from "mongoose";
import expenses from '../offer/expense';

class item {
    constructor(){

    }
 
    initSchema(){
        var schema = new Schema({
            itemNo: { type: Number},
            family: { type: String },
            product: { type: String },
            description: { type: String },
            quantity: { type: Number },
            unitCostExcludingExpenses: { type: Number },
            totalCostExcludingExpenses: { type: Number },
            unitCostIncludingExpenses: { type: Number},
            totalCostIncludingExpenses: { type: Number },
            expensesPerUnit: { type: Number },
            totalExpenses: { type: Number },
            Expenses_Percentage: { type: Number },
            unitSP2: { type: Number },
            totalSP2: { type: Number },
            CCOGM_Percentage: { type: Number },
            sellingLocalMargin: { type: Number },
            CCOGM_Percentage_Threshold: { type: Number },
            unitSP1: { type: Number },
            totalSP1: { type: Number },
            IG_OG: { type: String },
            numberOfCells : { type: Number },
            Upstream_Percentage: { type: Number },
            Upstream_amount: { type: Number },
            sellingCCOCM_Percentage: { type: Number },
            upstreamSFC_Percentage: { type: Number },
            upstreamSFC_amount: { type: Number },
            fOSFC_Percentage: {type : Number},
            fOSFC_amount: { type: Number },
            totalSFC_amount: { type: Number },
            totalSFC_Percentage : { type: Number },
            source: { type: String },
            package: { type: String },
            comment: { type: String },
            UPLOADED_FROM: { type: String},
            expenses: [new expenses().initSchema()]
        }, {_id: false});

        
        return schema;
    }

    constructModel(){
        return this.initSchema();
    }

}

export default item;