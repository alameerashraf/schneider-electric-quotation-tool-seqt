import mongoose, { Schema } from 'mongoose';
import items from '../offer/item';
import expenses from '../offer/expense';
import uniqueValidator from 'mongoose-unique-validator';
import validators from './validators/offer-validators';
import loggers from '../../loaders/logger';


class offer {
    constructor(){
        this._validators = new validators();
    }
    /**
     * Initialzie the DB scehma.
     */
    initSchema() {
        const schema = new Schema({
            offerNumber : { type: String , required: true , validate: {
                validator: function(v){
                    new validators().offerNumber(v)
                },
                message: "Offer number is not valid!"
            }},
            seRefrence: { type: String , required: true },
            currentStatus : { type: String , enum : ["CREATED" , "ASSIGNED" , "IN_PROGRESS" , "CONFIGURED" , "IN_APPROVAL_CYCLE" , "APPROVED" , "REJECTED" ] },
            opportunityName : { type: String },
            ownerName : { type: String },
            category : { type : String , required: true },
            leadingBusiness : { type: String , required : true },
            accountName : { type: String },
            endUserAccount: { type: String },
            countryOfDestination: { type: String },
            salesStage : { type : String , required: true },
            marketSegment : { type: String },
            marketSubSegment : { type: String },
            solutionCenter : { type : String },
            rfpReceivedOn : { type: Date },
            quoteSubmissionDate: { type: Date },
            issudQuoteDateToAccount: { type: Date },
            closedDate: { type: Date },
            amount: { type: Number , required: true},
            currencyCode : { type: String , required: true} ,
            classification: { type: String , required : true},
            classification_HUB: { type: String },
            quotationType1: { type: String },
            projectType: { type : String }, 
            paymentMethod: { type: String },
            incoTerms: { type: String },
            destination: { type: String },
            ig_og: { type: String },
            standardWarranty: { type: String },
            createdBy: { type: String },
            assignedTo: { type: String },
            paymentTerms: {
                advanced: { type : Number },
                dwgApproval: { type : Number },
                fat_cad: { type: Number },
                progress: { type: Number },
                delivery: { type : Number },
                sat: { type : Number },
                late1 : {
                    amount : { type : Number },
                    time: Number
                },
                late2 : {
                    amount : Number,
                    time : Number
                }, 
                late3: { 
                    amount: Number,
                    time : Number
                }
            },
            offerTimeline : {
                type: [
                    {
                        owner: { type: String },
                        newStatus : { type: String },
                        oldStatus : { type: String },
                        changedBy : { type: String },
                        changedAt : { type : Date ,  default: new Date() },
                        isLatest : { type : Boolean },
                        lastApproval: {type: String},
                    }
                ],
                validate: {
                    validator: function(v){
                        new validators().offerTimeLine(v)
                    },
                    message: "Offer TimeLine is not valid!"
                }
            },
            items : {
                type: [new items().initSchema()]
            },
            expenses : {
                type: [new expenses().initSchema()]
            }
        }, { timestamps: true });

        return mongoose.models.offer || mongoose.model('offer' , schema); 
        
    }


    /**
     * construct the model to be used by the service
     */
    constructModel() {
        return this.initSchema();
    }
}

export default offer;