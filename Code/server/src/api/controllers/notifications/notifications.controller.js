import notifications from '../../../application/notifications/notifications.app';
const notificationsActions = notifications();

async function notifyByEmail(req , res , next) {
    let notifications = req.body.notification;

    let result = await notificationsActions.notifyByEmail.execute(notifications);
};

function notifyInTool(req , res , next){

};

function notifyBoth(req , res, next){

};


const actions = {
    notifyByEmail,
    notifyInTool,
    notifyBoth
};


export default actions;