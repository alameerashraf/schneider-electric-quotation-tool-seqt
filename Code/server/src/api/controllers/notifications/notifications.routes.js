import { Router } from 'express';
import actions from './notifications.controller';

var router = Router();


router.route('/notify-by-email')
    .post(actions.notifyByEmail)


router.route('/notify-in-tool')
    .post()


// Notify by both
router.route('/notify')
    .post()

export default router;