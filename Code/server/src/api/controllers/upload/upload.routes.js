import {Router} from 'express';
import actions from './upload.controller';
import middlewares from '../../../loaders/middleware';



var router = Router();

router.route('/cost-files')
    .post(actions.uploadCostFiles);

router.route('/expenses-categories')
    .post(actions.uploadExpensesFileFromAdmin)

router.route('/families')
    .post(actions.uploadFamiliesMasterFileFromAdmin)

router.route('/exchange-rates')
    .post(actions.uploadExchangeRatesFromAdmin)

router.route('/create-file-tracker')
    .post(actions.createFileTracker)

router.route('/offer-documents/:offerNumber/:fileType')
    .post(actions.uploadOfferDocuments)

router.route('/list-files')
    .get(actions.listFiles)

router.route('/update-file-tracker/:offerNumber')
    .post(actions.updateFileTracker)

router.route('/delete-file-tracker/:offerNumber')
    .post(actions.deleteFileTracker)


export default router;