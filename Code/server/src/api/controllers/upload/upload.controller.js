import uploads from '../../../application/uploads/uploads.app'
import admin from '../../../application/admin/admin.app';


const uploadApps = uploads();
const adminApps = admin();


function uploadCostFiles(req, res, next) {
  // console.log(req.query.fileType);
  if (!req.files) {

  } else {
    // console.log(req.files)
    // let costFile = req.files.invoces
  }
};

async function uploadExpensesFileFromAdmin(req, res, next) {
  let file = req.files.expenses;
  let result = await uploadApps.uploadExpensesFile.execute(file);
  console.log(result)
  if (result.error) {
    res.json({
      code: result.code,
      message: "Error Uploading Files",
      error: result.error,
      data: false,
    });
  } else {
    res.json({
      error: false,
    });
  }
}

async function uploadFamiliesMasterFileFromAdmin(req, res, next) {
  let file = req.files.familyMaster;
  let result = await uploadApps.uploadFamilyMasterFile.execute(file);

  if (result.error) {
    res.json({
      code: result.code,
      message: '',
      error: result.error,
      data: false
    });
  } else {
    res.json({
      error: false,
    });
  }
};

async function uploadExchangeRatesFromAdmin(req, res, next) {
  let file = req.files.exchangeRate;
  let result = await uploadApps.uploadExchangeRateFile.execute(file);

  console.log(result)
  if (result.error) {
    res.json({
      code: result.code,
      message: '',
      error: result.error,
      data: false
    });
  } else {
    res.json({
      error: false,
    });
  }
}

async function createFileTracker(req, res, next) {

  let uploadedFile = req.body;
  let result = await uploadApps.createFileTracker.execute(uploadedFile);

  if (result.error) {
    res.json({
      code: result.code,
      message: result.message,
      error: result.error,
      data: false
    });
  } else {
    res.json({
      error: false,
      data: result.data
    });
  }
}

async function updateFileTracker(req, res, next) {
  let offerNumber = req.params.offerNumber;
  let uploadedFile = req.body;
  let result = await uploadApps.updateFileTracker.execute(offerNumber, uploadedFile);
  if (result.error) {
      res.json({
          code: result.code,
          message: result.message,
          error: result.error,
          data: false
      });
  } else {
      res.json({
          error: false,
          data: result.data
      });
  }
}

async function listFiles(req, res, next) {
  let result = await uploadApps.listFiles.execute();
  if (result.error) {
      res.json({
          code: result.code,
          message: result.message,
          error: result.error,
          data: false
      });
  } else {
      res.json({
          error: false,
          data: result.data
      });
  }
}

async function deleteFileTracker(req, res, next) {
  let offerNumber = req.params.offerNumber;
  let result = await uploadApps.deleteFileTracker.execute(offerNumber);
  if (result.error) {
      res.json({
          code: result.code,
          message: result.message,
          error: result.error,
          data: false
      });
  } else {
      res.json({
          error: false
      });
  }

}

async function uploadOfferDocuments(req , res , next){
    let files = req.files.file;
    let offerNumber = req.params.offerNumber.replace("*" , "$");
    let fileType = req.params.fileType;
  
    if(!Array.isArray(files)){
      let arrayOfFiles = [];
      arrayOfFiles.push(files)
      files = arrayOfFiles;
    }
  
    let result = {};
    files.forEach(async(file) => {
      result = await uploadApps.uploadOfferDocuments.execute(file , offerNumber , fileType);
    });


    if (result.error) {
      res.json({
          code: result.code,
          data: false
      });
  } else {
      res.json({
          error: false
      });
  }
}

const actions = {
  uploadCostFiles,
  uploadExpensesFileFromAdmin,
  uploadFamiliesMasterFileFromAdmin,
  uploadExchangeRatesFromAdmin,
  createFileTracker,
  updateFileTracker,
  listFiles,
  deleteFileTracker,
  uploadOfferDocuments
};


export default actions;