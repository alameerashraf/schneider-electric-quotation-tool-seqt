import { Router } from 'express';
import actions from './e2e.controller'

// Define router.. 
var router = Router();

router.route('/get-costing-summary/:offerNumber')
    .get(actions.getCostingSummary)


export default router;