import e2es from '../../../application/e2e/e2e.app'

const e2eApps = e2es();

async function getCostingSummary(req, res, next) {
    let offerNumber = req.params.offerNumber;
    let result = await e2eApps.getCostingSummary.execute(offerNumber);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result,
        });
    }
}

const actions = {
    getCostingSummary
};

export default actions;