import { Router } from 'express';
import * as offers from './offer/offer.routes';
import * as bFo from './bFO/bFO.routes';
import * as user from './user/user.routes';
import * as dashboard from './dashboard/dashboard.routes';
import * as lookups from './lookups/lookups.routes';
import * as items from './item/item.routes';
import * as uploads from './upload/upload.routes';
import * as notifications from './notifications/notifications.routes';
import * as exports from './exports/exports.routes'
import * as admins from './admin/admin.routes';

// Define router.. 
var router = Router();


// controllers
router.use('/offer' , offers.default);
router.use('/bFO' , bFo.default);
router.use('/user' , user.default);
router.use('/dashboard' , dashboard.default);
router.use('/lookup' , lookups.default);
router.use('/item' , items.default);
router.use('/upload' , uploads.default);
router.use('/notification' , notifications.default);
router.use('/exports', exports.default)
router.use('/admin', admins.default);

export default router;

