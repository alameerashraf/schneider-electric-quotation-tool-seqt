import offers from '../../../application/offer/offer.app';
import users from '../../../application/user/user.app';
import kpiStatusMap from '../../../common/constants/kpi-status-map';

const offerApps = offers();
const userApps = users();

async function paramSESA(req , res, next , userSESA){
    let user = await userApps.getUserBySesa.execute(userSESA);
    let userRoles = user.data.roles;

    if(userRoles != null || userRoles.length > 0)
        req.highestRole = user.data.roles.reduce((p, c) => p.priority > c.priority ? p : c);
    else 
        req.highestRole = null;
    
    req.user = user;
    next();
};

async function getUserOffers(req , res , next){
    let skip = parseInt(req.query.skip); 
    let limit = parseInt(req.query.limit); 
    let user = req.user;
    let highestRole = req.highestRole;
    let offerSubjects = [];
    let offersRoles = ["TE" , "TM"];
    if(highestRole != null){
        if(offersRoles.includes(highestRole.shortcut))
            offerSubjects = highestRole.subjects.find(x => x.name == "offer");
    }
    
    let queryParamters = [];
    Object.keys(req.query).forEach((param) => {
        if(param != "skip" && param != "limit"){
            queryParamters.push(param);
        }
    });


    if (user.error != false) {
        res.json({
            code: user.code,
            message: user.message,
            error: user.error,
            data: false
        });
    } else {
        let result = await offerApps.getUserOffers.execute(user.data , queryParamters);
        let totalNumberOfOffers = result.data.count;
        let userOffers = result.data.offers.slice(skip , limit);

        let offers = [];
        userOffers.forEach((element) => {
            let newOffer = element;
            let action = offerApps.defineOfferActions.execute(
                element.currentStatus,
                user.data.sesa,
                element.createdBy,
                element.assignedTo,
                offerSubjects.actions);
                
            newOffer = newOffer.toObject();
            newOffer.actions = action;
            offers.push(newOffer);
        });

        res.json({
            error: false,
            message: '',
            data: offers,
            total: totalNumberOfOffers
        });
    }
};

async function getUserFiltersCount(req , res , next){
    let user = req.user.data;
    let userOffers = user.offers || [];
    let result = await offerApps.getOffersCountPerFilter.execute(userOffers , user.sesa);

    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data || {},
        });
    }

};

function getUserKPIs(req , res , next){
    let user = req.user;

    if (user.error != false) {
        res.json({
            code: user.code,
            message: user.message,
            error: user.error,
            data: false
        });
    } else {
        let userKPIs = user.data.kpis.toObject();

        // Remove unwanted KPI from dashboard! (DRAFT)
        delete userKPIs.manager[kpiStatusMap.manager.CREATED];

        Object.keys(userKPIs["manager"]).forEach(function(key,index) {
            userKPIs["manager"][key] = userKPIs["manager"][key].length;
        });

        Object.keys(userKPIs["employee"]).forEach(function(key,index) {
            userKPIs["employee"][key] = userKPIs["employee"][key].length;
        });

        res.json({
            error: false,
            message: '',
            data: userKPIs,
        });
    }
};

function search(req , res , next){

};

let actions = {
    paramSESA,
    getUserOffers,
    getUserKPIs,
    getUserFiltersCount,
    search
};

export default actions;