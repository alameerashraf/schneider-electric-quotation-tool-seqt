import { Router } from 'express';
import actions from './dashboard.controller';

var router = Router();


router.param('userSESA' , actions.paramSESA);

router.route('/offers/:userSESA')
    .get(actions.getUserOffers);

router.route('/filters-count/:userSESA')
    .get(actions.getUserFiltersCount)

router.route('/kpis/:userSESA')
    .get(actions.getUserKPIs)


router.route('/search')
    .post(actions.search);


export default router;
