import apps from '../../../application/user/user.app';
import offer from '../../../application/offer/offer.app';
import errors from '../../../common/constants/error-codes';

const userApps = apps();
const offerApps = offer();

async function paramSESA(req , res , next , userSESA){
    let result = await userApps.getUserBySesa.execute(userSESA);
    req.user = result;
    next();
};

async function paramOfferNumber(req , res , next , offerNumber){
    let result = await offerApps.getOfferByOfferNumber.execute(offerNumber);
    req.offer = result.data;
    next();
};

async function createUser(req , res , next) {
    let user = req.body;
    let newUser = await userApps.createUser.execute(user);
    if (newUser.error != false) {
        res.json({
            code: newUser.code,
            message: newUser.message,
            error: newUser.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: newUser,
            total: 1
        });
    }
};

async function getSubordinates(req , res , next){
    let user = req.user;
    let result = await userApps.getAllTenderingEngineer.execute({});


    if (result.error != false) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data
        }); 
    }
};

async function getUser(req , res , next){
    let user = req.user;
    if (user.error != false) {
        res.json({
            code: user.code,
            message: user.message,
            error: user.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: user.data,
        });
    }
};

async function getUserRoles(req , res, next){
    let result = req.user;
    if (result.error != false) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        let roles = result.data.roles || [] ;
        
        res.json({
            error: false,
            message: '',
            data: roles,
        });
    }
};

async function updateUserKPIs(req, res, next) {
    let offer = req.offer;
    let result = await userApps.updateUserKPIs.execute(offer);

    if (result.error != false) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data,
        });
    }
};

async function getAllTenderingEngineer(req, res, next){
    let result = await userApps.getAllTenderingEngineer.execute()
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data,
        });
    }
};

async function getManagers(req , res , next){
    let result = await userApps.getAllManagers.execute({});

    if (result.error != false) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data
        }); 
    }
};

async function getEmployees(req , res , next){
    let result = await userApps.getAllEmployees.execute({});

    if (result.error != false) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data
        }); 
    }
};

const actions = {
    paramSESA,
    paramOfferNumber,
    createUser,
    getSubordinates,
    getManagers,
    getEmployees,
    getUser,
    getUserRoles,
    updateUserKPIs,
    getAllTenderingEngineer
};

export default actions;