import { Router } from 'express';
import actions from './user.controller';


// Define router.. 
var router = Router();

router.param('userSESA' ,  actions.paramSESA);
router.param('offerNumber' ,  actions.paramOfferNumber);



router.route('/update/kpis/:offerNumber')
    .get(actions.updateUserKPIs);

router.route('/load/:userSESA')
    .get(actions.getUser);

router.route('/create')
    .post(actions.createUser);

router.route('/subordinates/:userSESA')
    .get(actions.getSubordinates);

router.route("/load-managers")
    .get(actions.getManagers);

router.route("/load-employees")
    .get(actions.getEmployees);

router.route('/roles/:userSESA')
    .get(actions.getUserRoles);

router.route('/get-all-tendering-engineers')
    .get(actions.getAllTenderingEngineer)
    

export default router;