import apps from '../../../application/user/user.app';
import reportNames from '../../../common/constants/report-names-map';
import exports from '../../../application/exports/exports.app';

const userApps = apps();
const exportsApps = exports()

async function paramSESA(req , res , next , userSESA){
    let result = await userApps.getUserBySesa.execute(userSESA);
    req.user = result;
    next();
};


async function exportReport(req, res, next){
    const reportMetaData = req.body
    const reportUseCaseName = reportNames[reportMetaData.reportName]
    const user = req.user.data
    if(!reportUseCaseName){
        res.json({
            code: 400,
            message: "report name not found",
            error: true
        })
    }
    let result = await exportsApps[reportUseCaseName].execute(reportMetaData, user)
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data,
        });
    }
}

const actions = {
    paramSESA,
    exportReport
};

export default actions;