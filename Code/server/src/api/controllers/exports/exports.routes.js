import { Router } from 'express';
import actions from './exports.controller';

var router = Router();

router.param('userSESA' ,  actions.paramSESA);

router.route('/export/:userSESA')
    .post(actions.exportReport);


export default router;