import { Router } from 'express';
import actions from "./bFO.controller";
// Define router.. 
var router = Router();

router.param('seRefrence' , actions.params);

router.route('/obtain-access-tokens')
    .get(actions.obtainAceesTokens)

router.route('/load-opportunity-data/:seRefrence')
    .get(actions.loadData)

export default router;