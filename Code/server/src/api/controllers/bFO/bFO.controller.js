import logger from '../../../loaders/logger';
import apps from '../../../infrastructure/bFO/bFO.app';
import errors from '../../../common/constants/error-codes';
const bFOApp = apps();

/**
 * PARAMS functions to get all the paramters, if any provided by the client.
 */
function params (req , res , next , seRefrence){
    req.seRefrence = seRefrence;
    next();
};


/**
 * GET
 */
async function obtainAceesTokens(req, res, next) {
    let isTokenObtainedAndSaved = await bFOApp.obtainAccessTokens.execute();
    if (isTokenObtainedAndSaved.error) {
        res.json({
            code: isTokenObtainedAndSaved.code,
            message: isTokenObtainedAndSaved.message.toString(),
            error: isTokenObtainedAndSaved.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: "",
        });
    }
};


/**
 * Read access tokens from db, validate tokens, use tokens to load data.
 */
async function loadData(req, res, next) {
    let seRefrence = req.seRefrence;
    let requestedResource = "opportunity";
    let data = {};
    data = await bFOApp.loadData.execute(requestedResource, seRefrence);

    if (data.error) {
        if (data.code == errors.INVALID_ACCESS_TOKEN) {
            await bFOApp.obtainAccessTokens.execute("opportunityData", seRefrence);
            data = await bFOApp.loadData.execute(requestedResource, seRefrence);
        } else {
            res.json({
                code: data.code,
                message: data.message,
                error: data.error,
                data: false
            });
        }
    } else if (data.data.hasOwnProperty('Fault')) {
        res.json({
            code: errors.ERROR_LOADING_DATA_FROM_EXTERNAL_SOURCE,
            message: data.data.Fault.faultstring,
            error: data.error || true,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: "",
            data: data.opportunityDetail[0] || {}, /*.opportunityDetail[0] || {}, */
            total: data.total
        });
    }
}

const actions = {
    params,
    obtainAceesTokens,
    loadData
}

export default actions;
