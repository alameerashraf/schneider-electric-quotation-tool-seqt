import loggers from "../../../loaders/logger";
import offerStatus from "../../../common/constants/offer-status";
import apps from "../../../application/offer/offer.app";
import errorCodes from "../../../common/constants/error-codes";

let offerApps = apps();

async function paramOFFER_NUMBER(req, res, next, offerNumber) {
  let result = await offerApps.getOfferByOfferNumber.execute(offerNumber);
  req.offer = result.data;
  next();
}

function addItemToOffer(req, res, next) {}

async function addBulkItemsToOffer(req, res, next) {
  let offer = req.offer;
  let items = req.body;

  let data = await offerApps.addOfferItems.execute(offer, items);

  if (data.error != false) {
    res.json({
      code: data.code,
      message: data.message,
      error: data.error,
      data: false,
    });
  } else {
    res.json({
      error: false,
      message: "",
      data: data.data,
    });
  }
}

async function getOfferItems(req, res, next) {
  let offerNumber = req.params.offerNumber;
  let skip = req.query.skip != undefined ? parseInt(req.query.skip) : null;
  let limit = req.query.limit != undefined ? parseInt(req.query.limit) : null;

  let data = await offerApps.getOfferItems.execute(offerNumber);

  if (data.error != false) {
    res.json({
      code: data.code,
      message: data.message,
      error: data.error,
      data: false,
    });
  } else {
    let offerItems = data.data;
    let result = offerItems;

    res.json({
      error: false,
      message: "",
      data: result,
      total: offerItems.length,
    });
  }
}

async function deleteItemFromOffer(req, res, next) {
  let itemIds = req.body.itemIds;
  let offer = req.offer;
  let data = await offerApps.deleteItemFromOffer.execute(offer, itemIds);

  if (data.error != false) {
    res.json({
      code: data.code,
      message: data.message,
      error: data.error,
      data: false,
    });
  } else {
    res.json({
      error: false,
    });
  }
};

async function cloneOfferItems(req , res, next){
  let itemIds = req.body.itemIds;
  let offer = req.offer;
  let data = await offerApps.cloneItemsFromOffer.execute(offer, itemIds);

  if (data.error != false) {
    res.json({
      code: data.code,
      message: data.message,
      error: data.error,
      data: false,
    });
  } else {
    res.json({
      error: false,
      data: data.data
    });
  }
}

async function addExpenseToOffer(req , res , next){
  let generalExpenses = req.body.generalExpenses;
  let offer = req.offer;
  let data = await offerApps.addExpenseToOffer.execute(offer, generalExpenses);

  if (data.error != false) {
    res.json({
      code: data.code,
      message: data.message,
      error: data.error,
      data: false,
    });
  } else {
    res.json({
      error: false,
      data: data.data
    });
  }
};



const actions = {
  paramOFFER_NUMBER,
  addItemToOffer,
  addBulkItemsToOffer,
  addExpenseToOffer,
  getOfferItems,
  deleteItemFromOffer,
  cloneOfferItems,
};

export default actions;
