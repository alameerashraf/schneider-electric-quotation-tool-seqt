import { Router } from 'express';
import actions from './item.controller';

var router = Router();

router.param('offerNumber' , actions.paramOFFER_NUMBER)

router.route("/create-items/:offerNumber")
    .post(actions.addBulkItemsToOffer);

router.route("/load-items/:offerNumber")
    .get(actions.getOfferItems);

router.route("/delete-items/:offerNumber")
    .post(actions.deleteItemFromOffer);

router.route("/clone-items/:offerNumber")
    .post(actions.cloneOfferItems);



export default router;