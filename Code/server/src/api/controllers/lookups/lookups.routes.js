import { Router } from 'express';
import actions from './lookups.controller';

var router = Router();


router.route('/countries')
    .get(actions.getCountries);

router.route('/expenses')
    .get(actions.getExpenses)

router.route('/currencies')
    .get(actions.getCurrencies)

router.route('/roles')
    .get(actions.getRoles)

export default router;
