import lookups from '../../../application/lookups/lookups.app';
const lookupActions = lookups();


async function getCountries(req , res , next){
    let data = await lookupActions.getCountries.execute();

    if (data.error != false) {
        res.json({
            code: data.code,
            message: data.message,
            error: data.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: data.data.countries,
            total: data.data.countries.length
        });
    }
};

async function getExpenses(req , res , next){
    let data = await lookupActions.getExpenses.execute();
    if (data.error != false) {
        res.json({
            code: data.code,
            message: data.message,
            error: data.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: data.data,
        });
    }
};

async function getCurrencies(req , res , next){
    let data = await lookupActions.getCurrencies.execute();
    if (data.error != false) {
        res.json({
            code: data.code,
            message: data.message,
            error: data.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: data.data.conversionRate,
            total: data.data.conversionRate.length
        });
    }
};

async function getRoles(req , res , next){
    let data = await lookupActions.getRoles.execute();
    if (data.error != false) {
        res.json({
            code: data.code,
            message: data.message,
            error: data.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: data.data,
        });
    }
}

const actions = {
    getCountries,
    getExpenses,
    getCurrencies,
    getRoles
};

export default actions;