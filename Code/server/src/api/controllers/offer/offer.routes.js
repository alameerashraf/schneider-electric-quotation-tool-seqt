import { Router } from 'express';
import actions from './offer.controller';

// Define router.. 
var router = Router();

router.param('offerNumber' , actions.paramOFFER_NUMBER);


router.route('/load/:offerNumber')
    .get(actions.getOffer);
    
router.route('/latest-offer-number/:seRefrence')
    .get(actions.getLatestOfferNumber);

router.route('/create')
    .post(actions.createOffer);

router.route('/update/:offerNumber')
    .post(actions.updateOffer);

router.route('/assign/:offerNumber')
    .post(actions.assignOffer);

router.route('/submit-offer/:offerNumber')
    .post(actions.submitOffer);

router.route('/starting-approval-cycle/:offerNumber')
    .get(actions.startingApprovalcycle)

router.route('/approve/:offerNumber')
    .get(actions.approveOffer)

router.route('/reject/:offerNumber')
    .get(actions.rejectOffer)

router.route('/recall-offer/:offerNumber')
    .post(actions.recallOffer)

export default router;