import loggers from '../../../loaders/logger';
import offerStatus from '../../../common/constants/offer-status';
import emailHandler from '../../../common/handlers/email-handler';

import apps from '../../../application/offer/offer.app';
import user from '../../../application/user/user.app';
import notifications from '../../../application/notifications/notifications.app';

let offerApps = apps();
let userApps = user();
let notificationsApp = notifications();

async function paramOFFER_NUMBER(req, res, next, offerNumber) {
    let result = await offerApps.getOfferByOfferNumber.execute(offerNumber);
    req.offer = result.data;
    next();
};

async function getOffer(req, res, next) {
    let data = req.offer;
    res.json({
        data: data
    });
};

async function createOffer(req, res, next) {
    let requestData = req.body;
    let data = await offerApps.createOffer.execute(requestData.offer);
    let createdOffer = data.data;

    let result = await userApps.updateUserOffers.execute(requestData.user ,  createdOffer);

    if (data.error || result.error) {
        res.json({
            code: data.code || result.code,
            message: data.message || result.message,
            error: data.error|| result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: data.data,
        });
    }
};

async function assignOffer(req , res , next) {
    let assignementStep = req.body;
    let offer = req.offer;
    let assignee = assignementStep?.owner;
    let teManager = (await userApps.getUserBySesa.execute(assignee)).data?.manager?.sesa;
    let currentCreator = offer.offerTimeline.find(x => x.isLatest == true).owner;



    //Other manager assigning the offer to a non-subordinates.
    if(offer.createdBy != teManager){
        let creationTimeLineStep = offer.offerTimeline.find(x => x.isLatest == true);

        offer.createdBy = teManager;
        creationTimeLineStep["changedBy"] = teManager;
        creationTimeLineStep["owner"] = teManager;

        await offerApps.updateOffer.execute(offer.offerNumber , offer);
        await userApps.updateUserOffers.execute(teManager , offer._id);
        await userApps.updateUserOffers.execute(currentCreator , offer._id, "remove");
    }

    if(offer.createdBy != assignee){
        let assigningResult = await userApps.updateUserOffers.execute(assignee , offer);
    }

    let result = await offerApps.assignOffer.execute(offer , assignementStep);

    if (result.error) {
        res.json({
            code: result.code ,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data,
        });
    }
};

async function getLatestOfferNumber(req , res , next){
    let seRefrence = req.params.seRefrence;
    let data = await offerApps.getLatestCreatedOfferNumber.execute(seRefrence);

    if (data.error != false) {
        res.json({
            code: data.code,
            message: data.message,
            error: data.error,
            data: false
        });
    } else {
        let offerNumber = "0000";
        if(data.data.length > 0){
            offerNumber = parseInt(data.data[0].offerNumber.split('*')[1]);
        }

        res.json({
            error: false,
            message: '',
            data: offerNumber
        });
    }
};

//TODO: Update offer is not finished yet!
async function updateOffer(req , res , next){
    let offerNumber = req.params.offerNumber;
    let updatedOffer = req.body;
    offerApps.updateOffer.execute(offerNumber , updatedOffer);
};

async function submitOffer(req, res, next){
    const requestData = req.body;
    let offer = req.offer;

    let ownerEmail = await (await userApps.getUserBySesa.execute(offer.createdBy)).data;
    const result = await offerApps.submitOffer.execute(offer , requestData);

    if (result.error) {
        res.json({
            code: result.code ,
            message: result.message,
            error: result.error ,
            data: false
        });
    } else {
        let emailBody = await getEmailBody(offer.createdBy , requestData.changedBy , "offerSubmitted", { offerNumber: req.offer.offerNumber });
        let isEmailSent = await notificationsApp.notifyByEmail.execute(emailBody);

        res.json({
            error: false,
            message: '',
            data: result.data,
        });
    }
}

async function startingApprovalcycle(req, res, next) {
    let offer = req.offer;

    //update offer status and timeline
    const resultOffer = await offerApps.startingApprovalCycle.execute(offer);
    // update kpis
     let resultKPIs = await userApps.updateUserKPIs.execute(offer);

    if (resultOffer.error || resultKPIs.error) {
        res.json({
            code: resultOffer.code || resultKPIs.code,
            message: resultOffer.message || resultKPIs.message ,
            error: resultOffer.error || resultKPIs.error ,
            data: false
        });
    } else {
        res.json({
            error: false,
            data: resultOffer.data,
        });
    }
}

async function approveOffer(req, res, next){
    let offer = req.offer;

    // this returns the last move in the offer's timeline
    let resultOffer = await offerApps.approveOffer.execute(offer);
    //updating offer's creator and owner KPIs
    let resultKPIs = await userApps.updateUserKPIs.execute(offer);

    let resultUpdateManagerOffers = {}
    if(resultOffer.data && resultOffer.data.newStatus != offerStatus["APPROVED"]){
        //update next manager offers 
        resultUpdateManagerOffers = await userApps.updateUserOffers.execute(resultOffer.data.owner, offer)
        let emailBody = await getEmailBody(resultOffer.data.owner , resultOffer.data.changedBy , "offerWaitingApproval");
        // let isEmailSent = await notificationsApp.notifyByEmail.execute(emailBody);
    };

    if (resultOffer.error || resultKPIs.error || resultUpdateManagerOffers.error) {
        res.json({
            code: resultOffer.code || resultKPIs.code || resultUpdateManagerOffers.code,
            message: resultOffer.message || resultKPIs.message || resultUpdateManagerOffers.message ,
            error: resultOffer.error || resultKPIs.error || resultUpdateManagerOffers.error ,
            data: false
        });
    } else {
        res.json({
            error: false,
            data: resultOffer.data,
        });
    }
}

async function rejectOffer (req, res, next){
    let offer = req.offer;

    let resultOffer = await offerApps.rejectOffer.execute(offer);
    let resultKPIs = await userApps.updateUserKPIs.execute(offer);


    if (resultOffer.error || resultKPIs.error) {
        res.json({
            code: resultOffer.code || resultKPIs.code,
            message: resultOffer.message || resultKPIs.message ,
            error: resultOffer.error || resultKPIs.error ,
            data: false
        });
    } else {
        if(resultOffer.data && resultOffer.data.newStatus == offerStatus.REJECTED){
            let emailBody = await getEmailBody(resultOffer.data.owner , resultOffer.data.changedBy , "offerRejected" );
            // let isEmailSent = await notificationsApp.notifyByEmail.execute(emailBody);
        };

        res.json({
            error: false,
            data: resultOffer.data,
        });
    }
}


async function getEmailBody(owner , changer, subject, params){
    let anOwner = await (await userApps.getUserBySesa.execute(owner)).data;
    let aChanger = await (await userApps.getUserBySesa.execute(changer)).data;

    let emailBody = emailHandler.prepareEmail(
        subject,
      { ...params },
      {
        receiverInfo: { email: anOwner.email, name: anOwner.name },
        senderInfo: { email: aChanger.email, name: aChanger.name },
      }
    );

    return emailBody;
}

async function recallOffer(req, res, next) {
    let offerNumber = req.params.offerNumber;
    let result = await offerApps.recallOffer.execute(offerNumber);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Offer recalled successfully',
            data: result.data,
        });
    }
}

const actions = {
    paramOFFER_NUMBER,
    createOffer,
    assignOffer,
    getOffer,
    getLatestOfferNumber,
    updateOffer,
    submitOffer,
    startingApprovalcycle,
    approveOffer,
    rejectOffer,
    recallOffer
};

export default actions;