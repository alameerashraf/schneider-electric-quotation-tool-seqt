import admins from '../../../application/admin/admin.app'
import family from '../../../models/family/family';

const adminApps = admins();

async function log(req, res, next) {
    let logger = req.body;
    let result = await adminApps.logAction.execute(logger);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
        });
    }
}


async function getLogs(req, res, next) {
    let affectedUserSESA = req.params.userSESA;
    let result = await adminApps.listLogs.execute({ affectedUser :affectedUserSESA });
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data,
        });
    }
}

async function getUsersData(req, res, next) {
    let result = await adminApps.getUsersData.execute({})
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data,
        });
    }
}

async function updateUser(req, res, next) {
    let userSesa = req.params.sesa;
    let user = req.body;
    let result = await adminApps.updateUser.execute(userSesa, user);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'User updated successfully',
            data: result.data,
        });
    }
}

async function deleteUser(req, res, next) {
    let usersSesas = req.body.sesa;
    console.log(usersSesas);
    let result = await adminApps.deleteUser.execute(usersSesas);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'User deleted successfully'
        });
    }
}

async function delegateRole(req, res, next) {
    let fromUserSesa = req.body.fromUserSesa;
    let toUserSesa = req.body.toUserSesa;

    let date = "test";
    let result = await adminApps.delegateRole.execute(fromUserSesa, toUserSesa, date);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Role delegated successfully',
            data: result.data,
        });
    }


}

async function createFamily(req, res, next) {
    let family = req.body;
    let result = await adminApps.createFamily.execute(family);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Family created successfully',
            data: result.data,
        });
    }

}

async function getFamily(req, res, next) {
    let result = await adminApps.listFamilies.execute();
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data,
        });
    }
}

async function updateFamily(req, res, next) {
    let familyName = req.params.familyName;
    let family = req.body;
    let result = await adminApps.updateFamily.execute(familyName, family);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Family updated successfully',
            data: result.data,
        });
    }
}

async function deleteFamily(req, res, next) {
    let familyName = req.params.familyName;
    let result = await adminApps.deleteFamily.execute(familyName);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Family deleted successfully'
        });
    }

}

async function createProduct(req, res, next) {
    let familyName = req.params.familyName;
    let product = req.body;
    let result = await adminApps.createProduct.execute(familyName, product);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Product created successfully',
            data: result.data,
        });
    }

}

async function getProduct(req, res, next) {
    let familyName = req.params.familyName;
    let result = await adminApps.getProduct.execute({"family": familyName});
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: '',
            data: result.data.products,
        });
    }
}

async function deleteProduct(req, res, next) {
    let familyName = req.params.familyName;
    let productId = req.params.productId
    let result = await adminApps.deleteProduct.execute(familyName, productId);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Product deleted successfully'
        });
    }

}

async function updateProduct(req, res, next) {
    let familyName = req.params.familyName;
    let productId = req.params.productId;
    let product = req.body;
    let result = await adminApps.updateProduct.execute(familyName, productId, product);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Product updated successfully',
            data: result.data,
        });
    }
}

async function revokeRole(req, res, next) {
    let userSesa = req.params.userSesa;
    let roleId = req.params.roleId;
    let result = await adminApps.revokeRole.execute(userSesa, roleId);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Role Revoked Succesfully'
        });
    }
}

async function assignRole(req, res, next) {
    let userSesa = req.params.userSesa;
    let roleId = req.params.roleId;
    let result = await adminApps.assignRole.execute(userSesa, roleId);
    if (result.error) {
        res.json({
            code: result.code,
            message: result.message,
            error: result.error,
            data: false
        });
    } else {
        res.json({
            error: false,
            message: 'Role Assigned Succesfully'
        });
    }
}

const actions = {
    log,
    getLogs,
    getUsersData,
    updateUser,
    deleteUser,
    delegateRole,
    createFamily,
    getFamily,
    updateFamily,
    deleteFamily,
    createProduct,
    getProduct,
    deleteProduct,
    updateProduct,
    revokeRole,
    assignRole
};

export default actions;