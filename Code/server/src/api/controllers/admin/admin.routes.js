import { Router } from 'express';
import actions from './admin.controller'

// Define router.. 
var router = Router();

router.route('/log')
    .post(actions.log);

router.route('/logs/:userSESA')
    .get(actions.getLogs);

router.route('/load-users')
    .get(actions.getUsersData)    

router.route('/update-user/:sesa')
    .post(actions.updateUser)    

router.route('/delete-user')
    .post(actions.deleteUser)  
    
router.route('/delegate-role')
    .post(actions.delegateRole)

router.route('/create-family')
    .post(actions.createFamily)

router.route('/load-families')
    .get(actions.getFamily)

router.route('/update-family/:familyName')
    .post(actions.updateFamily)

router.route('/delete-family/:familyName')
    .post(actions.deleteFamily)

router.route('/create-product/:familyName')
    .post(actions.createProduct)

router.route('/get-product/:familyName')
    .get(actions.getProduct)

router.route('/delete-product/:familyName/:productId')
    .post(actions.deleteProduct)

router.route('/update-product/:familyName/:productId')
    .post(actions.updateProduct)

router.route('/revoke-role/:userSesa/:roleId')
    .post(actions.revokeRole)

router.route('/assign-role/:userSesa/:roleId')
    .post(actions.assignRole)




export default router;